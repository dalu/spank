/*

cc -DSPANK_PORT_UNIX -DSPANK_DEBUG_UNIX -I include spank_test_timer.c spank_port_unix.c spank_timer.c


*/

#include <stddef.h>
#include <ctype.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "spank_timer.h"

spank_timer_t t1 ,t2, t3, t4;


void cb(void *data) {
    printf("Timer %s\n", (char *)data);
}

void cb2(void *data) {
    printf("Timer %s\n", (char *)data);
    //spank_timer_set(&t2, 5, cb2, "T2");
    spank_timer_cancel(&t4);
}


int main() {

    spank_timer_set(&t1,  5, cb, "t1");
    spank_timer_set(&t2, 15, cb2, "t2");
    spank_timer_set(&t3,  7, cb, "t3");
    spank_timer_set(&t4,  50, cb, "t4");

    //spank_timer_cancel(&t1);
    //spank_timer_cancel(&t2);

    spank_timer_ctrl_dump();

    while (_spank_timer_is_alarm_running()) {
	_spank_fake_time_increase(0);
	spank_timer_ticking();
	spank_timer_ctrl_dump();
    }
    
}


