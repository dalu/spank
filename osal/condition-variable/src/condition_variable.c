#include <sys/queue.h>
#include <stdbool.h>

#include "spank/osal/cv.h"

#if !defined(SPANK_CV_NATIVE)

#if !defined(SPANK_CV_FALLBACK_TO_EMULATED)
#error "using semaphore emulated condition-variable is not advised (define SPANK_CV_FALLBACK_TO_EMULATED to accept the risk)"
#endif

#error "Not fully tested"

/*
 * Condition Variables
 * https://stackoverflow.com/questions/11000725/implementation-of-condition-variables
 * https://www.microsoft.com/en-us/research/wp-content/uploads/2004/12/ImplementingCVs.pdf
 */

typedef struct spank_cv_tctx {
    STAILQ_ENTRY(spank_cv_tctx) entries;
    spank_semaphore_t sem;
} spank_cv_tctx_t;


void spank_cv_init(spank_cv_t *cond) {
    STAILQ_INIT(&cond->head);
    spank_mutex_init(&cond->mutex);
}


bool spank_cv_wait(spank_cv_t *cond, spank_mutex_t *mutex,
		   spank_systicks_t timeout) {
    spank_cv_tctx_t self = { 0 };
    bool            rc   = false;

    spank_semaphore_init(&self.sem, 0);
    
    spank_mutex_acquire(&cond->mutex, SPANK_SYSTICKS_INFINITE);
    STAILQ_INSERT_TAIL(&cond->head, &self, entries);
    spank_mutex_release(&cond->mutex);

    spank_mutex_release(mutex);
    rc = spank_semaphore_take(&self.sem, timeout);
    spank_mutex_acquire(mutex, SPANK_SYSTICKS_INFINITE);

    return rc;
}



void spank_cv_signal(spank_cv_t *cond) {
    spank_mutex_acquire(&cond->mutex, SPANK_SYSTICKS_INFINITE);
    spank_cv_tctx_t *tctx = STAILQ_FIRST(&cond->head);
    if (tctx != NULL) {
        spank_semaphore_give(&tctx->sem);
	STAILQ_REMOVE_HEAD(&cond->head, entries);
    }
    spank_mutex_release(&cond->mutex);
}

void spank_cv_broadcast(spank_cv_t *cond) {
    spank_mutex_acquire(&cond->mutex, SPANK_SYSTICKS_INFINITE);
    while (!STAILQ_EMPTY(&cond->head)) {
	spank_cv_tctx_t *tctx = STAILQ_FIRST(&cond->head);
	STAILQ_REMOVE_HEAD(&cond->head, entries);
	spank_semaphore_give(&tctx->sem);
     }
    spank_mutex_release(&cond->mutex);
}

#endif // SPANK_CV_NATIVE
