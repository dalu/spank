#ifndef __SPANK_OSAL_CV_H__
#define __SPANK_OSAL_CV_H__

#include "spank/osal.h"

#if !defined(SPANK_CV_NATIVE)

#include <sys/queue.h>

typedef struct spank_cv {
    STAILQ_HEAD(, spank_cv_tctx) head;
    spank_mutex_t mutex;
} spank_cv_t;

void spank_cv_init(spank_cv_t *cv);
bool spank_cv_wait(spank_cv_t *cv, spank_mutex_t *mutex, spank_systicks_t timeout);
void spank_cv_signal(spank_cv_t *cv);
void spank_cv_broadcast(spank_cv_t *cv);

#endif // SPANK_CV_NATIVE

#endif
