/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_VERSION_H__
#define __SPANK_VERSION_H__

#ifndef SPANK_VERSION
#if defined(SPANK_VERSION_NICKNAME)
#define SPANK_VERSION SPANK_VERSION_NICKNAME
#else
#define SPANK_VERSION "< n/a >"
#endif
#endif

#ifndef SPANK_VERSION_ID
#if   defined(SPANK_VERSION_COMMIT_ID)
#define SPANK_VERSION_ID SPANK_VERSION_COMMIT_ID
#elif defined(SPANK_VERSION_COMMIT_ID_FULL)
#define SPANK_VERSION_ID SPANK_VERSION_COMMIT_ID_FULL
#elif defined(SPANK_VERSION_COMMIT_ID_SHORT)
#define SPANK_VERSION_ID SPANK_VERSION_COMMIT_ID_SHORT
#else
#define SPANK_VERSION_ID "< n/a >"
#endif
#endif

#endif
