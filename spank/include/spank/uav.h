/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK__UAV_H__
#define __SPANK__UAV_H__

#include "spank/osal.h"
#include "spank/types.h"

#define SPANK_UAV_CTRL_TAKEOFF            'T'
#define SPANK_UAV_CTRL_LANDING            'L'
#define SPANK_UAV_CTRL_EMERGENCY_SHUTDOWN 'E'

/**
 * Initialize UAV interraction
 */
bool spank_uav_init(void);

/**
 * Get UAV posture.
 *
 * @param posture		UAV posture
 */
void spank_uav_get_posture(spank_posture_t *posture);

/**
 * Feedback the UAV with computed distance from another UAV.
 *
 * @param dist			distance
 * @param stddev		standard deviation
 * @param posture		other UAV posture
 * @param id			other UAV identifier
 * @param extra
 */
void spank_uav_feed_distance(spank_distance_t dist, spank_stddev_t stddev,
			     spank_posture_t *posture, uint64_t id, void *extra);

/**
 * Transmit predefine control order to the UAV.
 * Existing commands are : takeoff, landing, and emergency shutdown.
 *
 * @param cmd			command to transmit to the UAV
 * @param args			arguments
 */
void spank_uav_control(int cmd, void *args);

#endif
