/*
 * (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_REPORT_H__
#define __SPANK_REPORT_H__

#define SPANK_REPORT_STATE		1
#define SPANK_REPORT_TWR_DISTANCE	2

typedef spank_state_t spank_report_state_t;

typedef struct spank_report_twr_distance {
    spank_distance_t distance;
    spank_addr_t     from;
    spank_posture_t *posture;
} spank_report_twr_distance_t;

#endif
