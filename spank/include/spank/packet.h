/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_PACKET_H__
#define __SPANK_PACKET_H__

#include "spank/io.h"
#include "spank/uav.h"


/* Type for SPANK packet
 * Note: Values 0x00 and 0xff are reserved and must not be used.
 */
#define SPANK_PKT_TYPE_TWR_POLL   0x01 // Poll is initiated by the tag
#define SPANK_PKT_TYPE_TWR_ANSWER 0x02
#define SPANK_PKT_TYPE_TWR_FINAL  0x03
#define SPANK_PKT_TYPE_TWR_REPORT 0x04 // Report contains all measurement from the anchor
#define SPANK_PKT_TYPE_ELECT      0x05
#define SPANK_PKT_TYPE_NAVCTRL    0x06 // Navigation control
#define SPANK_PKT_TYPE_MSG        0x07 // Information message

#define SPANK_NAVCTRL_CMD_EMERGENCY_SHUTDOWN SPANK_UAV_CTRL_EMERGENCY_SHUTDOWN
#define SPANK_NAVCTRL_CMD_TAKEOFF            SPANK_UAV_CTRL_TAKEOFF
#define SPANK_NAVCTRL_CMD_LANDING            SPANK_UAV_CTRL_LANDING

#define SPANK_NAVCTRL_COUNTDOWN_MS  250
#define SPANK_NAVCTRL_COUNTDOWN_MAX 2
#define SPANK_NAVCTRL_COUNTDOWN_COOLDOWN_MS (1.5 * SPANK_NAVCTRL_COUNTDOWN_MS)

struct spank_payload_elect {
    struct {
	spank_addr_t node;
	float32_t distance;
    } range;
    struct {
	spank_addr_t node;
    } next;
} __attribute__((packed));


struct spank_payload_report {
    struct {
	uint64_t t_rp; // Time Received Poll
	uint64_t t_sa; // Time Sent Answer
	uint64_t t_rf; // Time Received Final
    } twr;
    spank_packed_posture_t posture;
} __attribute__((packed));


struct spank_payload_navctrl {
    uint8_t cmd;
    uint8_t countdown;
} __attribute__((packed));

struct spank_payload_msg {
    char data[32];
} __attribute__((packed));

typedef struct spank_packet_hdr {
    uint8_t type;  // Packet type
    uint8_t seq;   // Packet sequence
} __attribute__((packed)) spank_packet_hdr_t;

struct spank_packet {
    struct spank_packet_hdr hdr;
    union {
	struct spank_payload_report  report;
	struct spank_payload_elect   elect;
	struct spank_payload_navctrl navctrl;
	struct spank_payload_msg     msg;
    };
} __attribute__((packed));



#define SPANK_PAYLOAD_ELECT_SIZE   sizeof(struct spank_payload_elect)
#define SPANK_PAYLOAD_REPORT_SIZE  sizeof(struct spank_payload_report)
#define SPANK_PAYLOAD_NAVCTRL_SIZE sizeof(struct spank_payload_navctrl)
#define SPANK_PAYLOAD_MSG_SIZE     sizeof(struct spank_payload_msg)
#define SPANK_PACKET_HEADER_SIZE   sizeof(struct spank_packet_hdr)

#define SPANK_PACKET_ELECT_SIZE   (SPANK_PACKET_HEADER_SIZE +	\
				   SPANK_PAYLOAD_ELECT_SIZE)
#define SPANK_PACKET_REPORT_SIZE  (SPANK_PACKET_HEADER_SIZE +	\
				   SPANK_PAYLOAD_REPORT_SIZE)
#define SPANK_PACKET_NAVCTRL_SIZE (SPANK_PACKET_HEADER_SIZE +	\
				   SPANK_PAYLOAD_NAVCTRL_SIZE)
#define SPANK_PACKET_MSG_SIZE     (SPANK_PACKET_HEADER_SIZE +	\
				   SPANK_PAYLOAD_MSG_SIZE)
#define SPANK_PACKET_NULL_SIZE    (SPANK_PACKET_HEADER_SIZE)

#endif
