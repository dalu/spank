/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_IO_H__
#define __SPANK_IO_H__


#include <sys/types.h>
#include "spank/osal.h"
#include "spank/config.h"
#include "spank/driver.h"
#include "spank/types.h"



/*
 *
 */

struct spank_packet;
typedef struct spank_packet spank_packet_t;


#define SPANK_IO_EVT_TX_DONE		1
#define SPANK_IO_EVT_RX_DONE		2
#define SPANK_IO_EVT_RX_FAILED		3
#define SPANK_IO_EVT_RX_TIMEOUT		4
#define SPANK_IO_EVT_TIMEOUT		5

typedef struct spank_io_frame_properties {
    struct {
	spank_rssi_t firstpath;
	spank_rssi_t signal;
    } power;
    struct {
	int32_t  offset;
	uint32_t interval;
    } clock_tracking;
    struct {
	uint16_t temp;
	uint16_t vbat;
    } chip_info;
} spank_io_frame_properties_t;


typedef struct spank_io_frame_info {
    size_t                      length;
    uint64_t                    timestamp;
    spank_io_frame_properties_t properties;
} spank_io_frame_info_t;


struct spank_io {
    void         *dev;
    spank_addr_t  src_addr;
    spank_panid_t pan_id;
    uint64_t      time_msk;
    float         metre_per_tick;

    
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT ) ||	\
    defined(SPANK_IO_SNIFFER_PACKET_SUPPORT)
    struct {
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT)
	struct { 
	    bool enabled;
	} frame;
#endif
#if defined(SPANK_IO_SNIFFER_PACKET_SUPPORT)
	struct { 
	    bool enabled;
	} packet;
#endif
    } sniffer;
#endif

    struct {
	spank_mutex_t     mutex;	// making sending thread-safe
	spank_semaphore_t done_sem;	// waiting for send completion
	volatile bool     sending;
	uint64_t         *timestamp;	// timestamping requested
    } tx;
    struct {
	spank_semaphore_t pkt_sem;	// handling reception queue
    } rx;

    struct {
	SPANK_STAT(rx_failed);
	SPANK_STAT(rx_timeout);
	SPANK_STAT(rx_sent);
	SPANK_STAT(rx_received);
	SPANK_STAT(rx_lost);
    } stats;

    struct {
	union {
	    uint8_t hdr[IEEE802154_MHR_MAXSIZE];
	} tx;
	union {
	    uint8_t hdr[IEEE802154_MHR_MAXSIZE];
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT)
	    struct {
		uint8_t data[SPANK_DRIVER_FRAME_MAXSIZE];
		spank_io_frame_info_t info;
	    } frame;
#endif
	} rx;
    } buffer;
};
typedef struct spank_io spank_io_t;



struct spank_io_rx_ctx {
    spank_addr_t                src_addr;
    spank_addr_t                dst_addr;
    spank_packet_t             *packet;
    size_t                      packetlen;
#ifdef SPANK_DEBUG_CKSUM
    uint16_t			cksum;
    uint16_t			pkt_cksum;
#endif
    uint8_t                     flags;
    uint64_t                    timestamp;
    spank_io_frame_properties_t properties;
};
typedef struct spank_io_rx_ctx spank_io_rx_ctx_t;



#define SPANK_IO_SEND_ANSWER_EXPECTED		0x1
#define SPANK_IO_SEND_DELAYED_EMBED_TIMESTAMP	0x2




static inline
bool spank_io_addr_is_mine(spank_io_t *io, spank_addr_t addr) {
    return io->src_addr == addr;
}

static inline
bool spank_io_addr_is_broadcast(spank_io_t *io, spank_addr_t addr) {
    return SPANK_ADDR_BROADCAST == addr;
}

static inline
bool spank_io_addr_is_for_me(spank_io_t *io, spank_addr_t addr) {
    return spank_io_addr_is_mine(io, addr) ||
	   spank_io_addr_is_broadcast(io, addr);
}



void spank_io_tx_power(spank_io_t *io, uint8_t *phr, uint8_t *sd);


#define SPANK_IO_TIMEOUT                  -0x01
#define SPANK_IO_UNIMPLEMENTED            -0x02
#define SPANK_IO_INVALID_PKT              -0x10
#define SPANK_IO_PKT_TOO_SHORT            -0x11
#define SPANK_IO_WRONG_SEQ_NUMBER         -0x12
#define SPANK_IO_UNSUPPORTED_VERSION      -0x13
#define SPANK_IO_UNSUPPORTED_IN_VERSION   -0x14
#define SPANK_IO_DECYPHERING_ERROR        -0x21
#define SPANK_IO_INTEGRITY_ERROR          -0x22





#define SPANK_IO_RX_CTX_FLG_RANGING        0x01
#define SPANK_IO_RX_CTX_FLG_POWER          0x02
#define SPANK_IO_RX_CTX_FLG_CLOCK_TRACKING 0x04
#define SPANK_IO_RX_CTX_FLG_CHIP_INFO      0x08

int spank_io_send(spank_io_t *io,
		  void *data, size_t datalen, spank_addr_t dst,
		  uint64_t *timestamp, uint32_t flags, ...);


ssize_t spank_io_recv(spank_io_t *io, spank_io_rx_ctx_t *rx_ctx, spank_systicks_t timeout);


#define SPANK_IO_AFTER_TX true
#define SPANK_IO_AFTER_RX false







void _spank_io_rx_start(spank_io_t *io);
void _spank_io_evt_tx_sent(spank_io_t *io);
void _spank_io_evt_rx_received(spank_io_t *io);
void _spank_io_evt_rx_failed(spank_io_t *io);
void _spank_io_evt_rx_timeout(spank_io_t *io);


void spank_io_init(spank_io_t *io);


void spank_io_sniffer_packet(spank_io_rx_ctx_t *rx_ctx, ssize_t size);
void spank_io_sniffer_frame(uint8_t *data, size_t size,
			    spank_io_frame_info_t *info);

#endif
