/*
 * Copyright (c) 2018-2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef __SPANK_SPANK_H__
#define __SPANK_SPANK_H__

#include "spank/config.h"
#include "spank/timer.h"
#include "spank/node.h"
#include "spank/packet.h"
#include "spank/types.h"
#include "spank/token-sdstwr.h"


#define SPANK_NETOK				 0
#define SPANK_NETOK_DISCARDED			 0
#define SPANK_NETERROR_BUSY			-1
#define SPANK_NETERROR_UNEXPECTED_PACKET	-2
#define SPANK_NETERROR_MALFORMED_PACKET		-3
#define SPANK_NETERROR_PROCESSING_FAILED	-4
#define SPANK_NETERROR_MISSING_RX_TIMESTAMP	-5


typedef enum spank_state {
    SPANK_STOPPED    = 0,   // Protocol is paused
    SPANK_LISTENING,        // Listening (range query, neighbour advertisement)
    SPANK_ELECTING,         // 
    SPANK_RANGE_QUERYING,   // Performing range measurement
} spank_state_t;



struct spank_ctx;
typedef struct spank_ctx spank_ctx_t;

typedef struct spank_config {
    struct {
	spank_systicks_t timeout;
    } twr;
    struct {
	uint8_t retries;
    } packet;
    struct {
	spank_systicks_t election_timeout;
	spank_systicks_t scheduled_next_ranging;
    } orchestration;
} spank_config_t;


struct spank_ctx {
    spank_config_t *cfg;          //!< Pointer to configuration
    spank_io_t     *io;           //!< Pointer to IO driver
    spank_node_t   *neighbours;   //!< List of neigbours
    uint8_t         behaviour;    //!< Our behaviour (uav / anchor)

    spank_node_ctx_t node_ctx[SPANK_SIMULTANEOUS_NODE_TALK];
    
    spank_state_t state;          //!< State


    
    uint8_t seq;

    spank_timer_t         timer;

    struct {
	spank_timer_t timer;
	uint8_t       cmd;
	uint8_t       countdown;
	bool          cooldown;
    } navctrl;

    // Memory allocation
    spank_packet_t tx_packet;  //<! TX packet buffer
    spank_packet_t rx_packet;  //<! RX packet buffer
};

// Get configured transmit power (phr and shr+data)
void spank_tx_power(uint8_t *phr, uint8_t *sd);

void spank_init(spank_io_t *io);
uint8_t spank_my_behaviour(void);
spank_addr_t spank_whoami(void);
void spank_start(void);
void spank_stop(void);
void spank_loop(void) __attribute__ ((noreturn));
spank_config_t *spank_getconfig(void);


#define SPANK_SNIFFER_FRAME	1
#define SPANK_SNIFFER_PACKET	2

#define SPANK_SNIFFER_STATUS	0
#define SPANK_SNIFFER_ENABLE	1
#define SPANK_SNIFFER_DISABLE	2

int spank_sniffer(int type, int action);


static inline int
spank_send(spank_ctx_t *ctx,
	   spank_packet_t *pkt, size_t pktlen, spank_addr_t dst,
	   uint64_t *timestamp, uint8_t flags)
{
    return spank_io_send(ctx->io, pkt, pktlen, dst, timestamp,
			 SPANK_IO_SEND_ANSWER_EXPECTED);
}

int spank_send_navctrl(spank_addr_t dst, uint8_t cmd, uint8_t countdown);
static inline int spank_broadcast_navctrl(uint8_t cmd, uint8_t countdown) {
    return spank_send_navctrl(SPANK_ADDR_BROADCAST, cmd, countdown);
}

int spank_send_msg(spank_addr_t dst, char *msg);
static inline int spank_broadcast_msg(char *msg) {
    return spank_send_msg(SPANK_ADDR_BROADCAST, msg);
}

spank_node_ctx_t* spank_alloc_node_ctx(spank_ctx_t *ctx);
spank_node_ctx_t* spank_lookup_node_ctx(spank_ctx_t *ctx, spank_addr_t addr);
void spank_node_ctx_free(spank_node_ctx_t *nctx);
void spank_node_ctx_cleanup(spank_node_ctx_t *nctx);
void spank_node_ctx_destroy(spank_node_ctx_t *nctx);

#endif
