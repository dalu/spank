/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_SYSLOG_H__
#define __SPANK_SYSLOG_H__

#include "spank/osal.h"


/*
 * Log level
 */
#define SPANK_SYSLOG_NONE       0
#define SPANK_SYSLOG_FATAL	1
#define SPANK_SYSLOG_ERROR	2
#define SPANK_SYSLOG_WARNING	3
#define SPANK_SYSLOG_INFO	4
#define SPANK_SYSLOG_DEBUG 	5

#ifndef SPANK_SYSLOG_LEVEL
#define SPANK_SYSLOG_LEVEL	SPANK_SYSLOG_INFO
#endif


/*
 * Dedicated static logging
 */
#if (SPANK_SYSLOG_LEVEL >= SPANK_SYSLOG_FATAL)  || defined(SPANK_SYSLOG_RUNTIME)
#define SPANK_FATAL(fmt, ...)   do {					\
	SPANK_SYSLOG(SPANK_SYSLOG_FATAL, fmt, ##__VA_ARGS__);		\
	SPANK_ASSERT(0);						\
    } while(0)
#else
#define SPANK_FATAL(fmt, ...)
#endif

#if (SPANK_SYSLOG_LEVEL >= SPANK_SYSLOG_ERROR)  || defined(SPANK_SYSLOG_RUNTIME)
#define SPANK_ERROR(fmt, ...)						\
    SPANK_SYSLOG(SPANK_SYSLOG_ERROR, fmt, ##__VA_ARGS__)
#else
#define SPANK_ERROR(fmt, ...)
#endif

#if (SPANK_SYSLOG_LEVEL >= SPANK_SYSLOG_WARNING)|| defined(SPANK_SYSLOG_RUNTIME)
#define SPANK_WARNING(fmt, ...)						\
    SPANK_SYSLOG(SPANK_SYSLOG_WARNING, fmt, ##__VA_ARGS__)
#else
#define SPANK_WARNING(fmt, ...)
#endif

#if (SPANK_SYSLOG_LEVEL >= SPANK_SYSLOG_INFO)   || defined(SPANK_SYSLOG_RUNTIME)
#define SPANK_INFO(fmt, ...)						\
    SPANK_SYSLOG(SPANK_SYSLOG_INFO, fmt, ##__VA_ARGS__)
#else
#define SPANK_INFO(fmt, ...)
#endif

#if (SPANK_SYSLOG_LEVEL >= SPANK_SYSLOG_DEBUG)  || defined(SPANK_SYSLOG_RUNTIME)
#define SPANK_DEBUG(fmt, ...)						\
    SPANK_SYSLOG(SPANK_SYSLOG_DEBUG, fmt, ##__VA_ARGS__)
#else
#define SPANK_DEBUG(fmt, ...)
#endif


/*
 * Helper for unimplemented
 */
#define SPANK_UNIMPLEMENTED(fmt, ...)   do {				\
	SPANK_SYSLOG(SPANK_SYSLOG_WARNING,				\
		     "Unimplemented: " fmt, ##__VA_ARGS__);		\
    } while(0)



/*
 * Level conversion string <-> number
 */
int spank_syslog_level_lookup(char *str);
const char *const spank_syslog_string(int lvl);

#endif
