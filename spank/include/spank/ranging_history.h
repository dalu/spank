/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_RANGING_HISTORY_H__
#define __SPANK_RANGING_HISTORY_H__

#include "spank/osal.h"
#include "spank/config.h"
#include "spank/types.h"

typedef struct spank_ranging_history {
    spank_distance_t items[SPANK_RANGING_HISTORY_LENGTH];
    size_t size;
    size_t head;
} spank_ranging_history_t;

/**
 * Push a distance into the ranging history.
 *
 * @param h		ranging history
 * @param distance	distance (in centimeters)
 */
void spank_ranging_history_push(spank_ranging_history_t *h, spank_distance_t distance);

/**
 * Check if a distance can be considered as an outlier
 *
 * @param h		ranging history
 * @param distance	distance (in centimeters)
 * @param threshold	comparison threshold
 */
bool spank_ranging_history_outlier_q(spank_ranging_history_t *h, spank_distance_t distance, int threshold);

#endif
