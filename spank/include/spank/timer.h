/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_TIMER_H__
#define __SPANK_TIMER_H__

#include <stdlib.h>
#include <stdbool.h>
#include <sys/queue.h>

#include "spank/osal.h"
#include "spank/types.h"

/**
 * Timer implementation. 
 * This is not a thread-safe implementation
 */




typedef struct spank_timer {
    LIST_ENTRY(spank_timer) entries;
    spank_systicks_t   delta;
    spank_callback_t   cb;
    void              *data;
} spank_timer_t;


/**
 * Check if a timer is active.
 */
static inline
bool spank_timer_is_active(spank_timer_t *timer) {
    return timer->cb != NULL;
}


/**
 * Cancel a timer.
 *
 * @note If a timer is already cancelled, it's a no-op.
 *
 * @param timer			Timer
 */
void spank_timer_cancel(spank_timer_t *timer);


/**
 * Compute remaining delay of an active timer
 *
 * @param      timer		Timer
 * @param[out] delay		Remaining delay
 *
 * @return -1 the timer is not active
 * @return  0 the timer is active and trigering it overdue
 * @return  1 the timer is active and has a remaining delay
 */
int spank_timer_remaining_delay(spank_timer_t *timer, spank_systicks_t *delay);


/**
 * Set a timer.
 *
 * @note If timer is already active, it will be cancelled.
 *
 * @param timer			Timer
 * @param delay			Delay before trigerring the timer
 * @param cb			Callback to run when timer is triggered
 * @param data			Data for the callback
 */
void spank_timer_set(spank_timer_t *timer, spank_systicks_t delay,
		     spank_callback_t cb, void *data);


/**
 * Need to be called by the OS to make our timer ticks
 */
void spank_timer_ticking(void);


#endif
