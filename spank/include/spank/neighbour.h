/*
 * Copyright (c) 2018-2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef __SPANK_NEIGHBOUR_H__
#define __SPANK_NEIGHBOUR_H__


spank_node_t *spank_lookup_neighbour(spank_ctx_t *ctx, spank_addr_t addr);

void spank_neighbour_range_reset(spank_ctx_t *ctx);
spank_addr_t spank_neighbour_range_next(spank_ctx_t *ctx);

/**
 * @brief Notify that the election process was successful
 */
void spank_neighbour_elected(spank_ctx_t *ctx);

/**
 * @brief Look in our neighbour and find the new master to elect
 */
spank_addr_t spank_neighbour_elect(spank_ctx_t *ctx);


#endif
