#ifndef _TOKEN_SDSTWR_H
#define _TOKEN_SDSTWR_H

#include <stdint.h>
#include "spank/timer.h"
#include "spank/spank.h"

typedef struct spank_ctx spank_ctx_t;
struct spank_ctx;


typedef enum spank_node_state {
    SPANK_NODE_VIRGIN          = 0x0000,
    SPANK_NODE_TWR_POLLING     = 0x0101,
    SPANK_NODE_TWR_FINALIZING  = 0x0102,
    SPANK_NODE_TWR_NOTIFYING   = 0x0103,
    SPANK_NODE_TWR_ANSWERING   = 0x0111,
    SPANK_NODE_TWR_REPORTING   = 0x0112,
} spank_node_state_t;

#define SPANK_NODE_STATE_GROUP(s)  (s & 0xff00)
#define SPANK_NODE_STATE_IS_TWR_GROUP(x) (SPANK_NODE_STATE_GROUP(x) == 0x0100)
#define SPANK_NODE_STATE_IS_TWR_TAG_GROUP(x)    ((x & 0xfff0) == 0x0100)
#define SPANK_NODE_STATE_IS_TWR_ANCHOR_GROUP(x) ((x & 0xfff0) == 0x0110)


typedef struct spank_node_ctx spank_node_ctx_t;

typedef void (*spank_node_ctx_retry_f)(spank_node_ctx_t *nctx);

struct spank_node_ctx {
    spank_ctx_t             *ctx;	//!< back pointer to spank context
    spank_node_state_t       state;	//!< node context state 
    spank_timer_t            timer;	//!< handling timeout

    struct {
	spank_node_ctx_retry_f   fn;      //!< retry function
	uint8_t                  count;   //!< number of retry
    } retry;
    
    volatile spank_addr_t  node;

    volatile uint8_t       seq;

    struct {
	uint64_t t_sp;
	uint64_t t_rp;
	uint64_t t_sa;
	uint64_t t_ra;
	uint64_t t_sf;
	uint64_t t_rf;
    } twr;
} ;


int spank_start_twr_ranging(spank_ctx_t *ctx, spank_addr_t node_addr);
int spank_process_twr_answer(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx);
int spank_process_twr_report(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx);
int spank_process_twr_poll(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx);
int spank_process_twr_final(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx);
int spank_process_elect(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx);


#endif

