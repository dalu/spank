/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_CONFIG_H__
#define __SPANK_CONFIG_H__

#define SPANK_DRIVER_TX_TIMEOUT                 SPANK_MS_TO_SYSTICKS(5)
#define SPANK_NEIGHBOURS_MAXSIZE	10
#define SPANK_ADDR_SIZE                         8

#if !defined(SPANK_BYTE_ORDER)
#define SPANK_BYTE_ORDER __ORDER_BIG_ENDIAN__
#endif

#if !defined(SPANK_ENCODED_TS_BITS)
#define SPANK_ENCODED_TS_BITS 40
#endif

#if (SPANK_ENCODED_TS_BITS != 40) && (SPANK_ENCODED_TS_BITS != 64)
#error "SPANK_ENCODED_TS_BITS must be 40 or 64"
#endif

// Outlier rejection
#define SPANK_RANGING_HISTORY_LENGTH 16
#define SPANK_RANGING_OUTLIER_THRESHOLD 4


#define SPANK_SPEED_OF_LIGHT 		299792458ll

#define SPANK_ANTENNA_DELAY_METER_TO_CLOCK(x)				\
    (((x) * SPANK_DRIVER_TIME_CLOCK) / SPANK_SPEED_OF_LIGHT)


#define SPANK_SIMULTANEOUS_NODE_TALK 		1
#define SPANK_IO_RX_QUEUE_SIZE			3
#define SPANK_IO_RX_PACKET_MAXSIZE		120

/*          DWM1001         vs  Crazyflie
 *  MCU  :  NRF52 = 64 MHz      STM32 F405 = 168 MHz
 *  BUS  :  SPI   =  8 MHz      SPI        = 20 MHz (max of dw1000)
 *  ticks:  100Hz               1000Hz
 *
 * => *Minimum* required timeout will differ
 */
/* ??  DWM1001 ~ 30/100, CF2 ~ 3/10 ?? */

#if !defined(SPANK_TIMEOUT_TWR)
#define SPANK_TIMEOUT_TWR            SPANK_MS_TO_SYSTICKS(30)
#endif

#if !defined(SPANK_TIMEOUT_ELECTION)
#define SPANK_TIMEOUT_ELECTION       SPANK_MS_TO_SYSTICKS(50)
#endif

#if !defined(SPANK_SCHEDULED_NEXT_RANGING)
#define SPANK_SCHEDULED_NEXT_RANGING SPANK_MS_TO_SYSTICKS(500)
#endif

#if !defined(SPANK_PACKET_RETRIES)
#define SPANK_PACKET_RETRIES	     2
#endif

#endif
