/*
 * Copyright (c) 2024
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef __SPANK_UTILS_H__
#define __SPANK_UTILS_H__

#include <stdint.h>

#if defined(SPANK_DISTANCE_LOG)
#include <spank/types.h>
#include <spank/syslog.h>
#else
#define SPANK_DISTANCE_LOG(...)
#endif

/**
 * Population count (popcount)
 *
 * @param x		32-bit integer
 * @return numbers of bits at 1
 */
int spank_popcount32(uint32_t x);

/**
 * Population count (popcount)
 *
 * @param x		64-bits integer
 * @return numbers of bits at 1
 */
int spank_popcount64(uint64_t x);

/**
 * Compute 16-bit checksum. CRC16-CCIT.
 *
 * @param data		Data buffer on which to compute checksum.
 * @param len		Data length.
 * @return crc
 */
uint16_t spank_crc16_ccitt(const uint8_t *data, size_t len);


/**
 * Compute 16-bit fletcher checksum.
 *
 * @param data		Data buffer on which to compute checksum.
 * @param len		Data length.
 * @return checksum
 */
uint16_t spank_fletcher16(const uint8_t *data, size_t len);

/**
 * Compute 16-bit checksum.
 * (current implementation is fletcher16)
 *
 * @param data		Data buffer on which to compute checksum.
 * @param len		Data length.
 * @return checksum
 */
uint16_t spank_cksum16(const uint8_t *data, size_t len);


/**
 * Pseudo random number.
 * Current implementation is splitmix64
 *
 * @return random number
 */
uint64_t spank_random(void);


/**
 * Set initial seed
 *
 * @param seed	Initial seed
 */
void spank_random_seed(uint64_t seed);


static inline
int32_t spank_distance_twr(uint64_t t_sp,  uint64_t t_rp,
			   uint64_t t_sr,  uint64_t t_rr,
			   uint64_t speed, uint64_t freq,
			   uint64_t time_msk)
{
    uint64_t t_round       = (t_rr - t_sp) & time_msk;
    uint64_t t_reply       = (t_sr - t_rp) & time_msk;
    int64_t  t_propagation = (int64_t)(t_round - t_reply) / 2;
    int64_t  dist_mm       = t_propagation
	                   * 1000 * (int64_t)speed / (int64_t)freq;

    SPANK_DISTANCE_LOG("dist=%d.%03d"
	       " ("
	       "sp="    SPANK_FMTts ", " "rp="    SPANK_FMTts ", "
	       "sr="    SPANK_FMTts ", " "rr="    SPANK_FMTts ", "
	       "round=" SPANK_FMTts ", " "reply=" SPANK_FMTts ", "
	       "prop=%" PRIi64      ", " "dist=%" PRIi64
	       ")",
	       (int)(dist_mm / 1000), abs(dist_mm) % 1000,
	       SPANK_FMT_TS(t_sp),    SPANK_FMT_TS(t_rp),
	       SPANK_FMT_TS(t_sr),    SPANK_FMT_TS(t_rr),
	       SPANK_FMT_TS(t_round), SPANK_FMT_TS(t_reply),
	       t_propagation, dist_mm);

    return dist_mm;
}



static inline
int32_t spank_distance_sds_twr(uint64_t t_sp,  uint64_t t_rp,
			       uint64_t t_sa,  uint64_t t_ra,
			       uint64_t t_sf,  uint64_t t_rf,
			       uint64_t speed, uint64_t freq,
			       uint64_t time_msk) {

#if 1
    uint64_t t_round1      = (t_ra - t_sp) & time_msk;
    uint64_t t_reply1      = (t_sa - t_rp) & time_msk;
    uint64_t t_round2      = (t_rf - t_sa) & time_msk;
    uint64_t t_reply2      = (t_sf - t_ra) & time_msk;
    int64_t  t_propagation =
	((t_round1 * t_round2) - (t_reply1 * t_reply2)) /
	(t_round1 + t_round2 + t_reply1 + t_reply2);
#else
    uint64_t t_round1      = (t_ra - t_sp) & time_msk;
    uint64_t t_reply1      = (t_sa - t_rp) & time_msk;
    uint64_t t_round2      = (t_rf - t_sa) & time_msk;
    uint64_t t_reply2      = (t_sf - t_ra) & time_msk;
    int64_t  t_propagation = ((t_round1 - t_reply1) +
			      (t_round2 - t_reply2)) / 4;
#endif

    int64_t  dist_mm       = t_propagation
	                   * 1000 * (int64_t)speed / (int64_t)freq;
    
    SPANK_DISTANCE_LOG("dist=%d.%03d"
	       " ("
	       "sp="     SPANK_FMTts ", " "rp="     SPANK_FMTts ", "
	       "sr="     SPANK_FMTts ", " "rr="     SPANK_FMTts ", "
	       "sf="     SPANK_FMTts ", " "rf="     SPANK_FMTts ", "
	       "round1=" SPANK_FMTts ", " "reply1=" SPANK_FMTts ", "
	       "round2=" SPANK_FMTts ", " "reply2=" SPANK_FMTts ", "
	       "prop=%"  PRIi64
	       ")",
	       (int)(dist_mm / 1000), abs(dist_mm) % 1000,
	       SPANK_FMT_TS(t_sp),    SPANK_FMT_TS(t_rp),
	       SPANK_FMT_TS(t_sa),    SPANK_FMT_TS(t_ra),
	       SPANK_FMT_TS(t_sf),    SPANK_FMT_TS(t_rf),
	       SPANK_FMT_TS(t_round1), SPANK_FMT_TS(t_reply1),
	       SPANK_FMT_TS(t_round2), SPANK_FMT_TS(t_reply2),
	       t_propagation);
    
    return dist_mm;
}


#endif
