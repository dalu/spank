/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_NODE_H__
#define __SPANK_NODE_H__

#include "spank/osal.h"
#include "spank/io.h"
#include "spank/types.h"
#include "spank/ranging_history.h"



/**
 * @brief Flag, indicate node for which range can be queried
 */
#define SPANK_NODE_BEHAVIOUR_RANGING   0x01
/**
 * @brief Flag, indicate node being able to initiate a ranging calculation
 */
#define SPANK_NODE_BEHAVIOUR_ELECTABLE 0x02
/**
 * @brief Flag, indicate node for which information is permanently 
 *        stored in memory
 */
#define SPANK_NODE_BEHAVIOUR_PINNED    0x08
/**
 * @brief Flag, indicate node is static (no navigation capacity)
 */
#define SPANK_NODE_BEHAVIOUR_STATIC    0x10


/**
 * @brief Set of flag, for UAV (ranging + electable)
 */
#define SPANK_NODE_BEHAVIOUR_UAV					\
    (SPANK_NODE_BEHAVIOUR_RANGING | SPANK_NODE_BEHAVIOUR_ELECTABLE)
/**
 * @brief Set of flag, for anchor (ranging)
 */
#define SPANK_NODE_BEHAVIOUR_ANCHOR					\
    (SPANK_NODE_BEHAVIOUR_RANGING | SPANK_NODE_BEHAVIOUR_STATIC)

/**
 * @brief Helper for defining static UAV node in neighbour table
 */
#define SPANK_NODE_DEF_UAV(addr)					\
    { .address = addr, .behaviour = SPANK_NODE_BEHAVIOUR_UAV    |	\
                                    SPANK_NODE_BEHAVIOUR_PINNED }
/**
 * @brief Helper for defining static Anchor node in neighbour table
 */
#define SPANK_NODE_DEF_ANCHOR(addr)					\
    { .address = addr, .behaviour = SPANK_NODE_BEHAVIOUR_ANCHOR |	\
                                    SPANK_NODE_BEHAVIOUR_PINNED }



typedef struct spank_node {
    spank_addr_t     address;
    spank_systicks_t last_seen;
    uint8_t          behaviour;
    spank_position_t position;
    float32_t        clock_tracking; // in ppm
    struct {
	int tried;
    } electing;
    struct {
	spank_ranging_history_t history;
	float32_t               distance;
    } ranging;
} spank_node_t;


bool spank_node_update_ranging(spank_node_t *n, float32_t distance,
			       spank_posture_t *posture);


#endif
