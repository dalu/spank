/*
 * Copyright (c) 2019-2020,2024
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: 
 */

#ifndef __SPANK__TYPES_H__
#define __SPANK__TYPES_H__

#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <sys/types.h>
#include "ieee802154.h"
#include "spank/config.h"


/*
 * Address
 */

typedef ieee802154_panid_t      spank_panid_t;

#if SPANK_ADDR_SIZE == 2

typedef ieee802154_addr_short_t   spank_addr_t;
#define SPANK_ADDR_BROADCAST	0xffff
#define SPANK_ADDR_NONE		0x0000
#ifndef __SPANK_FMTaddr
#define __SPANK_FMTaddr		"%04x"
#endif
#ifndef __SPANK_FMT_ADDR
#define __SPANK_FMT_ADDR(addr)	(addr)
#endif

#elif SPANK_ADDR_SIZE == 8

typedef ieee802154_addr_extended_t spank_addr_t;
#define SPANK_ADDR_BROADCAST	0xffffffffffffffff
#define SPANK_ADDR_NONE		0x0000000000000000
#ifndef __SPANK_FMTaddr
#define __SPANK_FMTaddr		"%04x:%04x:%04x:%04x"
#endif
#ifndef __SPANK_FMT_ADDR
#define __SPANK_FMT_ADDR(addr)	((uint16_t) ((addr) >> 48)),	\
				((uint16_t) ((addr) >> 32)),	\
				((uint16_t) ((addr) >> 16)),	\
				((uint16_t) ((addr) >>  0))
#endif
#else

#error "Unsupported address size for SPANK_ADDR_SIZE"

#endif

#ifndef _SPANK_FMTaddr
#define _SPANK_FMTaddr		__SPANK_FMTaddr
#endif

#ifndef _SPANK_FMT_ADDR
#define _SPANK_FMT_ADDR(addr)	__SPANK_FMT_ADDR(addr)
#endif

#ifndef SPANK_FMTaddr
#define SPANK_FMTaddr 		_SPANK_FMTaddr 
#endif

#ifndef SPANK_FMT_ADDR
#define SPANK_FMT_ADDR(addr) 	_SPANK_FMT_ADDR(addr)
#endif


#define SPANK_ADDR_COPY(dst, src)				\
    memcpy(dst, src, SPANK_ADDR_SIZE);


/*
 * Timestamp
 */
typedef uint64_t spank_ts;

#define SPANK_TS_UNDEFINED (~((uint64_t)0))
#define SPANK_TS_IS_UNDEFINED(ts) ((ts) == SPANK_TS_UNDEFINED)

#define _SPANK_FMTts		"%010" PRIx64
#define _SPANK_FMT_TS(ts)	(ts)

#ifndef SPANK_FMTts
#define SPANK_FMTts _SPANK_FMTts
#endif

#ifndef SPANK_FMT_TS
#define SPANK_FMT_TS(ts) _SPANK_FMT_TS(ts)
#endif


/*
 * Encoded timestamp
 */
typedef unsigned char spank_encoded_ts64_t[8];
typedef unsigned char spank_encoded_ts40_t[5];


#if SPANK_BYTE_ORDER == __ORDER_BIG_ENDIAN__

static inline void
spank_encoded_ts64_write(uint64_t ts, spank_encoded_ts64_t buf)
{
    for (int i = 0 ; i < sizeof(spank_encoded_ts64_t) ; i++)
	buf[i] = (ts >> ((sizeof(spank_encoded_ts64_t) - 1 - i) * 8)) & 0xff;
}
static inline void
spank_encoded_ts40_write(uint64_t ts, spank_encoded_ts40_t buf)
{
    for (int i = 0 ; i < sizeof(spank_encoded_ts40_t) ; i++)
	buf[i] = (ts >> ((sizeof(spank_encoded_ts40_t) - 1 - i) * 8)) & 0xff;
}

static inline uint64_t 
spank_encoded_ts64_read(spank_encoded_ts64_t buf)
{
    uint64_t ts = 0;
    for (int i = 0 ; i < sizeof(spank_encoded_ts64_t) ; i++)
	ts = (ts << 8) | buf[i];
    return ts;
}
	    
static inline uint64_t 
spank_encoded_ts40_read(spank_encoded_ts64_t buf)
{
    uint64_t ts = 0;
    for (int i = 0 ; i < sizeof(spank_encoded_ts40_t) ; i++)
	ts = (ts << 8) | buf[i];
    return ts;
}

#elif SPANK_BYTE_ORDER == __ORDER_LITTLE_ENDIAN__

static inline void
spank_encoded_ts64_write(uint64_t ts, spank_encoded_ts64_t buf)
{
    for (int i = 0 ; i < sizeof(spank_encoded_ts64_t) ; i++)
	buf[i] = (ts >> (i * 8)) & 0xff;
}
static inline void
spank_encoded_ts40_write(uint64_t ts, spank_encoded_ts40_t buf)
{
    for (int i = 0 ; i < sizeof(spank_encoded_ts40_t) ; i++)
	data[i] = (ts >> (i * 8)) & 0xff;
}

static inline uint64_t 
spank_encoded_ts64_read(spank_encoded_ts64_t buf)
{
    uint64_t ts = 0;
    for (int i = 0 ; i < sizeof(spank_encoded_ts64_t) ; i++)
	ts = (ts << 8) | buf[sizeof(spank_encoded_ts64_t) - 1 - i];
    return ts;
}

static inline uint64_t 
spank_encoded_ts40_read(spank_encoded_ts40_t buf)
{
    uint64_t ts = 0;
    for (int i = 0 ; i < sizeof(spank_encoded_ts40_t) ; i++)
	ts = (ts << 8) | buf[sizeof(spank_encoded_ts40_t) - 1 - i];
    return ts;
}


#else
#  error "unknown byte order"
#endif

#if SPANK_ENCODED_TS_BITS == 40
typedef spank_encoded_ts40_t spank_encoded_ts_t;
static inline uint64_t 
spank_encoded_ts_read(spank_encoded_ts_t buf)
{
    return spank_encoded_ts40_read(buf);
}
static inline void
spank_encoded_ts_write(uint64_t ts, spank_encoded_ts_t buf)
{
    spank_encoded_ts40_write(ts, buf);
}
#elif SPANK_ENCODED_TS_BITS == 64
typedef spank_encoded_ts64_t spank_encoded_ts_t;
static inline uint64_t 
spank_encoded_ts_read(spank_encoded_ts_t buf) {
    return spank_encoded_ts64_read(buf);
}
static inline void
spank_encoded_ts_write(uint64_t ts, spank_encoded_ts_t buf)
{
    spank_encoded_ts64_write(ts, buf);
}
#else
#  error "unsupport SPANK_ENCODED_TS_BITS value"
#endif





typedef void (*spank_callback_t)(void *data);



/**
 * Signed 32-bit integer (with user defined NaN value).
 */
typedef int32_t spank_int32_t;
#define SPANK_PRIint32  PRIi32
#define SPANK_INT32_MAX (INT32_MAX-1)
#define SPANK_INT32_MIN (-SPANK_INT32_MAX)
#define SPANK_INT32_NAN INT32_MIN
#define SPANK_INT32_INF INT32_MAX
#define SPANK_INT32_IS_NAN(v) ((v) == SPANK_INT32_NAN)

static inline bool spank_int32_from_int(spank_int32_t *i32, int i) {
    if      (i > SPANK_INT32_MAX) { *i32 = +SPANK_INT32_INF; return false; }
    else if (i < SPANK_INT32_MIN) { *i32 = -SPANK_INT32_INF; return false; }
    else                          { *i32 = i;                return true;  }
}


/**
 * Signed 16-bit integer (with user defined NaN value).
 */
typedef int16_t spank_int16_t;
#define SPANK_PRIint16  PRIi16
#define SPANK_INT16_MAX (INT16_MAX-1)
#define SPANK_INT16_MIN (-SPANK_INT16_MAX)
#define SPANK_INT16_NAN INT16_MIN
#define SPANK_INT16_INF INT16_MAX
#define SPANK_INT16_IS_NAN(v) ((v) == SPANK_INT16_NAN)

static inline bool spank_int16_from_int(spank_int16_t *i16, int i) {
    if      (i > SPANK_INT16_MAX) { *i16 = +SPANK_INT16_INF; return false; }
    else if (i < SPANK_INT16_MIN) { *i16 = -SPANK_INT16_INF; return false; }
    else                          { *i16 = i;                return true;  }
}


/**
 * RSSI (unsigned int)
 * Expressed in 0.01 dBm
 */
typedef spank_int16_t spank_rssi_t;
#define SPANK_PRIrssi  PRIu16
#define SPANK_RSSI_MAX SPANK_INT16_MAX
#define SPANK_RSSI_MIN SPANK_INT16_MIN
#define SPANK_RSSI_NAN SPANK_INT16_NAN
#define SPANK_RSSI_INF SPANK_INT16_INF
#define SPANK_RSSI_IS_NAN(v) ((v) == SPANK_RSSI_INF)

static inline bool spank_rssi_from_int(spank_int16_t *s, int i) {
    return spank_int16_from_int(s, i);
}

/**
 * Distance (signed int)
 * Expressed in centimeters.
 */
typedef spank_int32_t spank_distance_t;
#define SPANK_PRIdistance  SPANK_PRIint32
#define SPANK_DISTANCE_MAX SPANK_INT32_MAX
#define SPANK_DISTANCE_MIN SPANK_INT32_MIN
#define SPANK_DISTANCE_NAN SPANK_INT32_NAN
#define SPANK_DISTANCE_INF SPANK_INT32_INF
#define SPANK_DISTANCE_IS_NAN(v) ((v) == SPANK_DISTANCE_NAN)

static inline bool spank_distance_from_int(spank_distance_t *s, int i) {
    return spank_int32_from_int(s, i);
}


/**
 * Stddev (signed int)
 * Expressed in 1/100.
 */
typedef spank_int16_t spank_stddev_t;
#define SPANK_PRIstddev  SPANK_PRIint16
#define SPANK_STDDEV_MAX SPANK_INT16_MAX
#define SPANK_STDDEV_MIN SPANK_INT16_MIN
#define SPANK_STDDEV_NAN SPANK_INT16_NAN
#define SPANK_STDDEV_INF SPANK_INT16_INF
#define SPANK_STDDEV_IS_NAN(v) ((v) == SPANK_STDDEV_NAN)

static inline bool spank_stddev_from_int(spank_stddev_t *s, int i) {
    return spank_int16_from_int(s, i);
}


/**
 * Position (signed int)
 * Fields are expressed in centimeters.
 */
typedef union spank_position {
    struct {
	spank_int16_t x;
	spank_int16_t y;
	spank_int16_t z;
    };
    spank_int16_t axis[3];
} spank_position_t;
#define SPANK_PRIposition_axis  SPANK_PRIint16
#define SPANK_POSITION_AXIS_MAX SPANK_INT16_MAX
#define SPANK_POSITION_AXIS_MIN SPANK_INT16_MIN
#define SPANK_POSITION_AXIS_NAN SPANK_INT16_NAN
#define SPANK_POSITION_AXIS_INF SPANK_INT16_INF
#define SPANK_POSITION_AXIS_IS_NAN(v) ((v) == SPANK_POSITION_AXIS_NAN)
#define SPANK_POSITION_NAN					\
    { .x = SPANK_POSITION_AXIS_NAN,				\
      .y = SPANK_POSITION_AXIS_NAN,				\
      .z = SPANK_POSITION_AXIS_NAN }
#define SPANK_POSITION_IS_NAN(v)				\
    (SPANK_POSITION_AXIS_IS_NAN((v).x) ||			\
     SPANK_POSITION_AXIS_IS_NAN((v).y) ||			\
     SPANK_POSITION_AXIS_IS_NAN((v).z))

static inline void spank_position_set_nan(spank_position_t *s) {
    s->x = s->y = s->z = SPANK_POSITION_AXIS_NAN;
}
static inline void spank_position_set_zero(spank_position_t *s) {
    s->x = s->y = s->z = 0;
}
static inline bool
spank_position_from_int(spank_position_t *s, int x, int y , int z) {
    return spank_int16_from_int(&s->x, x) &&
   	   spank_int16_from_int(&s->y, y) &&
	   spank_int16_from_int(&s->z, z);
}


/**
 * Velocity (signed int)
 * Fields are expressed in centimeters/second.
 */
typedef union spank_velocity {
    struct {
	spank_int16_t x;
	spank_int16_t y;
	spank_int16_t z;
    };
    spank_int16_t axis[3];
} spank_velocity_t;
#define SPANK_PRIvelocity_axis  SPANK_PRIint16
#define SPANK_VELOCITY_AXIS_MAX SPANK_INT16_MAX
#define SPANK_VELOCITY_AXIS_MIN SPANK_INT16_MIN
#define SPANK_VELOCITY_AXIS_NAN SPANK_INT16_NAN
#define SPANK_VELOCITY_AXIS_INF SPANK_INT16_INF
#define SPANK_VELOCITY_AXIS_IS_NAN(v) ((v) == SPANK_VELOCITY_AXIS_NAN)
#define SPANK_VELOCITY_NAN					\
    { .x = SPANK_VELOCITY_AXIS_NAN,				\
      .y = SPANK_VELOCITY_AXIS_NAN,				\
      .z = SPANK_VELOCITY_AXIS_NAN }
#define SPANK_VELOCITY_IS_NAN(v)				\
    (SPANK_VELOCITY_AXIS_IS_NAN((v).x) ||			\
     SPANK_VELOCITY_AXIS_IS_NAN((v).y) ||			\
     SPANK_VELOCITY_AXIS_IS_NAN((v).z))
#define SPANK_VELOCITY_IS_ZERO(v)				\
    (((v).x == 0) && ((v).y == 0) && ((v).z == 0))

static inline void spank_velocity_set_nan(spank_velocity_t *s) {
    s->x = s->y = s->z = SPANK_VELOCITY_AXIS_NAN;
}
static inline void spank_velocity_set_zero(spank_velocity_t *s) {
    s->x = s->y = s->z = 0;
}
static inline bool
spank_velocity_from_int(spank_velocity_t *s, int x, int y , int z) {
    return spank_int16_from_int(&s->x, x) &&
   	   spank_int16_from_int(&s->y, y) &&
	   spank_int16_from_int(&s->z, z);
}


/**
 * Stddev on xyz (signed int)
 * Fields are expressed in 1/100.
 */
typedef union spank_stddev_xyz {
    struct {
	spank_int16_t x;
	spank_int16_t y;
	spank_int16_t z;
    };
    spank_int16_t axis[3];
} spank_stddev_xyz_t;
#define SPANK_PRIstddev_axis  SPANK_PRIint16
#define SPANK_STDDEV_AXIS_MAX SPANK_INT16_MAX
#define SPANK_STDDEV_AXIS_MIN SPANK_INT16_MIN
#define SPANK_STDDEV_AXIS_NAN SPANK_INT16_NAN
#define SPANK_STDDEV_AXIS_INF SPANK_INT16_INF
#define SPANK_STDDEV_AXIS_IS_NAN(v) ((v) == SPANK_STDDEV_AXIS_NAN)
#define SPANK_STDDEV_XYZ_NAN					\
    { .x = SPANK_STDDEV_AXIS_NAN,				\
      .y = SPANK_STDDEV_AXIS_NAN,				\
      .z = SPANK_STDDEV_AXIS_NAN }
#define SPANK_STDDEV_XYZ_IS_NAN(v)				\
    (SPANK_STDDEV_AXIS_IS_NAN((v).x) ||				\
     SPANK_STDDEV_AXIS_IS_NAN((v).y) ||				\
     SPANK_STDDEV_AXIS_IS_NAN((v).z))

static inline void spank_stddev_xyz_set_nan(spank_stddev_xyz_t *s) {
    s->x = s->y = s->z = SPANK_STDDEV_AXIS_NAN;
}
static inline void spank_stddev_xyz_set_zero(spank_stddev_xyz_t *s) {
    s->x = s->y = s->z = 0;
}
static inline bool
spank_stddev_xyz_from_int(spank_stddev_xyz_t *s, int x, int y , int z) {
    return spank_int16_from_int(&s->x, x) &&
   	   spank_int16_from_int(&s->y, y) &&
	   spank_int16_from_int(&s->z, z);
}


/**
 * Posture (signed int)
 */
typedef struct spank_posture {
    struct {
	spank_position_t   value;
	spank_stddev_xyz_t stddev;
    } position;
    struct {
	spank_velocity_t   value;
	spank_stddev_xyz_t stddev;
    } velocity;
} spank_posture_t;


#if defined(CONFIG_SPANK_PACKED_PACKET)

/**
 * Distance (compacted & packed)
 * 
 *  - unit       : centimeter
 *  - integer    : unsigned signed 16 bits
 *  - value range: 0..600_00
 *  - special    : nan = 65535, +inf = 65534
 */
typedef uint16_t     spank_packed_distance_t;
#define SPANK_PACKED_DISTANCE_MAX 60000
#define SPANK_PACKED_DISTANCE_MIN 0
#define SPANK_PACKED_DISTANCE_INF 65534
#define SPANK_PACKED_DISTANCE_NAN 65535


/**
 * Stddev (compacted & packed) 
 *
 * Value is:
 *  - integer    : signed 6 bits
 *  - value range: -30..30
 *  - special    : nan = -32, +inf = 31, -inf = -31
 *
 * Factor is a 2-bit flag
 *  - 0 = x  1
 *  - 1 = x  5
 *  - 2 = x 10
 *  - 3 = x 25
 *
 * @note If stddev is not available structure is 0xFF filled
 * @note sizeof(spank_packed_stddev_t) = 1 byte
 */
typedef union spank_packed_stddev {
    struct {
	int8_t  value  : 6;
	uint8_t factor : 2;
    };
    uint8_t raw;
} __attribute__((packed)) spank_packed_stddev_t;
#define SPANK_PACKED_STDDEV_MAX  30
#define SPANK_PACKED_STDDEV_MIN (-30)


/**
 * Velocity (compacted & packed).
 *
 * Fields x, y, and z:
 *  - unit       : centimeter / second
 *  - integer    : signed 13 bits
 *  - value range: -40_00..40_00
 *  - special    : nan = -4096, +inf = 4095, -inf= -4095
 *
 * @note If position is not available structure is 0xFF filled
 * @note sizeof(spank_packed_velocity_t) = 5 bytes
 */
typedef union spank_packed_velocity { 
    uint8_t data[5];
} __attribute__((packed)) spank_packed_velocity_t;
#define SPANK_PACKED_VELOCITY_MAX   4000
#define SPANK_PACKED_VELOCITY_MIN (-4000)
#define SPANK_PACKED_VELOCITY_INF   4095
#define SPANK_PACKED_VELOCITY_NAN (-4096)
#define SPANK_PACKED_VELOCITY_MSK 0x1FFF
#define SPANK_PACKED_VELOCITY_BITS 13


/**
 * Position (compacted & packed) 
 *
 * Fields x, y, and z:
 *  - unit       : centimeter
 *  - integer    : signed 18 bits
 *  - value range: -1000_00..1000_00
 *  - special    : nan = -131072, +inf = 131071, -inf= -131071
 *
 * @note If position is not available structure is 0xFF filled
 * @note sizeof(spank_packed_position_t) = 7 bytes
 */
typedef union spank_packed_position {
    uint8_t data[7];
} __attribute__((packed)) spank_packed_position_t;
#define SPANK_PACKED_POSITION_MAX   100000
#define SPANK_PACKED_POSITION_MIN (-100000)
#define SPANK_PACKED_POSITION_INF   131071
#define SPANK_PACKED_POSITION_NAN (-131072)
#define SPANK_PACKED_POSITION_MSK 0x3FFFF
#define SPANK_PACKED_POSITION_BITS 18


#else

typedef spank_distance_t     spank_packed_distance_t;
typedef spank_stddev_t	     spank_packed_stddev_t;
typedef spank_velocity_t     spank_packed_velocity_t;
typedef spank_position_t     spank_packed_position_t;


#endif


/**
 * Stddev on xyz (compacted & packed).
 * See spank_packed_stddev_t for field details.
 *
 * @note If stddev is not available structure is 0xFF filled
 * @note sizeof(spank_packed_stddev_xyz_t) = 3 bytes
 */
typedef struct spank_packed_stddev_xyz {
    spank_packed_stddev_t x;
    spank_packed_stddev_t y;
    spank_packed_stddev_t z;
} __attribute__((packed)) spank_packed_stddev_xyz_t;


/**
 * Posture (compacted & packed)
 */
typedef struct spank_packed_posture {
    struct {
	spank_packed_position_t   value;
	spank_packed_stddev_xyz_t stddev;
    } position;
    struct {
	spank_packed_velocity_t   value;
	spank_packed_stddev_xyz_t stddev;
    } velocity;
} __attribute__((packed)) spank_packed_posture_t;


/**
 * Write posture information in a packed format.
 * @note a negative return value doesn't guarantee that no data 
 *       has been written to the pointer region.
 *
 * @param posture
 * @param data
 * @param length
 *
 * @return < 0 in case of error
 * @return written data length
 */
ssize_t spank_posture_pack(spank_posture_t *posture, void *data, size_t length);


/**
 * Read posture information from a packed format.
 * @note a negative return value doesn't guarantee that no data 
 *       has been written to the posture structure.
 *
 * @param posture
 * @param data
 * @param length
 *
 * @return < 0 in case of error
 * @return read data length
 */
ssize_t spank_posture_unpack(spank_posture_t *posture, void *data, size_t length);



#endif
