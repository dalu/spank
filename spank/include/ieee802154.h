#ifndef __IEEE802154_H__
#define __IEEE802154_H__


#define IEEE802154_MHR_MAXSIZE              23
#ifdef IEEE802154_WITH_SECURITY
#define IEEE802154_MHR_AUX_SEC_MAXISZE      14
#define IEEE802154_MIC_MAXSIZE              16
#endif
#define IEEE802154_FRAME_MAXSIZE            127

enum ieee802154_version {
    IEEE802154_VERSION_802154_2003		= 0x0,
    IEEE802154_VERSION_802154_2006		= 0x1,
    IEEE802154_VERSION_802154			= 0x2,
    IEEE802154_VERSION_RESERVED			= 0x3,
};

enum ieee802154_security_level {
    IEEE802154_SECURITY_LEVEL_NONE		= 0x0,
    IEEE802154_SECURITY_LEVEL_MIC_32		= 0x1,
    IEEE802154_SECURITY_LEVEL_MIC_64		= 0x2,
    IEEE802154_SECURITY_LEVEL_MIC_128		= 0x3,
    IEEE802154_SECURITY_LEVEL_ENC		= 0x4,
    IEEE802154_SECURITY_LEVEL_ENC_MIC_32	= 0x5,
    IEEE802154_SECURITY_LEVEL_ENC_MIC_64	= 0x6,
    IEEE802154_SECURITY_LEVEL_ENC_MIC_128	= 0x7,
};

enum ieee802154_key_id_mode {
    IEEE802154_KEY_ID_MODE_IMPLICIT		= 0x0,
    IEEE802154_KEY_ID_MODE_INDEX		= 0x1,
    IEEE802154_KEY_ID_MODE_SRC_4_INDEX		= 0x2,
    IEEE802154_KEY_ID_MODE_SRC_8_INDEX		= 0x3,
};

enum ieee802154_addressing_mode {
    IEEE802154_ADDR_MODE_NONE			= 0x0,
    IEEE802154_ADDR_MODE_SIMPLE			= 0x1,
    IEEE802154_ADDR_MODE_SHORT			= 0x2,
    IEEE802154_ADDR_MODE_EXTENDED		= 0x3,
};

enum ieee802154_frame_type {
    IEEE802154_FRAME_TYPE_BEACON		= 0x0,
    IEEE802154_FRAME_TYPE_DATA			= 0x1,
    IEEE802154_FRAME_TYPE_ACK			= 0x2,
    IEEE802154_FRAME_TYPE_MAC_COMMAND		= 0x3,
    IEEE802154_FRAME_TYPE_LLDN			= 0x4,
    IEEE802154_FRAME_TYPE_MULTIPURPOSE		= 0x5,
    IEEE802154_FRAME_TYPE_RESERVED		= 0x6,
};


typedef uint16_t ieee802154_panid_t;
typedef uint8_t  ieee802154_addr_simple_t;
typedef uint16_t ieee802154_addr_short_t;
typedef uint64_t ieee802154_addr_extended_t;


typedef struct ieee802154_addressing {
    enum ieee802154_addressing_mode mode;
    ieee802154_panid_t pan_id;
    union {
	ieee802154_addr_simple_t   addr_simple;
	ieee802154_addr_short_t    addr_short;
	ieee802154_addr_extended_t addr_extended;
    };
} ieee802154_addressing_t;


typedef union ieee802154_fcf {
    uint16_t value;
    struct {
        uint16_t type             : 3;
        uint16_t security_enabled : 1;
        uint16_t frame_pending    : 1;
        uint16_t ack_requested    : 1;
        uint16_t pan_id_comp      : 1;
        uint16_t reserved         : 3;
        uint16_t dst_addr_mode    : 2;
        uint16_t version          : 2;
        uint16_t src_addr_mode    : 2;
    };
} __attribute__((packed)) ieee802154_fcf_t;


struct ieee802154_fcf_seq {
    ieee802154_fcf_t fcf;
    uint8_t seq;
} __attribute__((packed));

struct ieee802154_addr {
    union {
	ieee802154_addr_simple_t   addr_simple;
	ieee802154_addr_short_t    addr_short;
	ieee802154_addr_extended_t addr_extended;
    };
} __attribute__((packed));



struct ieee802154_address_field_compressed {
    struct ieee802154_addr addr;
} __attribute__((packed));

struct ieee802154_address_field_full {
    ieee802154_panid_t pan_id;
    struct ieee802154_addr addr;
} __attribute__((packed));

struct ieee802154_address_field {
    union {
	struct ieee802154_address_field_full       full;
	struct ieee802154_address_field_compressed compressed;
    };
} __attribute__((packed));


struct ieee802154_security_control_field {
    uint8_t security_level	:3;
    uint8_t key_id_mode		:2;
    uint8_t reserved		:3;
} __attribute__((packed));

struct ieee802154_key_identifier_field {
    union {
	/* mode_0 being implicit, it holds no info here */
	struct {
	    uint8_t key_index;
	} mode_1;
	
	struct {
	    uint8_t key_src[4];
	    uint8_t key_index;
	} mode_2;
	
	struct {
	    uint8_t key_src[8];
	    uint8_t key_index;
	} mode_3;
    };
} __attribute__((packed));


struct ieee802154_aux_security_hdr {
    struct ieee802154_security_control_field control;
    uint32_t frame_counter;
    struct ieee802154_key_identifier_field kif;
} __attribute__((packed));

struct ieee802154_mhr {
    struct ieee802154_fcf_seq          *fcf_seq;
    struct ieee802154_address_field    *dst_addr;
    struct ieee802154_address_field    *src_addr;
#ifdef IEEE802154_WITH_SECURITY
    struct ieee802154_aux_security_hdr *aux_sec;
#endif
};


#endif
