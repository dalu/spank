#include <strings.h>
#include "spank/syslog.h"

static const char *const spank_syslog_lvlstr[] = {
    [SPANK_SYSLOG_NONE   ] = "None", 
    [SPANK_SYSLOG_FATAL  ] = "Fatal", 
    [SPANK_SYSLOG_ERROR  ] = "Error", 
    [SPANK_SYSLOG_WARNING] = "Warning", 
    [SPANK_SYSLOG_INFO   ] = "Info", 
    [SPANK_SYSLOG_DEBUG  ] = "Debug", 
    NULL
};

int spank_syslog_level_lookup(char *str) {
    for (int i = 0 ; spank_syslog_lvlstr[i] ; i++) {
	if (!strcasecmp(str, spank_syslog_lvlstr[i])) {
	    return i;
	}
    }
    return -1;
}

const char *const spank_syslog_string(int lvl) {
    static const char *unknown = "????";
    if ((lvl < SPANK_SYSLOG_NONE) || (lvl > SPANK_SYSLOG_DEBUG)) {
	return unknown;
    }
    return spank_syslog_lvlstr[lvl];
}
