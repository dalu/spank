/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: 
 */

#include <string.h>

#include "spank/types.h"

#if defined(CONFIG_SPANK_PACKED_PACKET)
#error "Not implemented yet"

static inline int32_t
spank_i2p_position_value(int32_t v)
{
    switch(v) {
    case  SPANK_INT32_NAN: v =  SPANK_PACKED_POSITION_NAN; break;
    case  SPANK_INT32_INF: v =  SPANK_PACKED_POSITION_INF; break;
    case -SPANK_INT32_INF: v = -SPANK_PACKED_POSITION_INF; break;
    default:
	v /= 10; // from mm/s to cm/s
	if (v > SPANK_PACKED_POSITION_MAX) {
	    v =  SPANK_PACKED_POSITION_INF;
	} else if (v < SPANK_PACKED_POSITION_MIN) {
	    v = -SPANK_PACKED_POSITION_INF;
	}
	break;
    }
    return v;
}

static inline int32_t
spank_i2p_velocity_value(int32_t v)
{
    switch(v) {
    case  SPANK_INT32_NAN: v =  SPANK_PACKED_VELOCITY_NAN; break;
    case  SPANK_INT32_INF: v =  SPANK_PACKED_VELOCITY_INF; break;
    case -SPANK_INT32_INF: v = -SPANK_PACKED_VELOCITY_INF; break;
    default:
	v /= 10; // from mm/s to cm/s
	if (v > SPANK_PACKED_VELOCITY_MAX) {
	    v =  SPANK_PACKED_VELOCITY_INF;
	} else if (v < SPANK_PACKED_VELOCITY_MIN) {
	    v = -SPANK_PACKED_VELOCITY_INF;
	}
	break;
    }
    return v;
}




static inline int
spank_position_pack(spank_position_t *position, uint8_t *data)
{
    _Static_assert( (SPANK_PACKED_POSITION_BITS * 3) < 64, "storage too short" );
    _Static_assert( sizeof(spank_packed_position_t)  < 8,  "storage too short" );
    
    int64_t v = 0;
    v <<= SPANK_PACKED_POSITION_BITS ; v |= spank_i2p_position_value(i->z);
    v <<= SPANK_PACKED_POSITION_BITS ; v |= spank_i2p_position_value(i->y);
    v <<= SPANK_PACKED_POSITION_BITS ; v |= spank_i2p_position_value(i->x);

    for (int i = 0; i < sizeof(spank_packed_position_t) ; i++) {
	((uint8_t *)o)[i] = v & 0xFF;
	v >>= 8;
    }
    
    return 0;
}



static inline int
spank_velocity_pack(spank_packed_position_t *o, spank_position_t *i)
{
    _Static_assert( (SPANK_PACKED_VELOCITY_BITS * 3) < 64, "storage too short" );
    _Static_assert( sizeof(spank_packed_velocity_t)  < 8,  "storage too short" );
    
    int64_t v = 0;
    v <<= SPANK_PACKED_VELOCITY_BITS ; v |= spank_i2p_velocity_value(i->z);
    v <<= SPANK_PACKED_VELOCITY_BITS ; v |= spank_i2p_velocity_value(i->y);
    v <<= SPANK_PACKED_VELOCITY_BITS ; v |= spank_i2p_velocity_value(i->x);

    for (int i = 0; i < sizeof(spank_packed_velocity_t) ; i++) {
	((uint8_t *)o)[i] = v & 0xFF;
	v >>= 8;
    }
    
    return 0;
}

#endif




#define SPANK_PACK(f, s, d, l) do {				\
	ssize_t len = spank_##f##_pack(s, d, l);		\
	if (len < 0) return len;				\
	d  = ((char *)d) + len;					\
	l -= len;						\
    } while(0)

#define SPANK_UNPACK(f, s, d, l) do {				\
	ssize_t len = spank_##f##_unpack(s, d, l);		\
	if (len < 0) return len;				\
	d  = ((char *)d) + len;					\
	l -= len;						\
    } while(0)








ssize_t
spank_position_unpack(spank_position_t *position, void *data, size_t length)
{
    size_t len = sizeof(spank_position_t);
    if (length < len) return -1;
    memcpy(position, data, len);
    return len;
}

ssize_t
spank_velocity_unpack(spank_velocity_t *velocity, void *data, size_t length)
{
    size_t len = sizeof(spank_velocity_t);
    if (length < len) return -1;
    memcpy(velocity, data, len);
    return len;
}

ssize_t
spank_stddev_xyz_unpack(spank_stddev_xyz_t *stddev_xyz, void *data, size_t length)
{
    size_t len = sizeof(spank_stddev_xyz_t);
    if (length < len) return -1;
    memcpy(stddev_xyz, data, len);
    return len;
}

ssize_t
spank_posture_unpack(spank_posture_t *posture, void *data, size_t length)
{
    ssize_t data_length = length;
    SPANK_UNPACK(position,   &posture->position.value,  data, length);
    SPANK_UNPACK(stddev_xyz, &posture->position.stddev, data, length);
    SPANK_UNPACK(velocity,   &posture->velocity.value,  data, length);
    SPANK_UNPACK(stddev_xyz, &posture->velocity.stddev, data, length);
    return length - data_length;
}





static inline ssize_t
spank_position_pack(spank_position_t *position, void *data, size_t length)
{
    size_t len = sizeof(spank_position_t);
    memcpy(data, position, len);
    return len;
}

static inline ssize_t
spank_velocity_pack(spank_velocity_t *velocity, void *data, size_t length)
{
    size_t len = sizeof(spank_velocity_t);
    memcpy(data, velocity, len);
    return len;
}

static inline ssize_t
spank_stddev_xyz_pack(spank_stddev_xyz_t *stddev_xyz, void *data, size_t length)
{
    size_t len = sizeof(spank_stddev_xyz_t);
    memcpy(data, stddev_xyz, len);
    return len;
}

ssize_t
spank_posture_pack(spank_posture_t *posture, void *data, size_t length)
{
    size_t data_length = length;
    SPANK_PACK(position,   &posture->position.value,  data, length);
    SPANK_PACK(stddev_xyz, &posture->position.stddev, data, length);
    SPANK_PACK(velocity,   &posture->velocity.value,  data, length);
    SPANK_PACK(stddev_xyz, &posture->velocity.stddev, data, length);
    return length - data_length;
}




