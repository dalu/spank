/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include <string.h>

#include "spank/uav.h"

void __attribute__((weak))
spank_uav_get_posture(spank_posture_t *posture)
{
    memset(posture, 0, sizeof(*posture));
}

void __attribute__((weak))
spank_uav_feed_distance(spank_distance_t dist, spank_stddev_t stddev,
			spank_posture_t *posture, uint64_t id, void *extra)
{
}

void __attribute__((weak))
spank_uav_control(int cmd, void *args)
{
}

bool __attribute__((weak))
spank_uav_init(void)
{
    return true;
}

