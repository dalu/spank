/*
 * Copyright (c) 2018-2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <sys/types.h>

#define SPANK_SYSLOG_PREFIX "NEIGHB"

#include "spank/io.h"
#include "spank/spank.h"
#include "spank/syslog.h"

static int neighbours_idx = 0;

spank_node_t *
spank_lookup_neighbour(spank_ctx_t *ctx, spank_addr_t addr) {
    for (int i = 0 ; i < SPANK_NEIGHBOURS_MAXSIZE ; i++) {
	if (ctx->neighbours[i].address == addr)
	    return &ctx->neighbours[i];
    }
    return NULL;
}



void
spank_neighbour_range_reset(spank_ctx_t *ctx) {
    neighbours_idx = 0;
}

spank_addr_t
spank_neighbour_range_next(spank_ctx_t *ctx) {
    while (neighbours_idx < SPANK_NEIGHBOURS_MAXSIZE) {
	spank_node_t *n = &ctx->neighbours[neighbours_idx++];
	if ((n->address != SPANK_ADDR_NONE  ) &&
	    (n->address != ctx->io->src_addr))
	    return n->address;
    }
    neighbours_idx = 0;
    return SPANK_ADDR_NONE;
}


bool
spank_neighbour_remove_by_addr(spank_ctx_t *ctx, spank_addr_t addr) {
    spank_node_t *n = spank_lookup_neighbour(ctx, addr);
    if (n == NULL)
	return false;
    
    n->address = SPANK_ADDR_NONE;
    return true;
}


void
spank_neighbour_elected(spank_ctx_t *ctx) {
    for (int i = 0 ; i < SPANK_NEIGHBOURS_MAXSIZE ; i++) {
	spank_node_t *n = &ctx->neighbours[i];
	n->electing.tried = 0;
    }
}


spank_addr_t
spank_neighbour_elect(spank_ctx_t *ctx) {
    // Found ourself
    int i = 0;
    for (i = 0 ; i < SPANK_NEIGHBOURS_MAXSIZE ; i++)
	if (ctx->neighbours[i].address == ctx->io->src_addr)
	    break;
    
    // Take the next one marked as electable
    int m = i + SPANK_NEIGHBOURS_MAXSIZE;
    for (i++ ; i < m ; i++) {
	spank_node_t *n = &ctx->neighbours[i % SPANK_NEIGHBOURS_MAXSIZE];

	if ((n->address != SPANK_ADDR_NONE)                 &&
	    (n->behaviour & SPANK_NODE_BEHAVIOUR_ELECTABLE)) {
	    if (n->electing.tried < 4) {
		n->electing.tried++;
		return n->address;
	    }
	}
    }

    // Was not able to find an electable node
    // Perhabs due to to many retries, but as this election
    // process is not robust, we still want to retry
    // after a new ranging
    for (int i = 0 ; i < SPANK_NEIGHBOURS_MAXSIZE ; i++) {
	spank_node_t *n = &ctx->neighbours[i];
	n->electing.tried = 0;
    }
    
    return SPANK_ADDR_NONE;
}

