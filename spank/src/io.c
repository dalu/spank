/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include <stdint.h>
#include <sys/types.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include <inttypes.h>

#define SPANK_SYSLOG_PREFIX "IO    "
#include "spank/syslog.h"
#include "spank/driver.h"
#include "spank/io.h"
#include "spank/utils.h"
#include "sys/queue.h"


#define SPANK_IO_SEND_TIMEOUT SPANK_DRIVER_TX_TIMEOUT


#define SPANK_IO_DEV(io) ((spank_driver_t *)io->dev)

static struct {
    spank_mutex_t mutex;
    struct rx_entry {
	spank_io_rx_ctx_t rx_ctx;
	uint8_t packet[SPANK_IO_RX_PACKET_MAXSIZE];
	ssize_t size;
	TAILQ_ENTRY(rx_entry) entries;
    } entry[SPANK_IO_RX_QUEUE_SIZE];
    TAILQ_HEAD(, rx_entry) head;
    TAILQ_HEAD(, rx_entry) free;
} rx_queue = {
    .head = TAILQ_HEAD_INITIALIZER(rx_queue.head),
    .free = TAILQ_HEAD_INITIALIZER(rx_queue.free),
};


void __attribute__((weak))
spank_io_sniffer_frame(uint8_t *data, size_t size,
		       spank_io_frame_info_t *info);

void __attribute__((weak))
spank_io_sniffer_packet(spank_io_rx_ctx_t *rx_ctx, ssize_t size);


void spank_io_init(spank_io_t *io) {
    // Initialize mutex and semaphore
    spank_mutex_init(&io->tx.mutex);
    spank_semaphore_init(&io->rx.pkt_sem, 0);
    spank_semaphore_init(&io->tx.done_sem, 0);

    // Initialize RX queue
    spank_mutex_init(&rx_queue.mutex);
    for (int i = 0 ; i < SPANK_IO_RX_QUEUE_SIZE ; i++)
	TAILQ_INSERT_HEAD(&rx_queue.free, &rx_queue.entry[i], entries);

    // Ensure receiver is turned on
    spank_driver_rx_start(io->dev);
}


/*
 */
static ssize_t
_spank_io_parse_header(spank_io_t *io, spank_io_rx_ctx_t *rx_ctx,
		       uint8_t *data, size_t size) {
    uint8_t *hdr = data;
    
    // Ensure minimum frame size
    if (size < sizeof(ieee802154_fcf_t))
	return SPANK_IO_PKT_TOO_SHORT;

    // Frame control
    ieee802154_fcf_t *fcf = (ieee802154_fcf_t *) hdr;
    hdr  += sizeof(ieee802154_fcf_t);
    size -= sizeof(ieee802154_fcf_t);
    
    // Check version
    switch(fcf->version) {
    case IEEE802154_VERSION_802154_2006: break;
    default: return SPANK_IO_UNSUPPORTED_VERSION;
    }

    // Check frame type
    if (fcf->type != IEEE802154_FRAME_TYPE_DATA)
	return SPANK_IO_INVALID_PKT;
    
    // Check for security header
    if (fcf->security_enabled)
	return SPANK_IO_UNIMPLEMENTED;
   
    // Ignore frame pending and ack request
    
    // Sequence number
    //  (well we don't care for now)
    if (size < sizeof(uint8_t))
	return SPANK_IO_PKT_TOO_SHORT;
    uint8_t seq = *hdr;
    (void)seq;
    hdr  += sizeof(uint8_t);
    size -= sizeof(uint8_t);

    // Sanity check on addresses and PAN id
    // We only support:
    //   - same PAN
    //   - dst: short broadcast | extended
    //   - src: extended
    if (fcf->pan_id_comp != 1)
	return SPANK_IO_INVALID_PKT; 
    switch (fcf->dst_addr_mode) {
    case IEEE802154_ADDR_MODE_NONE:     return SPANK_IO_INVALID_PKT;
    case IEEE802154_ADDR_MODE_SIMPLE:   return SPANK_IO_UNSUPPORTED_IN_VERSION;
    case IEEE802154_ADDR_MODE_SHORT:    break;
    case IEEE802154_ADDR_MODE_EXTENDED: break;
    }
    switch (fcf->src_addr_mode) {
    case IEEE802154_ADDR_MODE_NONE:     return SPANK_IO_INVALID_PKT;
    case IEEE802154_ADDR_MODE_SIMPLE:   return SPANK_IO_UNSUPPORTED_IN_VERSION;
    case IEEE802154_ADDR_MODE_SHORT:    return SPANK_IO_INVALID_PKT;
    case IEEE802154_ADDR_MODE_EXTENDED: break;
    }

    if (size < sizeof(ieee802154_panid_t))
	return SPANK_IO_PKT_TOO_SHORT;
    ieee802154_panid_t *pan_id  = (ieee802154_panid_t *)hdr;
    hdr  += sizeof(ieee802154_panid_t);
    size -= sizeof(ieee802154_panid_t);

    if (*pan_id != io->pan_id)
	return SPANK_IO_INVALID_PKT;
    
    if (fcf->dst_addr_mode == IEEE802154_ADDR_MODE_SHORT) {	
	if (size < sizeof(ieee802154_addr_short_t))
	    return SPANK_IO_PKT_TOO_SHORT;
	ieee802154_addr_short_t *dst_addr = (ieee802154_addr_short_t *)hdr;
	hdr  += sizeof(ieee802154_addr_short_t);
	size -= sizeof(ieee802154_addr_short_t);	
	if (*dst_addr != 0xffff)
	    return SPANK_IO_INVALID_PKT;
	rx_ctx->dst_addr = SPANK_ADDR_BROADCAST;
    } else {
	if (size < sizeof(ieee802154_addr_extended_t))
	    return SPANK_IO_PKT_TOO_SHORT;
	ieee802154_addr_extended_t *dst_addr=(ieee802154_addr_extended_t *)hdr;
	hdr  += sizeof(ieee802154_addr_extended_t);
	size -= sizeof(ieee802154_addr_extended_t);
	memcpy(&rx_ctx->dst_addr, dst_addr, sizeof(*dst_addr));
    }

    if (size < sizeof(ieee802154_addr_extended_t))
	return SPANK_IO_PKT_TOO_SHORT;
    ieee802154_addr_extended_t *src_addr = (ieee802154_addr_extended_t *)hdr;
    hdr  += sizeof(ieee802154_addr_extended_t);
    size -= sizeof(ieee802154_addr_extended_t);
    memcpy(&rx_ctx->src_addr, src_addr, sizeof(*src_addr));

    // Header size
    return hdr - data;
}

/*
 */
static ssize_t
spank_io_get_from_driver(spank_io_t *io, spank_io_rx_ctx_t *rx_ctx) {
    // Get length
    size_t size = spank_driver_get_data_length(SPANK_IO_DEV(io));
    size_t osize = size;
    
#ifdef SPANK_DEBUG_CKSUM
    SPANK_DEBUG("READ DATA CKSUM");
    // Checksum (for debugging)
    uint8_t data[size];
    spank_driver_rx_read_frame_data(SPANK_IO_DEV(io), data, size, 0);
    rx_ctx->cksum = spank_cksum16(data, size);

    SPANK_DEBUG("frame: length=%lu, cksum=0x%04" PRIx16,
		(unsigned long)size, rx_ctx->cksum);
#else
    SPANK_DEBUG("frame: length=%lu", (unsigned long)size);
#endif

    
    // Expect at least a 2-byte crc
    if (size < SPANK_DRIVER_FRAME_CRC_SIZE) {
	SPANK_WARNING("packet is shorter than CRC size (discarding)");
	return SPANK_IO_PKT_TOO_SHORT;
    }
    
    // Drop CRC
    size -= SPANK_DRIVER_FRAME_CRC_SIZE;

    // Header
    uint8_t *hdr        = io->buffer.rx.hdr;
    size_t   hdrbufsize = sizeof(io->buffer.rx.hdr) < size
	                ? sizeof(io->buffer.rx.hdr) : size;

    // Ensure flags are cleared
    rx_ctx->flags = 0;

    // Retrieve header data
    //  - WITHOUT auxillariy security header
    spank_driver_rx_read_frame_data(SPANK_IO_DEV(io), hdr, hdrbufsize, 0);
    
    // Parse header
    ssize_t hdrsize = _spank_io_parse_header(io, rx_ctx, hdr, hdrbufsize);
    if (hdrsize < 0)
	return hdrsize;
    
    // Adjust to packet data size
    size -= hdrsize;
    
    // Get timestamp (should only be set if ranging)
    rx_ctx->flags    |= SPANK_IO_RX_CTX_FLG_RANGING;
    rx_ctx->timestamp = spank_driver_rx_get_timestamp(SPANK_IO_DEV(io));

    // Get power (FPU usage)
    rx_ctx->flags    |= SPANK_IO_RX_CTX_FLG_POWER;
    spank_driver_rx_get_power_estimate(SPANK_IO_DEV(io),
			       &rx_ctx->properties.power.firstpath,
			       &rx_ctx->properties.power.signal);

    // Get clock tracking
    rx_ctx->flags    |= SPANK_IO_RX_CTX_FLG_CLOCK_TRACKING;
    spank_driver_rx_get_clock_tracking(SPANK_IO_DEV(io),
			      &rx_ctx->properties.clock_tracking.offset,
			      &rx_ctx->properties.clock_tracking.interval);
    
    // Get chip temperature / voltage
    rx_ctx->flags    |= SPANK_IO_RX_CTX_FLG_CHIP_INFO;
    spank_driver_read_temp_vbat(SPANK_IO_DEV(io),
			      &rx_ctx->properties.chip_info.temp,
			      &rx_ctx->properties.chip_info.vbat);

    // Retrieve packet data if buffer was given
    if (rx_ctx->packet != NULL) {
	if (size < rx_ctx->packetlen) {
	    rx_ctx->packetlen = size;
	}
	SPANK_DEBUG("READ DATA");
	spank_driver_rx_read_frame_data(SPANK_IO_DEV(io),
		       (uint8_t *) rx_ctx->packet, rx_ctx->packetlen, hdrsize);

#ifdef SPANK_DEBUG_CKSUM
        rx_ctx->pkt_cksum = spank_cksum16(rx_ctx->packet, rx_ctx->packetlen);
        SPANK_DEBUG("packet: length=%lu (%lu), cksum=0x%04" PRIx16,
		    (unsigned long)size,
                    (unsigned long)rx_ctx->packetlen,
		    rx_ctx->pkt_cksum);
#endif
#ifdef SPANK_DEBUG_CKSUM
	uint8_t data[osize];
	spank_driver_rx_read_frame_data(SPANK_IO_DEV(io), data, osize, 0);
	uint16_t cksum = spank_cksum16(data, osize);
	if (rx_ctx->cksum != cksum) {
	    SPANK_DEBUG("%04x != %04x", rx_ctx->cksum, cksum);
	    SPANK_ASSERT(rx_ctx->cksum == cksum);
	}
#endif
	SPANK_DEBUG("READ DATA DONE");

    } else {
	rx_ctx->packetlen = 0;
    }

    // Return data size
    return size;
}

/*
 */
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT)
static ssize_t
spank_io_get_from_buffer(spank_io_t *io, spank_io_rx_ctx_t *rx_ctx,
			 uint8_t *buffer, size_t bufsize,
			 spank_io_frame_info_t *info) {

#ifdef SPANK_DEBUG_CKSUM
    // Checksum (for debugging)
    rx_ctx->cksum = spank_cksum16(buffer, bufsize);

    SPANK_DEBUG("frame: length=%lu, cksum=0x%04" PRIx16,
		(unsigned long)info->length, rx_ctx->cksum);
#else
    SPANK_DEBUG("frame: length=%lu", (unsigned long)info->length);
#endif
    
    // Expect at least a 2-byte crc
    if (size < SPANK_DRIVER_FRAME_CRC_SIZE) {
	SPANK_WARNING("packet is shorter than CRC size (discarding)");
	return SPANK_IO_PKT_TOO_SHORT;
    }

    // Drop CRC
    bufsize -= SPANK_DRIVER_FRAME_CRC_SIZE;
    
    // Ensure flags are cleared
    rx_ctx->flags = 0;

    // Parse header
    ssize_t hdrsize = _spank_io_parse_header(io, rx_ctx, buffer, bufsize);
    if (hdrsize < 0)
	return hdrsize;

    // Adjust to packet data size
    size_t size = bufsize - hdrsize;
    
    // Get timestamp (should only be set if ranging)
    rx_ctx->flags    |= SPANK_IO_RX_CTX_FLG_RANGING;
    rx_ctx->timestamp = info->timestamp;

    // Copy properties (power, clock_tracking, chip_info, ...)
    memcpy(&rx_ctx->properties, &io->buffer.rx.frame.info.properties,
	   sizeof(spank_io_frame_properties_t));

    // Retrieve packet data if buffer was given
    if (rx_ctx->packet != NULL) {
	if (size < rx_ctx->packetlen) {
	    rx_ctx->packetlen = size;
	}
	memcpy(rx_ctx->packet, &buffer[hdrsize], rx_ctx->packetlen);
    } else {
	rx_ctx->packetlen = 0;
    }

    // Return data size
    return size;
}
#endif



// Header buffer is big enough so we don't have to check for overflow
static int
_spank_io_sendv(spank_io_t *io, void *data, size_t datalen, spank_addr_t dst,
		  uint32_t flags, va_list ap) {
    SPANK_DEBUG("sendv: [" SPANK_FMTaddr " -> " SPANK_FMTaddr "]",
		SPANK_FMT_ADDR(io->src_addr), SPANK_FMT_ADDR(dst));
    
    /* Construct header
     */
    uint8_t *hdr                 = io->buffer.tx.hdr;
    bool     is_broadcast        = dst == SPANK_ADDR_BROADCAST;
    bool     is_panid_compressed = true;

    // Set Frame Control Field
    *(union ieee802154_fcf *)hdr = (union ieee802154_fcf) {
	.version          = IEEE802154_VERSION_802154_2006,
	.type             = IEEE802154_FRAME_TYPE_DATA,
	.security_enabled = 0,
	.frame_pending    = 0,
	.ack_requested    = 0,
	.pan_id_comp      = is_panid_compressed,
	.dst_addr_mode    = is_broadcast
	                  ? IEEE802154_ADDR_MODE_SHORT
	                  : IEEE802154_ADDR_MODE_EXTENDED,
	.src_addr_mode    = IEEE802154_ADDR_MODE_EXTENDED,
    };
    hdr += sizeof(ieee802154_fcf_t);

    // Set Sequence number
    *(uint8_t *)hdr = 0;
    hdr += sizeof(uint8_t);
    
    // Set address destination
    *(ieee802154_panid_t *)hdr = io->pan_id; // SAN: alignement -> OK
    hdr += sizeof(ieee802154_panid_t);

    if (is_broadcast) {
	*(ieee802154_addr_short_t *)hdr = 0xffff; // SAN: alignement -> OK
	hdr += sizeof(ieee802154_addr_short_t);
    } else {
	memcpy(hdr, &dst, sizeof(ieee802154_addr_extended_t));
	hdr += sizeof(ieee802154_addr_extended_t);
    }

    // Set address source
    if (!is_panid_compressed) {
	*(ieee802154_panid_t *)hdr = io->pan_id;
	hdr += sizeof(ieee802154_panid_t);
    }
    memcpy(hdr, &io->src_addr, sizeof(ieee802154_addr_extended_t));
    hdr += sizeof(ieee802154_addr_extended_t);

    
    /* Prepare for low-level driver processing thread
     */
    spank_driver_t *dev     = SPANK_IO_DEV(io);
    int             tx_mode = SPANK_DRIVER_TX_IMMEDIATE;
    
    // Answer expected? (=> Auto-RX after TX)
    if (flags & SPANK_IO_SEND_ANSWER_EXPECTED) {
	tx_mode |= SPANK_DRIVER_TX_RESPONSE_EXPECTED;
    }

    // Timestamp offset
    size_t pkt_ts_offset = 0;
    if (flags & SPANK_IO_SEND_DELAYED_EMBED_TIMESTAMP) {
	tx_mode |= SPANK_DRIVER_TX_DELAYED_START                      |
	           SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP            |
	           SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_DEFAULT    ;
        // Offset relative to start of packet
	pkt_ts_offset = hdr - io->buffer.tx.hdr + va_arg(ap, size_t);
    }

    // Acquire driver processing mutex
    spank_mutex_acquire(&dev->processing.tx.mutex,
			SPANK_SYSTICKS_INFINITE);

    // Set data
    struct iovec *iovec = dev->processing.tx.iovec;
    iovec[0].iov_base = io->buffer.tx.hdr;
    iovec[0].iov_len  = hdr - io->buffer.tx.hdr;
    iovec[1].iov_base = data;
    iovec[1].iov_len  = datalen;

    dev->processing.tx.ts_offset = pkt_ts_offset;
    dev->processing.tx.tx_mode   = tx_mode;
    
    // Release processing mutex
    spank_mutex_release(&dev->processing.tx.mutex);

    
    /* Signal processing thread and wait for it's answer
     */
    // Signal processing thread
    SPANK_DEBUG("processing: post tx");
    spank_driver_post_tx(dev);

    // Wait for transmission being started
    SPANK_DEBUG("processing: tx.sem take");
    spank_semaphore_take(&dev->processing.tx.sem, SPANK_SYSTICKS_INFINITE);
    
    return dev->processing.tx.rc;
}



/* 
 * Perform timestamping and notify of packet being sent 
 * (unblocking the thread waiting in spank_io_send), 
 *
 * It seems that some 'sent' event are not generated or missed,
 * we are taking that into account by using timeout on spank_io_send
 *
 * It doesn't seem likely to have extraneous 'sent' event
 * (except for the one that would be generated after the timeout
 * in spank_io_send), as when processing the IRQ the chip register
 * value is checked
 */
void _spank_io_evt_tx_sent(spank_io_t *io) {
    /* Check for unexepected 'sent' event as:
     *  - 'sent' is normally generated but 
     *     we already gave up waiting for it in spank_io_send
     *  - we got a purely extraneous event
     */
    if (!io->tx.sending) {
	return;
    }
    io->tx.sending = false;	    /* This ensure that we only have 
 				     * to deal with 1 extra event */
    
    /* Retrieve and set timestamp */
    if (io->tx.timestamp) {
	*io->tx.timestamp = spank_driver_tx_get_timestamp(SPANK_IO_DEV(io));
	io->tx.timestamp = NULL;
    }
    
    // spank_driver_rx_get_power_estimate(...)
    
    /* Notify of packet transmitted */
    spank_semaphore_give(&io->tx.done_sem);
}


void _spank_io_evt_rx_received(spank_io_t *io) {
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT)
    bool sniffing = spank_io_sniffer_frame && io->sniffer.frame.enabled;
    
    if (sniffing) {
	// Retrieve frame information (length, timestamp)
	io->buffer.frame.info.length =
	    spank_driver_get_data_length(SPANK_IO_DEV(io));

	io->buffer.frame.info.timestamp = 
	    spank_driver_rx_get_timestamp(SPANK_IO_DEV(io));

	// Retrieve frame properties (that can be used to improve ranging)
	// - power (FPU usage)
	// - clock tracking
	// - chip temperature / voltage	
	spank_io_frame_properties_t *properties =
	    &io->buffer.frame.info.properties;
	spank_driver_rx_get_power_estimate(SPANK_IO_DEV(io),
				&properties->power.signal,
				&properties->power.firstpath);
	spank_driver_rx_get_clock_tracking(SPANK_IO_DEV(io),
				&properties->clock_tracking.offset,
				&properties->clock_tracking.interval);
	spank_driver_read_temp_vbat(SPANK_IO_DEV(io),
				&properties->chip_info.temp,
				&properties->chip_info.vbat);

	// Read full frame (according to buffer size)
	size_t size = io->buffer.frame.info.length;
	if (size > sizeof(io->buffer.frame.data))
	    size = sizeof(io->buffer.frame.data);

	spank_driver_rx_read_frame_data(SPANK_IO_DEV(io),
					io->buffer.frame.data, size, 0);

	// Sniffer callback
	spank_io_sniffer_frame(io->buffer.frame.data, size,
			       &io->buffer.frame.info);
    }
#endif

    // Get free rx_entry
    spank_mutex_acquire(&rx_queue.mutex, SPANK_SYSTICKS_INFINITE);
    struct rx_entry *rx_entry = TAILQ_FIRST(&rx_queue.free);
    if (rx_entry == NULL) {
	SPANK_DEBUG("evt_rx_received: queue full (packet lost)");
	SPANK_STAT_INC(io->stats.rx_lost);
	spank_mutex_release(&rx_queue.mutex);
	return;
    }
    TAILQ_REMOVE(&rx_queue.free, rx_entry, entries);
    spank_mutex_release(&rx_queue.mutex);
    
    // Initialize rx_entry
    rx_entry->rx_ctx.packet    = (spank_packet_t *)rx_entry->packet;
    rx_entry->rx_ctx.packetlen = sizeof(rx_entry->packet);
	
    // Retrieving packet (and basic sanity check)
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT)
    if (sniffing) {
	size_t size = io->buffer.frame.info.length;
	if (size > sizeof(io->buffer.frame.data))
	    size = sizeof(io->buffer.frame.data);
	rx_entry->size = spank_io_get_from_buffer(
			  io, &rx_entry->rx_ctx,
			  io->buffer.frame.data, size, &io->buffer.frame.info);
    } else {
#endif
	rx_entry->size = spank_io_get_from_driver(io, &rx_entry->rx_ctx);
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT)
    }
#endif

    // Put rx_entry in reception queue
    spank_mutex_acquire(&rx_queue.mutex, SPANK_SYSTICKS_INFINITE);
    TAILQ_INSERT_TAIL(&rx_queue.head, rx_entry, entries);
    spank_mutex_release(&rx_queue.mutex);

    // Notify of received packet
    spank_semaphore_give(&io->rx.pkt_sem);
}

void _spank_io_evt_rx_failed(spank_io_t *io) {
    SPANK_STAT_INC(io->stats.rx_failed);
}
void _spank_io_evt_rx_timeout(spank_io_t *io) {
    SPANK_STAT_INC(io->stats.rx_timeout);
}





ssize_t spank_io_recv(spank_io_t *io, spank_io_rx_ctx_t *rx_ctx, spank_systicks_t timeout) {
    struct rx_entry *rx_entry;
    ssize_t size;
    
    // Wait for a packet
    if (!spank_semaphore_take(&io->rx.pkt_sem, timeout)) {
	SPANK_DEBUG("recv: aborted by timeout");
	return SPANK_IO_TIMEOUT;
    }
    
    // Remove packet from the receive queue
    spank_mutex_acquire(&rx_queue.mutex, SPANK_SYSTICKS_INFINITE);
    rx_entry = TAILQ_FIRST(&rx_queue.head);
    SPANK_ASSERT(rx_entry != NULL); // Help catching semaphore impl bug
    TAILQ_REMOVE(&rx_queue.head, rx_entry, entries);
    spank_mutex_release(&rx_queue.mutex);

    // Copy packet to user
    SPANK_ADDR_COPY(&rx_ctx->src_addr, &rx_entry->rx_ctx.src_addr);
    SPANK_ADDR_COPY(&rx_ctx->dst_addr, &rx_entry->rx_ctx.dst_addr);
    if (rx_ctx->packetlen > rx_entry->rx_ctx.packetlen)
	rx_ctx->packetlen = rx_entry->rx_ctx.packetlen;
    memcpy(rx_ctx->packet, rx_entry->rx_ctx.packet, rx_ctx->packetlen);
#ifdef SPANK_DEBUG_CKSUM
    rx_ctx->cksum = rx_entry->rx_ctx.cksum;
    rx_ctx->pkt_cksum = rx_entry->rx_ctx.pkt_cksum;
#endif
    rx_ctx->flags     = rx_entry->rx_ctx.flags;
    rx_ctx->timestamp = rx_entry->rx_ctx.timestamp;
    memcpy(&rx_ctx->properties, &rx_entry->rx_ctx.properties, sizeof(spank_io_frame_properties_t));


    size = rx_entry->size;

#ifdef SPANK_DEBUG_CKSUM
    SPANK_ASSERT(rx_ctx->pkt_cksum == spank_cksum16(rx_ctx->packet, rx_ctx->packetlen));
						    
 #endif						    
    
    SPANK_DEBUG("recv: [" SPANK_FMTaddr " -> " SPANK_FMTaddr "]",
		SPANK_FMT_ADDR(rx_ctx->src_addr),
		SPANK_FMT_ADDR(rx_ctx->dst_addr));
    
    // Put packet back to the free list
    spank_mutex_acquire(&rx_queue.mutex, SPANK_SYSTICKS_INFINITE);
    TAILQ_INSERT_HEAD(&rx_queue.free, rx_entry, entries);
    spank_mutex_release(&rx_queue.mutex);

    // Sniffer callback
#if defined(SPANK_IO_SNIFFER_PACKET_SUPPORT)
	if (io->sniffer.packet.enabled && spank_io_sniffer_packet) {
	    spank_io_sniffer_packet(rx_ctx, size);
	}
#endif
	
    // Return size
    return size;
}


int
spank_io_send(spank_io_t *io, void *data, size_t datalen, spank_addr_t dst,
	      uint64_t *timestamp, uint32_t flags, ...) {
    int rc = 0;

    /* Acquire TX, as only one can send */
    spank_mutex_acquire(&io->tx.mutex, SPANK_SYSTICKS_INFINITE);
    
    /* Mark as starting to send a packet */
    io->tx.sending = true;
    /* Request a timestamping */
    io->tx.timestamp = timestamp;
    /* Perform the send request */
    va_list ap;
    va_start(ap, flags);
    rc = _spank_io_sendv(io, data, datalen, dst, flags, ap);
    va_end(ap);

    /* Check if send request was successful */
    if (rc < 0) {
	goto done;
    }

    /* Ensure that the packet is really sent before returning.
     * Unfortunatelly once in a while some 'sent' event are missing,
     * so we need to have short timeout to unlock the situation,
     * but long enough not to miss it.
     */
    if (!spank_semaphore_take(&io->tx.done_sem, SPANK_IO_SEND_TIMEOUT)) {
	SPANK_DEBUG("send: failed (TX done timed out)");
	rc = -1;
    }

 done:
    /* Unfortunatelly we don't have condition variable for signaling,
     * so we need to fake it with semaphore ...  which is tricky
     * (see: DOI10.1007/0-387-21821-1_5).
     * https://www.microsoft.com/en-us/research/wp-content/uploads/2004/12/ImplementingCVs.pdf
     * Luckily we only have one thread waiting
     */
    
    /* Event can be generated here (as the mutex is unlock and
     * tx.sending not marked as false yet).
     * Luckily we will consider it as still being part of this transaction
     * but we will need to remove the extra semaphore given
     */
    io->tx.sending = false;
    spank_semaphore_take(&io->tx.done_sem, 0);

    /* Release TX */
    spank_mutex_release(&io->tx.mutex);

    return rc;
}


void spank_io_tx_power(spank_io_t *io, uint8_t *phr, uint8_t *sd) {
    uint32_t tx_power = SPANK_DRIVER_TX_POWER(SPANK_IO_DEV(io));
    uint8_t  tx_phr = (tx_power >> 16) & 0xFF;
    uint8_t  tx_sd  = (tx_power >> 8 ) & 0xFF;

    *phr = (tx_phr & 0x1F) + 5 * (6 - ((tx_phr & 0xE0) >> 5));
    *sd  = (tx_sd  & 0x1F) + 5 * (6 - ((tx_sd  & 0xE0) >> 5));
}
    
