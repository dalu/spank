/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include <string.h>

#define SPANK_SYSLOG_PREFIX "SPANK "

#include "spank/syslog.h"
#include "spank/io.h"
#include "spank/timer.h"
#include "spank/node.h"
#include "spank/packet.h"
#include "spank/spank.h"
#include "spank/neighbour.h"
#include "spank/spank.h"
#include "spank/config.h"
#include "spank/uav.h"
#include "spank/report.h"
#include "spank/token-sdstwr.h"

extern spank_node_t spank_neighbours_table[SPANK_NEIGHBOURS_MAXSIZE];

// NEED TO VALIDATE PACKET RX SIZE




static struct spank_config _spank_config = {
    .packet        = { .retries                = SPANK_PACKET_RETRIES },
    .twr           = { .timeout                = SPANK_TIMEOUT_TWR },
    .orchestration = { .election_timeout       = SPANK_TIMEOUT_ELECTION,
		       .scheduled_next_ranging = SPANK_SCHEDULED_NEXT_RANGING },
};

static struct spank_ctx    _spank_ctx;





/*======================================================================*/
/* Packet sending helpers                                               */
/*======================================================================*/


int
spank_send_navctrl(spank_addr_t dst, uint8_t cmd, uint8_t countdown) {
    SPANK_INFO("Sending NavCtrl 0x%x (%d)", cmd, countdown);
    spank_ctx_t *ctx  = &_spank_ctx;
    struct {
	struct spank_packet_hdr hdr;
	struct spank_payload_navctrl navctrl;
    } __attribute__((packed)) pkt =  {
	  .hdr     = { .type      = SPANK_PKT_TYPE_NAVCTRL,
		       .seq       = 0,			     },
	  .navctrl = { .cmd       = cmd,
		       .countdown = countdown,		     }
    };
    return spank_io_send(ctx->io, &pkt, SPANK_PACKET_NAVCTRL_SIZE, dst,
			 NULL, SPANK_IO_SEND_ANSWER_EXPECTED);
}

int
spank_send_msg(spank_addr_t dst, char *msg) {
    spank_ctx_t *ctx  = &_spank_ctx;
    struct {
	struct spank_packet_hdr hdr;
	struct spank_payload_msg msg;
    } __attribute__((packed)) pkt =  {
	  .hdr     = { .type      = SPANK_PKT_TYPE_MSG,
		       .seq       = 0,			     },
    };
    strncpy(pkt.msg.data, msg, sizeof(pkt.msg.data));
    pkt.msg.data[sizeof(pkt.msg.data) - 1] = 0;
    return spank_io_send(ctx->io, &pkt, SPANK_PACKET_MSG_SIZE, dst,
			 NULL, SPANK_IO_SEND_ANSWER_EXPECTED);

}



/*======================================================================*/
/* Timer handler                                                        */
/*======================================================================*/

static void
spank_handle_navctrl_countdown(void *data)
{
    SPANK_DEBUG("Timer: navctrl countdown");
    
    spank_ctx_t *ctx = (spank_ctx_t *)data;

    // If in cooldown period, this is the end of it
    if (ctx->navctrl.cooldown) {
	SPANK_DEBUG("Removing NavCtrl cooldown");
	ctx->navctrl.cooldown = false;

    // If not in cooldown period process the command
    // and trigger cooldown period
    } else {
	SPANK_DEBUG("Setting cooldown for %d ms",
		    (int) SPANK_NAVCTRL_COUNTDOWN_COOLDOWN_MS);
	ctx->navctrl.cooldown = true;

	SPANK_DEBUG("Performing UAV control (0x%x)", ctx->navctrl.cmd);
	spank_uav_control(ctx->navctrl.cmd, NULL);

	spank_timer_set(&ctx->navctrl.timer,
		SPANK_MS_TO_SYSTICKS(SPANK_NAVCTRL_COUNTDOWN_COOLDOWN_MS),
		spank_handle_navctrl_countdown, ctx);
    }
}






/*======================================================================*/
/* Packet processing                                                    */
/*======================================================================*/

static inline int
_spank_process_navctrl(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx) {
    SPANK_DEBUG("Navigation control");

    // If we don't have/want navigation control
    // don't process this type of packets
    if (ctx->behaviour & SPANK_NODE_BEHAVIOUR_STATIC) {
	return SPANK_NETOK_DISCARDED;
    }

    
    spank_packet_t   *rx_packet = rx_ctx->packet;
    spank_addr_t      node_addr = rx_ctx->src_addr;
    uint8_t           cmd       = rx_packet->navctrl.cmd;
    uint8_t           countdown = rx_packet->navctrl.countdown;
    spank_systicks_t  delay     = SPANK_MS_TO_SYSTICKS(countdown *
						   SPANK_NAVCTRL_COUNTDOWN_MS);

    SPANK_DEBUG("NavCtrl 0x%x (countdown=%d) received from " SPANK_FMTaddr,
		cmd, countdown, SPANK_FMT_ADDR(node_addr));

    if (countdown > SPANK_NAVCTRL_COUNTDOWN_MAX) {
	SPANK_DEBUG("Invalid countdown value (%d), ignoring", countdown);
	return SPANK_NETERROR_MALFORMED_PACKET;
    }

    // Only process command if not in cooldown
    // Note that it is quite likely that a packet with countdown=0
    // will be in the cooldown period due to previous packets
    // (countdown > 0) having trigger th command processing
    if (ctx->navctrl.cooldown) {
	SPANK_DEBUG("NavCtrl is in cooldown period, ignoring command");
	return SPANK_NETERROR_BUSY;
    }
    
    switch(cmd) {
    case SPANK_NAVCTRL_CMD_EMERGENCY_SHUTDOWN:
    case SPANK_NAVCTRL_CMD_TAKEOFF:
    case SPANK_NAVCTRL_CMD_LANDING:
	ctx->navctrl.cmd       = cmd;
	ctx->navctrl.countdown = countdown;
	spank_timer_set(&ctx->navctrl.timer, delay,
			spank_handle_navctrl_countdown, ctx);
	break;
	
    default:
	SPANK_DEBUG("Unknown navctrl 0x%x, ignoring", cmd);
	return SPANK_NETERROR_PROCESSING_FAILED;
    }
    
    // Job's done
    return SPANK_NETOK;
}


/*======================================================================*/


static inline int
_spank_rxcallback(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx) {
    spank_packet_t *rx_packet = rx_ctx->packet;

    // If we are electing, look for a TWR polling request
    // This will indicate that a node has been successfully elected 
    // and is performing ranging
    if (ctx->state == SPANK_ELECTING) {
	if (rx_packet->hdr.type == SPANK_PKT_TYPE_TWR_POLL) {
	    // XXX: need a seq id
	    ctx->state = SPANK_LISTENING;
	    SPANK_REPORT(STATE, ctx->state);
	    spank_neighbour_elected(ctx);
	    spank_timer_cancel(&ctx->timer);
	    SPANK_DEBUG("rxcallback: intercepted TWR polling"
			" [" SPANK_FMTaddr " -> " SPANK_FMTaddr "]",
			SPANK_FMT_ADDR(rx_ctx->src_addr),
			SPANK_FMT_ADDR(rx_ctx->dst_addr));
	}
    }
	    
    // Ensure that packet is for us
    if (!spank_io_addr_is_for_me(ctx->io, rx_ctx->dst_addr)) {
	SPANK_DEBUG("rxcallback: ignoring not-for-us packet"
		    " [" SPANK_FMTaddr " -> " SPANK_FMTaddr "]",
		    SPANK_FMT_ADDR(rx_ctx->src_addr),
		    SPANK_FMT_ADDR(rx_ctx->dst_addr));
	return SPANK_NETOK; // Not for us but that's ok
    }

    // Lookup for known neighbor node
    spank_node_t *node =
	spank_lookup_neighbour(ctx, rx_ctx->src_addr);
    if (node) {
	// Update clock tracking (FPU required)
	float32_t ppm =
	    (float32_t)(rx_ctx->properties.clock_tracking.offset * 1000000) /
	    (float32_t)(rx_ctx->properties.clock_tracking.interval);

	node->clock_tracking = ppm;
    }

    // Dispatch packet according to packet type
#ifdef SPANK_DEBUG_CKSUM
    SPANK_DEBUG("pkt-cksum=0x%04" PRIx16 " (rxcallback)" ,
		spank_cksum16(rx_ctx->packet, rx_ctx->packetlen));
#endif
    
    switch(rx_packet->hdr.type) {
    case SPANK_PKT_TYPE_NAVCTRL:     // Navigation Control
	return _spank_process_navctrl(ctx, rx_ctx);
    case SPANK_PKT_TYPE_TWR_ANSWER:  // TWR Tag
	return spank_process_twr_answer(ctx, rx_ctx);
    case SPANK_PKT_TYPE_TWR_REPORT:  // TWR Tag
	return spank_process_twr_report(ctx, rx_ctx);
    case SPANK_PKT_TYPE_TWR_POLL:    // TWR Anchor
	return spank_process_twr_poll(ctx, rx_ctx);
    case SPANK_PKT_TYPE_TWR_FINAL:   // TWR Anchor
	return spank_process_twr_final(ctx, rx_ctx);
    case SPANK_PKT_TYPE_ELECT:       // Election
	return spank_process_elect(ctx, rx_ctx);
    default:                         // WTF?!
	return SPANK_NETERROR_UNEXPECTED_PACKET;
    }

    // NOT REACHED
}


spank_config_t *spank_getconfig(void) {
    spank_ctx_t *ctx  = &_spank_ctx;
    return ctx->cfg;
}


static spank_packet_t    _packet;
static spank_io_rx_ctx_t _rx_ctx;


uint8_t spank_my_behaviour(void) {
    spank_ctx_t *ctx  = &_spank_ctx;
    return ctx->behaviour;
}

spank_addr_t spank_whoami(void) {
    spank_ctx_t *ctx  = &_spank_ctx;
    return ctx->io->src_addr;
}


void spank_tx_power(uint8_t *phr, uint8_t *sd) {
    spank_ctx_t *ctx  = &_spank_ctx;
    spank_io_tx_power(ctx->io, phr, sd);
}


void spank_init(spank_io_t *io) {
    spank_ctx_t *ctx  = &_spank_ctx;

    /* Sanity check
     */
    SPANK_INFO("OS systicks       = %ld ticks/sec",
	       (long)SPANK_SYSTICKS_PER_SECOND);
    if (SPANK_SYSTICKS_PER_SECOND < 1000) {
	SPANK_WARNING("OS systicks are below 1000 ticks/sec!"
		      " (=> stability issue)");
    }

    /* Hardcoded values
     */
    SPANK_INFO("Speed of light    = %lu m/s",
	       (unsigned long)SPANK_SPEED_OF_LIGHT);
    SPANK_INFO("Driver clock      = %llu Hz (%u bits)",
	       SPANK_DRIVER_TIME_CLOCK, SPANK_DRIVER_TIME_BITS);
    SPANK_INFO("Distance per tick = %d mm/tick",
	       (int)(1000 * SPANK_SPEED_OF_LIGHT / SPANK_DRIVER_TIME_CLOCK));
      
    /* Configuration information
     */
    SPANK_INFO(".packet.retries = %lu",
	   (unsigned long)_spank_config.packet.retries);
    SPANK_INFO(".twr.timeout = %lu",
	   (unsigned long)_spank_config.twr.timeout);
    SPANK_INFO(".orchestration.election_timeout = %lu",
	   (unsigned long)_spank_config.orchestration.election_timeout);
    SPANK_INFO(".orchestration.scheduled_next_ranging = %lu",
	   (unsigned long)_spank_config.orchestration.scheduled_next_ranging);
    
    /* Initializing
     */
    memset(ctx, 0, sizeof(*ctx));
    ctx->cfg        = &_spank_config;
    ctx->io         = io;
    ctx->neighbours = spank_neighbours_table;


    /* XXX: For now using the neighbour table 
     *      => to identify ourself
     */
    for (int i = 0 ; i < SPANK_NEIGHBOURS_MAXSIZE ; i++) {
	spank_node_t *n = &ctx->neighbours[i];
	if (ctx->io->src_addr == n->address) {
	    ctx->behaviour = n->behaviour;
	    SPANK_INFO("Our behaviour is %d", ctx->behaviour);
	    break;
	}
    }
    
    SPANK_INFO("Assigned MAC address = " SPANK_FMTaddr,
	       SPANK_FMT_ADDR(io->src_addr));
}



void spank_start(void) {
    spank_ctx_t *ctx  = &_spank_ctx;

    /* Check for no-op */
    if (ctx->state != SPANK_STOPPED)
	return;
	
    SPANK_INFO("Starting");

    /* Require radio running
     */
    // TODO
    
    /* Moving to listening state
     */
    ctx->state = SPANK_LISTENING;
    SPANK_REPORT(STATE, ctx->state);
    
    /* XXX: For now using the neighboor table
     *      => to elect the first electable
     */
    for (int i = 0 ; i < SPANK_NEIGHBOURS_MAXSIZE ; i++) {
	spank_node_t *n = &ctx->neighbours[i];
	if (n->behaviour & SPANK_NODE_BEHAVIOUR_ELECTABLE) {
	    if (ctx->io->src_addr == n->address) {
		SPANK_INFO("Initiating FIRST ranging");
		ctx->state = SPANK_RANGE_QUERYING;
		SPANK_REPORT(STATE, ctx->state);
		spank_start_twr_ranging(ctx, spank_neighbour_range_next(ctx));
	    }
	    break;
	}
    }
}

void spank_stop(void) {
    spank_ctx_t *ctx  = &_spank_ctx;

    /* Check for no-op */
    if (ctx->state == SPANK_STOPPED)
	return;
    
    SPANK_INFO("Stopping");

    /* Moving to stopped state
     */
    ctx->state = SPANK_STOPPED;
    SPANK_REPORT(STATE, ctx->state);

    /* Notify radio is not used
     */
    // TODO
}


void spank_loop(void) {
    spank_ctx_t *ctx  = &_spank_ctx;
    ssize_t      size;

    /* SPANK loop
     */
    SPANK_DEBUG("Starting processing loop");

    while(1) {
	if (   (ctx->state == SPANK_STOPPED)
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT)
	       && !ctx->io->sniffer.frame.enabled
#endif
#if defined(SPANK_IO_SNIFFER_PACKET_SUPPORT)
	       && !ctx->io->sniffer.packet.enabled
#endif
	    ) {
	    spank_systicks_t delay = SPANK_MS_TO_SYSTICKS(1000);
	    if (delay > _spank_alarm_timeout)
		delay = _spank_alarm_timeout;

	    spank_delay(delay);
	    goto ticking;
	}

	
	_rx_ctx.packet    = &_packet;
	_rx_ctx.packetlen = sizeof(_packet);

	size = spank_io_recv(ctx->io, &_rx_ctx, _spank_alarm_timeout);
	if (size >= 0) {
#ifdef SPANK_DEBUG_CKSUM
	    SPANK_DEBUG("Received packet"
			" [" SPANK_FMTaddr " -> " SPANK_FMTaddr "]"
			" %d bytes"
			" (cksum: cksum=0x%04" PRIx16 
			       ", pkt=0x%04"   PRIx16 ")"
			" (ts=" SPANK_FMTts ")",
			SPANK_FMT_ADDR(_rx_ctx.src_addr),
			SPANK_FMT_ADDR(_rx_ctx.dst_addr),
			(int)size,
			_rx_ctx.cksum,
			_rx_ctx.pkt_cksum,
			SPANK_FMT_TS(_rx_ctx.timestamp));
	    SPANK_DEBUG("pkt-cksum=0x%04" PRIx16 " (loop)",
			spank_cksum16(_rx_ctx.packet, _rx_ctx.packetlen));
#else
	    SPANK_DEBUG("Received packet"
			" [" SPANK_FMTaddr " -> " SPANK_FMTaddr "]"
			" %d bytes"
			" (ts=" SPANK_FMTts ")",
			SPANK_FMT_ADDR(_rx_ctx.src_addr),
			SPANK_FMT_ADDR(_rx_ctx.dst_addr),
			(int)size,
			SPANK_FMT_TS(_rx_ctx.timestamp));
#endif
	}
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT ) || \
    defined(SPANK_IO_SNIFFER_PACKET_SUPPORT)
	if (ctx->state == SPANK_STOPPED) {
	    goto ticking;
	}
#endif
	
	if (size >= 0) {
	    if (size >= sizeof(spank_packet_hdr_t)) {
		int rc = _spank_rxcallback(ctx, &_rx_ctx);
		if (rc < 0) {
		    SPANK_DEBUG("rxcallback: failed (%d)", rc);
		}
	    } else {
		SPANK_DEBUG("Received short packet (len=%ld)", (long)size);
	    }
	} else {
	    switch(size) {
	    case SPANK_IO_TIMEOUT:
		// Don't log this as it is normal behaviour
		// for the timer handling
		break;
	    default:
		SPANK_DEBUG("Received invalid packet (err=%zd)", size);
		break;
	    }
	}

    ticking:
	spank_timer_ticking();
    }
}


int spank_sniffer(int type, int action) {
#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT ) || \
    defined(SPANK_IO_SNIFFER_PACKET_SUPPORT)

    spank_ctx_t *ctx  = &_spank_ctx;
    spank_io_t  *io   = ctx->io;

    switch(type) {

#if defined(SPANK_IO_SNIFFER_FRAME_SUPPORT)
    case SPANK_SNIFFER_FRAME:
	switch(action) {
	case SPANK_SNIFFER_STATUS:
	    return io->sniffer.frame.enabled;
	case SPANK_SNIFFER_ENABLE:
	case SPANK_SNIFFER_DISABLE:
	    io->sniffer.frame.enabled = action == SPANK_SNIFFER_ENABLE;
	    return 0;
	default:
	    return -EOPNOTSUPP;
	};
#endif

#if defined(SPANK_IO_SNIFFER_PACKET_SUPPORT)
    case SPANK_SNIFFER_PACKET:
	switch(action) {
	case SPANK_SNIFFER_STATUS:
	    return io->sniffer.packet.enabled;
	case SPANK_SNIFFER_ENABLE:
	case SPANK_SNIFFER_DISABLE:
	    io->sniffer.packet.enabled = action == SPANK_SNIFFER_ENABLE;
	    return 0;
	default:
	    return -EOPNOTSUPP;
	};
#endif

    default:
 	return -EOPNOTSUPP;
    }

#else

    return -EOPNOTSUPP;

#endif
};

