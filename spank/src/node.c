/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include "spank/node.h"

bool spank_node_update_ranging(spank_node_t *n, float32_t distance,
			       spank_posture_t *posture) {

    // Check if distance is an outlier, and push it to the ranging history
    spank_ranging_history_t *h = &n->ranging.history;
    bool is_valid_distance     = !spank_ranging_history_outlier_q(h, distance,
					      SPANK_RANGING_OUTLIER_THRESHOLD);
    spank_ranging_history_push(h, distance);

    // If distance is valid, save it
    if (is_valid_distance)
	n->ranging.distance = distance;

    // Notifiy if distance has been updated
    return is_valid_distance;
}

