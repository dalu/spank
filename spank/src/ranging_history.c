/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include <math.h>
#include <stdlib.h>
#include "spank/ranging_history.h"

void
spank_ranging_history_push(
	spank_ranging_history_t *h,
	spank_distance_t distance)
{
    if (h->size < SPANK_RANGING_HISTORY_LENGTH)
	h->size++;
    h->items[h->head] = distance;
    h->head = (h->head + 1) % SPANK_RANGING_HISTORY_LENGTH;
}



bool
spank_ranging_history_outlier_q(spank_ranging_history_t *h,
				spank_distance_t distance, int threshold)
{
    int32_t mean   = 0;
    float32_t stddev = 0;

    // XXX: check float/int
    
    // Protect against empty history
    if (h->size == 0)
	return false;

    // arm_mean_f32(h->items, h->size, &mean);
    for (int i = 0; i < h->size ; i++) {
        mean += h->items[i];
    }
    mean /= h->size;
    
    // arm_std_f32 (h->items, h->size, &stddev);
    for (int i = 0; i < h->size ; i++) {
        stddev += (h->items[i] - mean) * (h->items[i] - mean);
    }
    stddev = sqrtf(stddev / h->size);
    
    // Check threshold
    int32_t diff = abs(mean - distance);
    return diff >= (threshold * stddev);
}
