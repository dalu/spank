/*
 * Copyright (c) 2024
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdint.h>
#include <stddef.h>

#if defined(SPANK_WTIH_STDATOMIC)
#include <stdatomic.h>
#endif

#include "spank/utils.h"
#include "spank/osal.h"

/* From: https://en.wikipedia.org/wiki/Hamming_weight
 */
int
spank_popcount32(uint32_t x)
{
    const uint64_t m1  = 0x55555555;
    const uint64_t m2  = 0x33333333;
    const uint64_t m4  = 0x0f0f0f0f;
    const uint64_t h01 = 0x01010101;
    
    x -= (x >> 1) & m1;
    x = (x & m2) + ((x >> 2) & m2);
    x = (x + (x >> 4)) & m4;
    return  (x * h01) >> 24;
}

int
spank_popcount64(uint64_t x)
{
    const uint64_t m1  = 0x5555555555555555;
    const uint64_t m2  = 0x3333333333333333;
    const uint64_t m4  = 0x0f0f0f0f0f0f0f0f;
    const uint64_t h01 = 0x0101010101010101;
    x -= (x >> 1) & m1;
    x = (x & m2) + ((x >> 2) & m2);
    x = (x + (x >> 4)) & m4;
    return (x * h01) >> 56;
}


/* From zephyr lib/crc/crc16_sw.c
 */
uint16_t
spank_crc16_ccitt(const uint8_t *src, size_t len)
{
    uint16_t seed = 0x0000;
    for (; len > 0; len--) {
	uint8_t e, f;

	e = seed ^ *src++;
	f = e ^ (e << 4);
	seed = (seed >> 8) ^ ((uint16_t)f << 8) ^ ((uint16_t)f << 3) ^ ((uint16_t)f >> 4);
    }
    return seed;
}

/* From: https://en.wikipedia.org/wiki/Fletcher%27s_checksum
 */
inline uint16_t
spank_fletcher16(const uint8_t *data, size_t len)
{
    uint32_t c0, c1;
    
    // Found by solving for c1 overflow:
    //   n > 0 and n * (n+1) / 2 * (2^8-1) < (2^32-1).
    for (c0 = c1 = 0; len > 0; ) {
	size_t blocklen = len;
	if (blocklen > 5802) {
	    blocklen = 5802;
	}
	len -= blocklen;
	do {
	    c0 = c0 + *data++;
	    c1 = c1 + c0;
	} while (--blocklen);
	c0 = c0 % 255;
	c1 = c1 % 255;
    }
    return (c1 << 8 | c0);
}


/* From: https://rosettacode.org/wiki/Pseudo-random_numbers/Splitmix64
 */
#if defined(SPANK_WTIH_STDATOMIC)
static _Atomic(uint64_t) _spank_splitmix64_state = 0;

static inline uint64_t
spank_splitmix64(_Atomic(uint64_t) *state)
{
    uint64_t z = 0x9e3779b97f4a7c15 +
	           atomic_fetch_add(state, 0x9e3779b97f4a7c15);
	    
    z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
    z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
    return z ^ (z >> 31);
}
#else
static volatile uint64_t _spank_splitmix64_state = 0;
static spank_mutex_t _spank_splitmix64_mutex;

__SPANK_CONSTRUCTOR(_spank_splitmix64_init) {
    spank_mutex_init(&_spank_splitmix64_mutex);
}

static inline uint64_t
spank_splitmix64(volatile uint64_t *state)
{
    uint64_t z;
    spank_mutex_acquire(&_spank_splitmix64_mutex, SPANK_SYSTICKS_INFINITE);
    *state += 0x9e3779b97f4a7c15;
    z       = *state;
    spank_mutex_release(&_spank_splitmix64_mutex);

    z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
    z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
    return z ^ (z >> 31);
}
#endif






uint16_t spank_cksum16(const uint8_t *data, size_t len) {
    return spank_fletcher16(data, len);
}

void spank_random_seed(uint64_t seed)
{
    _spank_splitmix64_state = seed;
}

uint64_t spank_random(void) {
    return spank_splitmix64(&_spank_splitmix64_state);
}
