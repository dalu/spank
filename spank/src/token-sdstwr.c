#define SPANK_SYSLOG_PREFIX "TOKTWR"

#include "spank/syslog.h"
#include "spank/node.h"
#include "spank/spank.h"
#include "spank/report.h"
#include "spank/neighbour.h"
#include "spank/utils.h"

static void spank_handle_timeout_election(void *data);
static void spank_handle_scheduled_next_ranging(void *data);
static void spank_node_handle_timeout_twr(void *data);

/*======================================================================*/
/* Helper                                                               */
/*======================================================================*/

static inline spank_distance_t
sds_twr_distance(spank_io_t *io,
		 uint64_t t_sp, uint64_t t_rp,
		 uint64_t t_sa, uint64_t t_ra,
		 uint64_t t_sf, uint64_t t_rf) {

    return spank_distance_sds_twr(t_sp, t_rp, t_sa, t_ra, t_sf, t_rf,
			   SPANK_SPEED_OF_LIGHT,
			   SPANK_DRIVER_TIME_CLOCK,
			   SPANK_DRIVER_TIME_MASK);
}




int
spank_token_send(spank_ctx_t *ctx, spank_addr_t addr)
{
    SPANK_DEBUG("Token send " SPANK_FMTaddr, SPANK_FMT_ADDR(addr));

    SPANK_ASSERT(addr != SPANK_ADDR_NONE);
    SPANK_ASSERT(ctx->state == SPANK_ELECTING);
    
    // Set node timer for "election"
    spank_timer_cancel(&ctx->timer);
    spank_timer_set(&ctx->timer, ctx->cfg->orchestration.election_timeout,
		    spank_handle_timeout_election, ctx);

    // Send "elect" packet
    ctx->tx_packet = (spank_packet_t) {
	.hdr = { .type = SPANK_PKT_TYPE_ELECT,
		 .seq  = 0 },
    };

    memcpy(&ctx->tx_packet.elect.next, &addr, sizeof(addr));

    int rc = spank_send(ctx, &ctx->tx_packet, SPANK_PACKET_ELECT_SIZE, addr, NULL, 0);

    // Job's done
    return rc;
}


static void
spank_handle_timeout_election(void *data)
{
    SPANK_DEBUG("Timer: Timeout election");

    spank_ctx_t *ctx = (spank_ctx_t *)data;
    spank_addr_t node_addr = SPANK_ADDR_NONE;

    if (ctx->state == SPANK_STOPPED) {
	return;
    }	
    
    /* Election processed failed, either:
     *  1) try a new election
     *       will either retry the same node or find a new node
     *  2) perform a new ranging
     *       mainly useful in case of anchors or static neighbours
     *       as it can be requiered to maintain proper navigation
     *  3) move to listening state
     *       expecting that we will discover a new neighbour
     */
    
    // Find new election candidate
    node_addr = spank_neighbour_elect(ctx);
    if (node_addr != SPANK_ADDR_NONE) {
	spank_token_send(ctx, node_addr);
	return;
    }

    // Find new node for ranging
    spank_neighbour_range_reset(ctx);
    node_addr = spank_neighbour_range_next(ctx);
    if (node_addr != SPANK_ADDR_NONE) {
	SPANK_DEBUG("No electable node, moving back to RANGE_QUERYING");
	ctx->state = SPANK_RANGE_QUERYING;
	SPANK_REPORT(STATE, ctx->state);
	spank_start_twr_ranging(ctx, node_addr);
	return;
    }

    // Fallback to listening state
    SPANK_DEBUG("No neighbours for election or ranging, moving to LISTENING");
    ctx->state = SPANK_LISTENING;
    SPANK_REPORT(STATE, ctx->state);
}


static void
spank_handle_scheduled_next_ranging(void *data)
{
    SPANK_DEBUG("Timer: Scheduled next ranging");

    spank_ctx_t *ctx = (spank_ctx_t *)data;
    spank_addr_t node_addr = SPANK_ADDR_NONE;

    if (ctx->state == SPANK_STOPPED) {
	return;
    }	
    
    /* A new ranging is scheduled, either:
     *  1) find and perform a new ranging
     *       (specific ranging timeout are handled in the node context)
     *  2) start the election process
     *       (because we already have perform all the necessary ranging)
     *  3) move to listening state
     */

    // Find new node for ranging
    node_addr = spank_neighbour_range_next(ctx);
    if (node_addr != SPANK_ADDR_NONE) {
	spank_start_twr_ranging(ctx, node_addr);
	return;
    }

    // Find a new election candidate
    node_addr = spank_neighbour_elect(ctx);
    if (node_addr != SPANK_ADDR_NONE) {    
	SPANK_DEBUG("RANGE_QUERYING is done, moving to ELECTING");
	ctx->state = SPANK_ELECTING;
	SPANK_REPORT(STATE, ctx->state);
	spank_token_send(ctx, node_addr);
	return;
    }

    // Fallback to ranging again
    spank_neighbour_range_reset(ctx);
    node_addr = spank_neighbour_range_next(ctx);
    if (node_addr != SPANK_ADDR_NONE) {
	SPANK_DEBUG("No electable node, staying in RANGE_QUERYING");
	ctx->state = SPANK_RANGE_QUERYING;
	SPANK_REPORT(STATE, ctx->state);
	spank_start_twr_ranging(ctx, node_addr);
	return;
    }
    
    // Fallback to listening state
    SPANK_DEBUG("No neighbours for election or ranging, moving to LISTENING");
    ctx->state = SPANK_LISTENING;
    SPANK_REPORT(STATE, ctx->state);
}


static void
spank_node_handle_timeout_twr(void *data)
{
    spank_node_ctx_t *nctx = (spank_node_ctx_t *)data;
    spank_ctx_t *ctx = nctx->ctx;

    SPANK_DEBUG("Timer: Timeout twr (node=%lx)", (long)nctx->node);
    
    
    switch (nctx->state) {
    // Impossible state
    case SPANK_NODE_VIRGIN:
	SPANK_FATAL("Got node in impossible virgin state");
	break;

    // TWR "tag" mode
    case SPANK_NODE_TWR_POLLING:
	SPANK_DEBUG("Timeout in polling");
	if (nctx->retry.fn) {
	    nctx->retry.fn(nctx);
	} else {
	    SPANK_DEBUG("Schedule next ranging");
	    spank_node_ctx_destroy(nctx);
	    spank_timer_set(&ctx->timer,
			    ctx->cfg->orchestration.scheduled_next_ranging,
			    spank_handle_scheduled_next_ranging, ctx);
	}
	break;
    case SPANK_NODE_TWR_FINALIZING:
	SPANK_DEBUG("Timeout in finalizing");
	if (nctx->retry.fn) {
	    nctx->retry.fn(nctx);
	} else {
	    SPANK_DEBUG("Schedule next ranging");
	    spank_node_ctx_destroy(nctx);
	    spank_timer_set(&ctx->timer,
			    ctx->cfg->orchestration.scheduled_next_ranging,
			    spank_handle_scheduled_next_ranging, ctx);
	}
	break;
    case SPANK_NODE_TWR_NOTIFYING:
	SPANK_FATAL("Timeout in notifying");
	// no timeout generated in this state
	break;
	
    // TWR "anchor" mode
    case SPANK_NODE_TWR_ANSWERING:
	SPANK_DEBUG("Timeout in answering");
	// If got a timeout during the TWR transaction, for now
	// just abort the whole transaction by destroying
	// the node context
	spank_node_ctx_destroy(nctx);
	break;
    case SPANK_NODE_TWR_REPORTING:
	SPANK_DEBUG("Timeout in reporting");
	// If got a timeout during the TWR transaction, for now
	// just abort the whole transaction by destroying
	// the node context
	spank_node_ctx_destroy(nctx);
	break;
    }
}



/*======================================================================*/
/* Retry functions                                                      */
/*======================================================================*/

static void
spank_node_retry_twr_ranging(spank_node_ctx_t *nctx)
{
    if (nctx->retry.count <= 1) {
	SPANK_WARNING("Retry: %d (last) [-> %lx]",
		      nctx->retry.count, (long)nctx->node);
	nctx->retry.count = 0;
	nctx->retry.fn = NULL;
    } else {
	SPANK_WARNING("Retry: %d [-> %lx]",
		      nctx->retry.count, (long)nctx->node);
	nctx->retry.count--;
    }
    
    spank_start_twr_ranging(nctx->ctx, nctx->node);
}




int
spank_start_twr_ranging(spank_ctx_t *ctx, spank_addr_t node_addr) 
{
    SPANK_DEBUG("start ranging: " SPANK_FMTaddr, SPANK_FMT_ADDR(node_addr));

    spank_node_ctx_t      *nctx;
    spank_node_ctx_retry_f retry_fn    = spank_node_retry_twr_ranging;
    uint8_t                retry_count = ctx->cfg->packet.retries;
    
    // Check if we are already engaged with this node
    if ((nctx = spank_lookup_node_ctx(ctx, node_addr))) {
	// Duplicated, lost, or out-of-order packet?
	// Or no fucking idea how we got there?
	//  -> check for retries, and reset state anyway
	retry_count = nctx->retry.count;
	retry_fn    = nctx->retry.fn;
	spank_node_ctx_cleanup(nctx);
    } else if (!(nctx = spank_alloc_node_ctx(ctx))) {
	// Can't handle it right now
	return SPANK_NETERROR_BUSY;
    }

    // Define node context
    nctx->node        = node_addr;
    nctx->seq         = ++ctx->seq;
    nctx->retry.fn    = retry_fn;
    nctx->retry.count = retry_count;
    
    // We are now in a "polling" state for the TWR
    nctx->state = SPANK_NODE_TWR_POLLING;
    
    // Set node timer for "answer"
    spank_timer_set(&nctx->timer, ctx->cfg->twr.timeout,
		    spank_node_handle_timeout_twr, nctx);

    // Send "polling" packet
    ctx->tx_packet = (spank_packet_t) {
	.hdr = { .type = SPANK_PKT_TYPE_TWR_POLL,
		 .seq  = nctx->seq }
    };
    spank_send(ctx, &ctx->tx_packet, SPANK_PACKET_NULL_SIZE, node_addr,
	       &nctx->twr.t_sp, 0);

    // Job's done
    return SPANK_NETOK;
}



int
spank_process_twr_answer(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx) {
    SPANK_DEBUG("TWR answer");
        
    spank_packet_t   *tx_packet = &ctx->tx_packet;
    spank_packet_t   *rx_packet = rx_ctx->packet;
    spank_addr_t      node_addr = rx_ctx->src_addr;
    spank_node_ctx_t *nctx      = spank_lookup_node_ctx(ctx, node_addr);

    // Ensure we are: - engaged with this node
    //                - in the corresponding state
    //                - the packet sequence is correct
    if ((nctx        == NULL                  ) ||
	(nctx->state != SPANK_NODE_TWR_POLLING) ||
	(nctx->seq   != rx_packet->hdr.seq    )) {
	return SPANK_NETERROR_UNEXPECTED_PACKET;
    }
    
    // Cancel pending node timer
    spank_timer_cancel(&nctx->timer);

    // Switch to "finalizing" state
    nctx->state = SPANK_NODE_TWR_FINALIZING;

    // Save the rx timestamp
    nctx->twr.t_ra  = rx_ctx->timestamp;

    // Set node timer for "report"
    spank_timer_set(&nctx->timer, ctx->cfg->twr.timeout,
		    spank_node_handle_timeout_twr, nctx);

    // Send "final" packet
    *tx_packet = (spank_packet_t) {
	.hdr = { .type = SPANK_PKT_TYPE_TWR_FINAL,
		 .seq  = rx_packet->hdr.seq     }
    };
    spank_send(ctx, tx_packet, SPANK_PACKET_NULL_SIZE, node_addr,
	       &nctx->twr.t_sf, 0);

    // Job's done
    return SPANK_NETOK;
}


int
spank_process_twr_report(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx) {
    SPANK_DEBUG("TWR report");

    spank_packet_t*   pkt       = rx_ctx->packet;
    spank_addr_t      node_addr = rx_ctx->src_addr;
    spank_node_ctx_t *nctx      = spank_lookup_node_ctx(ctx, node_addr);

    // Ensure we are: - engaged with this node
    //                - in the corresponding state
    //                - the packet sequence is correct
    if ((nctx        == NULL                     ) ||
	(nctx->state != SPANK_NODE_TWR_FINALIZING) ||
	(nctx->seq   != pkt->hdr.seq             ))
	return SPANK_NETERROR_UNEXPECTED_PACKET;
    
    // Cancel pending node timer
    spank_timer_cancel(&nctx->timer);

    // Switch to "notifying" state
    nctx->state = SPANK_NODE_TWR_NOTIFYING;

    // Using memcpy due to alignement requirements for 64bits
    memcpy(&nctx->twr.t_rp, &pkt->report.twr.t_rp, sizeof(nctx->twr.t_rp));
    memcpy(&nctx->twr.t_sa, &pkt->report.twr.t_sa, sizeof(nctx->twr.t_sa));
    memcpy(&nctx->twr.t_rf, &pkt->report.twr.t_rf, sizeof(nctx->twr.t_rf));

    // Compute distance
    spank_distance_t distance = sds_twr_distance(ctx->io,
				      nctx->twr.t_sp, nctx->twr.t_rp,
				      nctx->twr.t_sa, nctx->twr.t_ra,
				      nctx->twr.t_sf, nctx->twr.t_rf);

    // Posture
    spank_posture_t posture;
    spank_posture_unpack(&posture,
			 &pkt->report.posture,
			 sizeof(pkt->report.posture));

    SPANK_REPORT(TWR_DISTANCE, (& (spank_report_twr_distance_t) {
	    .distance = distance,
	    .from     = rx_ctx->src_addr,
	    .posture  = &posture
        }) );

#if 0
    SPANK_INFO("TWR report: distance = %d"
	       " [" SPANK_FMTaddr " -> " SPANK_FMTaddr "]",
	       distance,
	       SPANK_FMT_ADDR(rx_ctx->src_addr),
	       SPANK_FMT_ADDR(rx_ctx->dst_addr));

    DEBUG_PRINT("TWR report: distance = %d / xyz=%d,%d,%d\n",
		distance,
		posture.position.value.x,
		posture.position.value.y,
		posture.position.value.z);
#endif
    
    spank_node_t *node =
	spank_lookup_neighbour(ctx, node_addr);
    
    if (node) {
	if (spank_node_update_ranging(node, distance, &posture)) {
	    // Make a distinction between fixed (ie: no velocity)
	    // nodes (ie: anchor-like) and moving nodes (ie: uav-like)
	    bool is_fixed = SPANK_VELOCITY_IS_ZERO(posture.velocity.value);

	    // Use different std-dev for fixed or moving nodes
	    // TODO: find a way to really compute stddev
	    spank_stddev_t stddev = is_fixed ? 25 : 40;

	    // Feed distance to UAV
	    // For now spank_uav_distance_measurement_extra_t =
	    //         spank_io_frame_properties_t
	    spank_uav_feed_distance(distance, stddev, &posture, node_addr,
				    &rx_ctx->properties);
	}
    }
    
    // TWR transaction is terminated, destroy node context
    spank_node_ctx_destroy(nctx);

    
    // Schedule new ranging
    SPANK_DEBUG("TWR report: scheduling next ranging");
    spank_timer_set(&ctx->timer,
		    ctx->cfg->orchestration.scheduled_next_ranging,
		    spank_handle_scheduled_next_ranging, ctx);

    return SPANK_NETOK;
}


int
spank_process_twr_poll(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx) {
    SPANK_DEBUG("TWR poll");
    
    spank_packet_t   *tx_packet = &ctx->tx_packet;
    spank_packet_t   *rx_packet = rx_ctx->packet;
    spank_addr_t      node_addr = rx_ctx->src_addr;
    spank_node_ctx_t *nctx;

    // Check if received packet is compatible with the current state
    if ((nctx = spank_lookup_node_ctx(ctx, node_addr))) {
	if (SPANK_NODE_STATE_IS_TWR_ANCHOR_GROUP(nctx->state)) {
	    // Duplicated, lost, or out-of-order packet
	    // XXX: for now assume it's due to a lost packet,
	    //      other cases will be dealt with timeout
	    //      on the other side
	    //   -> reset state and reuse
	    SPANK_DEBUG("Out of sync node state, reseting before processing");
	    spank_node_ctx_cleanup(nctx);
	} else {
	    // Incompatible state, discard packet
	    return SPANK_NETERROR_UNEXPECTED_PACKET;
	}
    } else if (!(nctx = spank_alloc_node_ctx(ctx))) {
	// Can't handle it right now
	return SPANK_NETERROR_BUSY;
    }

    // Define node context
    nctx->node  = node_addr;
    nctx->seq   = rx_packet->hdr.seq;

    // We are now in an "answering" state for the TWR
    nctx->state = SPANK_NODE_TWR_ANSWERING;
    
    // Save timestamp
    nctx->twr.t_rp  = rx_ctx->timestamp;

    // Set node timer for "answer"
    spank_timer_set(&nctx->timer, ctx->cfg->twr.timeout,
		    spank_node_handle_timeout_twr, nctx);

    // Send TWR ANSWER
    ctx->tx_packet = (spank_packet_t) {
	.hdr = { .type = SPANK_PKT_TYPE_TWR_ANSWER,
		 .seq  = rx_packet->hdr.seq     }
    };
    spank_send(ctx, tx_packet, SPANK_PACKET_NULL_SIZE, node_addr,
	       &nctx->twr.t_sa, 0);
    
    // Job's done
    return SPANK_NETOK;

}


int
spank_process_twr_final(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx) {
    SPANK_DEBUG("TWR final");

    spank_packet_t   *tx_packet = &ctx->tx_packet;
    spank_packet_t   *rx_packet = rx_ctx->packet;
    spank_addr_t      node_addr = rx_ctx->src_addr;
    spank_node_ctx_t *nctx      = spank_lookup_node_ctx(ctx, node_addr);
    spank_posture_t   posture;
    
    // Ensure we are: - engaged with this node
    //                - in the corresponding state
    //                - the packet sequence is correct
    if ((nctx        == NULL                    ) ||
	(nctx->state != SPANK_NODE_TWR_ANSWERING) ||
	(nctx->seq   != rx_packet->hdr.seq      )) {
	return SPANK_NETERROR_UNEXPECTED_PACKET;
    }
    
    // Cancel pending node timer
    spank_timer_cancel(&nctx->timer);

    // Switching to "reporting" state
    nctx->state = SPANK_NODE_TWR_REPORTING;

    // Save Timestamp
    nctx->twr.t_rf = rx_ctx->timestamp;

    // Get UAV posture (position / velocity)
    spank_uav_get_posture(&posture);
    
    // Build TWR_REPORT
    *tx_packet = (spank_packet_t) {
	.hdr = { .type = SPANK_PKT_TYPE_TWR_REPORT,
		 .seq  = rx_packet->hdr.seq     }
    };
    // fill timestamps
    memcpy(&ctx->tx_packet.report.twr.t_rp, &nctx->twr.t_rp,
	   sizeof(nctx->twr.t_rp));
    memcpy(&ctx->tx_packet.report.twr.t_sa, &nctx->twr.t_sa,
	   sizeof(nctx->twr.t_sa));
    memcpy(&ctx->tx_packet.report.twr.t_rf, &nctx->twr.t_rf,
	   sizeof(nctx->twr.t_rf));
    // fill posture
    spank_posture_pack(&posture,
		       &ctx->tx_packet.report.posture,
		       sizeof(ctx->tx_packet.report.posture));
    
    // Send TWR_REPORT
    spank_send(ctx, tx_packet, SPANK_PACKET_REPORT_SIZE, node_addr, NULL, 0);

    // TWR transaction is finished, destroy node context
    spank_node_ctx_destroy(nctx);
    
    SPANK_DEBUG("TWR ended");

    // Job's done
    return SPANK_NETOK;
}


int
spank_process_elect(spank_ctx_t *ctx, spank_io_rx_ctx_t *rx_ctx) {
    SPANK_DEBUG("Got election token from " SPANK_FMTaddr,
		SPANK_FMT_ADDR(rx_ctx->src_addr));

    switch(ctx->state) {
    case SPANK_LISTENING:
	SPANK_DEBUG("Moving to QUERYING state");
	ctx->state = SPANK_RANGE_QUERYING;
	SPANK_REPORT(STATE, ctx->state);
	spank_neighbour_range_reset(ctx);
	spank_handle_scheduled_next_ranging(ctx);
	break;

    case SPANK_ELECTING:
	/* That's dodgy
	 *  => Timeout are not the same on different nodes
	 */
	SPANK_DEBUG("Got elected while electing... WTF?!");
	break;

    case SPANK_RANGE_QUERYING:
	/* Got elected again...
	 *   (Perhaps a lost packet didn't make us visible yet)
	 *  => Change nothing
	 */
	SPANK_DEBUG("Got elected but was alreay in the QUERYING state");
	break;

    case SPANK_STOPPED:
	/* Protocol is stopped
	 * => Discarding
	 */
	break;
    }
	
    return SPANK_NETOK;
}
