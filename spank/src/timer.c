/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

/* Only log information if explicitely requested as this
 * will generate a lot of noise
 */
#if !defined(SPANK_NO_SYSLOG) && !defined(SPANK_WITH_SYSLOG_TIMER)
#define SPANK_NO_SYSLOG
#endif

#define SPANK_SYSLOG_PREFIX "TIMER "
#include "spank/syslog.h"
#include "spank/timer.h"


/* Quick hack:
 *  build for version of queue.h without macro for LIST_PREV
 */
#ifndef LIST_PREV
#include <sys/cdefs.h>

#define	QUEUE_TYPEOF(type) struct type
#define LIST_PREV(elm, head, type, field)                       \
        ((elm)->field.le_prev == &LIST_FIRST((head)) ? NULL :   \
            __containerof((elm)->field.le_prev,                 \
            QUEUE_TYPEOF(type), field.le_next))
#endif



typedef struct spank_timer_ctrl {
    LIST_HEAD(, spank_timer) head;
    spank_systime_t   lasttime;
    spank_systicks_t  const delta_min;
    // spank_mutex_t     mutex;
} spank_timer_ctrl_t;



static spank_timer_ctrl_t _spank_timer_ctrl = {
    .head      = LIST_HEAD_INITIALIZER(_spank_timer_ctrl.head),
    .delta_min = SPANK_TIMER_DELTA_MIN
};

/*
void _spank_timer_ctrl_init() {
    spank_timer_ctrl_t  *ctrl  = &_spank_timer_ctrl;
    spank_mutex_init(&ctrl->mutex);
}

void _spank_timer_ctrl_lock(void) {
    spank_timer_ctrl_t  *ctrl  = &_spank_timer_ctrl;
    spank_mutex_acquire(&ctrl->mutex, SPANK_SYSTICKS_INFINITE);
}

void _spank_timer_ctrl_unlock(void) {
    spank_timer_ctrl_t  *ctrl  = &_spank_timer_ctrl;
    spank_mutex_release(&ctrl->mutex);
}
*/

void spank_timer_dump(spank_timer_t *t) {
    SPANK_DEBUG("Dump (0x%lx): n = 0x%lx, &p = 0x%lx, p = 0x%lx",
		(long)t,
		(long)t->entries.le_next,
		(long)t->entries.le_prev,
		(long)t->entries.le_prev ? (long)(*(t->entries.le_prev)) : 0);
}

void spank_timer_head_dump(void) {
    spank_timer_ctrl_t  *ctrl  = &_spank_timer_ctrl;
    (void)ctrl;
    SPANK_DEBUG("HEAD: 0x%lx", (long)ctrl->head.lh_first);
}

void spank_timer_ctrl_dump(void) {
    spank_timer_t *t;
    spank_timer_ctrl_t  *ctrl  = &_spank_timer_ctrl;
    SPANK_DEBUG("Lasttime: %ld", (long)ctrl->lasttime);
    
    LIST_FOREACH(t, &ctrl->head, entries) {
	SPANK_DEBUG("Timer:  0x%lx (+%ld), cb=0x%ld",
		    (long)t, (long)t->delta, (long)t->cb);
    }
}



static inline
spank_systicks_t _spank_timer_ensure_delta_min(spank_timer_ctrl_t *ctrl,
					       spank_systicks_t delta) {
    return (delta < ctrl->delta_min) ? ctrl->delta_min : delta;
}



int spank_timer_remaining_delay(spank_timer_t *timer, spank_systicks_t *delay) {
    // Ensure that timer is active
    if (timer->cb == NULL)
	return -1;

    // Compute timer remaining delay
    spank_timer_ctrl_t *ctrl  = &_spank_timer_ctrl;
    spank_systicks_t    delta = 0;
    spank_timer_t      *t     = NULL;
    for (t = timer ;
	 t ; 
	 t = LIST_PREV(timer, &ctrl->head, spank_timer, entries)) {
	delta += t->delta;
    }

    // Compute already elapsed time
    spank_systicks_t elapsed = spank_systime() - ctrl->lasttime;

    // Adjust timer remaining delay
    if (delta < elapsed) { delta = 0;        }
    else                 { delta -= elapsed; }

    // Save remaining delay if requested
    if (delay)
	*delay = delta;

    // Returns 0 if no remaining delay, 1 otherwise
    return delta == 0 ? 0 : 1;
}

    
void spank_timer_cancel(spank_timer_t *timer) {
    spank_timer_ctrl_t  *ctrl  = &_spank_timer_ctrl;
    spank_timer_t       *t     = LIST_FIRST(&ctrl->head);
    spank_systicks_t     delta = timer->delta;
    
    // If timer has already been disarmed, just return 
    if (timer->cb == NULL)
	return;

    SPANK_DEBUG("Cancel 0x%lx", (long)timer);
    
    // If timer is not in first position
    //   1- Remove it from the list
    //   2- Add delta to next timer (if possible)
    //
    if (t != timer) {
	if ((t = LIST_NEXT(timer, entries))) // If possible
	    t->delta += delta;               //   add delta to next timer

	LIST_REMOVE(timer, entries);         // Remove timer from list
	timer->cb = NULL;                    // Mark it as disarmed

	return;
    }

    // Timer is in first position
    //   1- Remove it from the list
    //   2- Add delta to next timer (if possible)
    //   3- Reschedule or stop alarm
    //
    LIST_REMOVE(timer, entries);             // Remove timer from list
    timer->cb = NULL;                        // Mark it as disarmed

    // We got a new first timer, reschedule alarm
    if ((t = LIST_FIRST(&ctrl->head))) {
	t->delta += delta;                   // Add delta to next timer

	// Do we really need to reschedule alarm,
	// or can we consider it is due to be run by a pending alarm
	spank_systicks_t elapsed = spank_systime() - ctrl->lasttime;

	// Timer is due, return to let it process by the pending alarm
	if (t->delta <= elapsed)
	    return;

	// Reschedule the alarm
	delta = _spank_timer_ensure_delta_min(ctrl, t->delta - elapsed);
	_spank_set_alarm(ctrl->lasttime + elapsed, delta);

    // That was the last timer, stop the alarm
    } else {
	_spank_stop_alarm();
    }	
}


void spank_timer_set(spank_timer_t *timer, spank_systicks_t delay,
		     spank_callback_t cb, void *data) {
    spank_timer_ctrl_t *ctrl    = &_spank_timer_ctrl;
    spank_systime_t     now     = spank_systime();
    spank_timer_t      *t       = NULL;
    spank_timer_t      *nt      = NULL;
    spank_systicks_t    elapsed;

    /* If timer is active cancel it now! */
    if (spank_timer_is_active(timer)) {
	SPANK_WARNING("Timer 0x%lx already set, forcing cancel", (long)timer);
	spank_timer_cancel(timer);
    }

    /* Sanity check */
    SPANK_ASSERT(cb != NULL);		/* A callback is needed       */
    SPANK_ASSERT(timer->cb == NULL);	/* Timer should NOT be active */

    /* Get basic info */
    t           = LIST_FIRST(&ctrl->head);
    elapsed     = now - ctrl->lasttime;
    delay       = _spank_timer_ensure_delta_min(ctrl, delay);

    /* Set new timer data */
    timer->cb   = cb;
    timer->data = data;    

    SPANK_DEBUG("Set 0x%lx (+%ld) = 0x%lx", (long)timer, (long)delay, (long)cb);

    // Controller has no timer registered
    if (t == NULL) {
	ctrl->lasttime = now;
	timer->delta   = delay;
	LIST_INSERT_HEAD(&ctrl->head, timer, entries);
	_spank_set_alarm(ctrl->lasttime, timer->delta);
	return;

    // Controller already got at least one timer registered
    //   => first timer has a t->delta > elapsed
    } else {
	// Compute delta for new timer
	spank_systicks_t delta = elapsed + delay;

	// If delta overflow, force a move to next registered timer.
	//   It will fix overflow, as the first registered timer has
	//   by definition: t->delta > elapsed
	if (delta < elapsed) {
	    goto move_next;

	// If delta is less than first timer, set alarm
	} else if(delta < t->delta) {
	    _spank_set_alarm(ctrl->lasttime, delta);
	}

	// Iterate over registered timer to insert it in the right place
	while (t->delta < delta) {
	move_next:
	    delta -= t->delta;
	    if ((nt = LIST_NEXT(t, entries))) {
		t = nt;
	    } else {
		LIST_INSERT_AFTER(t, timer, entries);
		goto set_delta;
	    }
	}
	LIST_INSERT_BEFORE(t, timer, entries);
    set_delta:
	timer->delta = delta;
    }
}


void spank_timer_ticking(void) {
    spank_timer_ctrl_t  *ctrl  = &_spank_timer_ctrl;
    spank_timer_t       *timer = LIST_FIRST(&ctrl->head);
    spank_systime_t      now   = spank_systime();
    
    SPANK_DEBUG("Ticking (first=0x%lx)", (long)timer);
    
    // Iterate over ordered timers,
    // and trigger the ones which have elapsed time
    while (timer && (timer->delta <= (spank_systicks_t)(now-ctrl->lasttime))) {
	SPANK_DEBUG("Processing 0x%lx", (long)timer);
	
	// Callback is allowed to restart a timer
	//  => we need to clean up everything before calling the callback
	spank_callback_t cb   = timer->cb;   // Save callback 
	void            *data = timer->data;

	SPANK_ASSERT(cb != NULL);	

	// Remove timer from controller and mark it as disarmed
	SPANK_DEBUG("Removing: 0x%lx", (long)timer);
	ctrl->lasttime += timer->delta;   // Move last time forward
	LIST_REMOVE(timer, entries);      // Detach timer
	timer->cb = NULL;                 // Mark timer as disarmed
	
	// If no more pending timer, stop alarm here (before callback).
	if (LIST_EMPTY(&ctrl->head)) {
	    _spank_stop_alarm();
	}

	// Callback
	// (clean state: timer has been removed and marked as disarmed)
	SPANK_DEBUG("Triggering callback (0x%lx)", (long)cb);
	cb(data);

	// Next
	timer = LIST_FIRST(&ctrl->head);  // Fetch next timer
	now   = spank_systime();          // Get new "now" time
    }

    // If a timer is pending, compute the next wait time
    //  (Stop case has already been treated in the loop)
    if (timer) {
	spank_systicks_t delta = ctrl->lasttime + timer->delta - now;
	delta = _spank_timer_ensure_delta_min(ctrl, delta);
	SPANK_DEBUG("Scheduling next alarm (+%ld ms) for 0x%lx",
		     (long)delta, (long)timer);
	_spank_set_alarm(now, delta);
    }
}
