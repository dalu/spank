/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include "spank/spank.h"

spank_node_ctx_t* spank_alloc_node_ctx(spank_ctx_t *ctx) {
    for (int i = 0 ; i < SPANK_SIMULTANEOUS_NODE_TALK ; i++)
	if (ctx->node_ctx[i].node == SPANK_ADDR_NONE) {
	    spank_node_ctx_t *nctx = &ctx->node_ctx[i];
	    nctx->ctx = ctx; // Back pointer on spank context
	    return nctx;
	}
    return NULL;
}

spank_node_ctx_t* spank_lookup_node_ctx(spank_ctx_t *ctx, spank_addr_t addr) {
    for (int i = 0 ; i < SPANK_SIMULTANEOUS_NODE_TALK ; i++)
	if (ctx->node_ctx[i].node == addr)
	    return &ctx->node_ctx[i];
    return NULL;
}


void spank_node_ctx_free(spank_node_ctx_t *nctx) {
    nctx->node  = SPANK_ADDR_NONE;
}

void spank_node_ctx_cleanup(spank_node_ctx_t *nctx) {
    nctx->state    = SPANK_NODE_VIRGIN;
    nctx->retry.fn = NULL;
    spank_timer_cancel(&nctx->timer);
    // memset to be on the safe side?
}


void spank_node_ctx_destroy(spank_node_ctx_t *nctx) {
    spank_node_ctx_cleanup(nctx);
    spank_node_ctx_free(nctx);
}
