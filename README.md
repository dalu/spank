SPANK
=====

Build
-----

The following OS are supported

| Plateform / OS | Directory   | Comments              |
|----------------|-------------|-----------------------|
| Crazyflie 2    | port/cf2    |                       |
| MyNewt         | port/mynewt | not tested recently   |
| Zephyr         | port/zephyr |                       |
| Unix           | port/unix   | only for simulation   |


When building from the git repository, it is advised to install
[`version-export`](https://gitlab.com/sdalu/version-export) and have
it accessible from your `PATH`. It will allow the build process
to provide version information.


The recommended integration is with [zephyr-redskin](https://gitlab.inria.fr/dalu/zephyr-redskin)


License
-------

When using this software for your work or publication, you should credit
<q>Laboratoire CITI, INSA Lyon, Stéphane D'Alu</q> and make reference to
the associated [paper](https://hal.archives-ouvertes.fr/hal-02421783)
([BibTex](https://hal.archives-ouvertes.fr/hal-02421783/bibtex), 
 [TEI](https://hal.archives-ouvertes.fr/hal-02421783/tei),
 [DC](https://hal.archives-ouvertes.fr/hal-02421783/dc))
	
