#!/bin/sh

# Useful functions
join_by() { local IFS="$1"; shift; echo "$*"; }
warn()    { echo "$*" > /dev/stderr;          }
die()     { warn "$*" ; exit 1;               }
usage()   {
    cat <<EOF
Usage: ${0##*/} -h
       ${0##*/} mode [prefix] [git-parent-directory]

    mode :
      - cmake  : generate cmake list
      - sh     : use export for environment variables
      - csh    : use setenv for environment variables
      - env    : produce key=value suitable for /bin/env

    prefix :
      the prefix to use for variable declaration.
      ex: FOOBAR  ->  FOOBAR_VERSION_*

    git-parent-directory :
      force git to use the .git directory present in git-parent-directory

EOF
    exit 1
}

# Argument parsing
[ -n "$1" ] && mode="$1"
[ -n "$2" ] && prefix="$2"
[ -n "$3" ] && git_dir="$3/.git"

# Help
while getopts 'h' c ; do
    case $c  in
	h) usage ;;
    esac
done

# Sanity check
case "$mode" in
    cmake|sh|csh|env) ;;
    *) die "Unsupported mode '$1'. Try with 'cmake', 'sh', 'csh', or 'env'." ;;
esac

# If git
if which git > /dev/null; then
    # Defining git command
    GIT="git ${git_dir+--git-dir} ${git_dir}"

    # Retrieve values
    git_count=`   $GIT rev-list --count HEAD                    2> /dev/null`
    git_id_full=` $GIT rev-parse HEAD                           2> /dev/null`
    git_id_short=`$GIT rev-parse --short HEAD                   2> /dev/null`
    git_abbrev=`  $GIT rev-parse --abbrev-ref HEAD              2> /dev/null`
    git_tag=`     $GIT describe --exact-match --abbrev=0 --tags 2> /dev/null`

    # Build versions
    if   [ -n "${git_tag}" ]; then
	VERSION_NICKNAME=${git_tag}
    elif [ -n "${git_abbrev}" -a -n "${git_count}" ]; then
	if [ -n "${git_id_short}" ];
	then VERSION_NICKNAME="${git_abbrev}-${git_count} (${git_id_short})"
	else VERSION_NICKNAME="${git_abbrev}-${git_count}"
	fi
    fi
    [ -n "${git_abbrev}"   ] && VERSION_BRANCH="${git_abbrev}"
    [ -n "${git_id_full}"  ] && VERSION_COMMIT_ID_FULL="${git_id_full}"
    [ -n "${git_id_short}" ] && VERSION_COMMIT_ID_SHORT="${git_id_short}"
fi


# Generate output
prev=""
for k in NICKNAME COMMIT_ID_FULL COMMIT_ID_SHORT BRANCH ; do
    eval "v=\$VERSION_$k"
    case "$mode" in
	cmake)
	    if [ -n "$v" ]; then
		[ -n "$prev" ] && echo -n ';'
		echo -n "${prefix}VERSION_${k}=\"$v\""
	    fi
	    ;;
	sh)
	    [ -n "$v" ] && echo "export ${prefix}VERSION_${k}=\"$v\""
	    ;;
	ch)
	    [ -n "$v" ] && echo "setenv ${prefix}VERSION_${k} \"$v\""
	    ;;
	env)
	    [ -n "$v" ] && echo "${prefix}VERSION_${k}=\"$v\""
	    ;;
    esac
    prev="$v"
done
    

exit 0
