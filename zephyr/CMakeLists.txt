# CMake
#
# Copyright (c) 2019,2022
# Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
#
# SPDX-License-Identifier: Apache-2.0
#

if (CONFIG_SPANK)

execute_process(COMMAND version-export
                        cmake SPANK_ "${CMAKE_CURRENT_SOURCE_DIR}/.."
		OUTPUT_VARIABLE SPANK_VERSIONS)
zephyr_compile_definitions(${SPANK_VERSIONS})

if (NOT CONFIG_SPANK_DRIVER_UWB_TX_POWER_AUTO)
zephyr_compile_definitions(SPANK_DRIVER_UWB_TX_POWER=CONFIG_SPANK_DRIVER_UWB_TX_POWER)
endif()

zephyr_compile_definitions(SPANK_TIMEOUT_TWR=SPANK_MS_TO_SYSTICKS\(CONFIG_SPANK_TIMEOUT_TWR\))
zephyr_compile_definitions(SPANK_TIMEOUT_ELECTION=SPANK_MS_TO_SYSTICKS\(CONFIG_SPANK_TIMEOUT_ELECTION\))
zephyr_compile_definitions(SPANK_SCHEDULED_NEXT_RANGING=SPANK_MS_TO_SYSTICKS\(CONFIG_SPANK_SCHEDULED_NEXT_RANGING\))

if (CONFIG_SPANK_IO_SNIFFER_FRAME)
  zephyr_compile_definitions(SPANK_IO_SNIFFER_FRAME_SUPPORT)
endif()

if (CONFIG_SPANK_IO_SNIFFER_PACKET)
  zephyr_compile_definitions(SPANK_IO_SNIFFER_PACKET_SUPPORT)
endif()

if (CONFIG_SPANK_OUTPUT_LOCKING)
  zephyr_compile_definitions(SPANK_WITH_OUTPUT_LOCKING)
endif()

# OSAL
zephyr_include_directories(../port/zephyr/osal/include)
zephyr_library_sources(../port/zephyr/osal/src/osal.c)

# Utils
zephyr_include_directories(../utils/q/include)
zephyr_library_sources(../utils/q/src/q.c)


# Main code
zephyr_include_directories(../spank/include)
zephyr_library_sources(../spank/src/utils.c)
zephyr_library_sources(../spank/src/io.c)
zephyr_library_sources(../spank/src/types.c)
zephyr_library_sources(../spank/src/node_ctx.c)
zephyr_library_sources(../spank/src/ranging_history.c)
zephyr_library_sources(../spank/src/syslog.c)
zephyr_library_sources(../spank/src/timer.c)
zephyr_library_sources(../spank/src/neighbour.c)
zephyr_library_sources(../spank/src/node.c)
zephyr_library_sources(../spank/src/spank.c)
zephyr_library_sources(../spank/src/uav.c)	# Only weak symbols
zephyr_library_sources(../spank/src/token-sdstwr.c)

# UAV interfacing
if (CONFIG_SPANK_UAV_PUPPET)
  zephyr_include_directories(../hal/uav-puppet/include)
  zephyr_library_sources(../hal/uav-puppet/src/uav.c)
endif()

# Low level driver
zephyr_include_directories(../port/hal/io/dw1000/include)
zephyr_library_sources(../port/hal/io/dw1000/src/driver.c)

endif()
