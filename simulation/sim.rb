require 'logger'
require_relative 'lib/spank'
require_relative 'lib/spank/simulator'
require_relative 'lib/spank/simulator/node'


$logger = Logger.new($stderr, level: :debug)
$logger.formatter = proc {|severity, datetime, progname, msg|
    str = case msg
          when String     then msg
          when Exception  then "#{msg.message} (#{msg.class})\n" <<
                               (msg.backtrace || []).join("\n")
          else                   msg.inspect
          end
    
    "%s: %s\n" % [ severity[0], str ]
}




SPANK::Simulator::Node.logger = $logger

$sim = SPANK::Simulator.new(logger: $logger)
$sim.start
