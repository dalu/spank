/*
** Mapping of spank_addr_t to nickname
**  => Easing reading debug
**
** Implementation is not subtil, but we are not memory constraint.
*/

#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/tree.h>

#define SPANK_SYSLOG_PREFIX "ADDR/M"

#include "spank/config.h"
#include "spank/osal.h"
#include "spank/syslog.h"
#include "spank/types.h"
#include "address-mapping.h"


struct address_info {
    spank_addr_t addr;
    char  *nickname;
    RB_ENTRY(address_info) entries;
};

static RB_HEAD(address_map, address_info) address_map =
    RB_INITIALIZER(&address_map);


static int
address_map_sort(struct address_info *a, struct address_info *b)
{
    if      (a->addr < b->addr) { return -1; }
    else if (a->addr > b->addr) { return  1; }
    else                        { return  0; }
}

RB_GENERATE_STATIC(address_map, address_info, entries, address_map_sort);


static spank_mutex_t address_mutex;



static bool address_insert(spank_addr_t addr, char *nickname) {
    struct address_info *n = calloc(1, sizeof(struct address_info));
    SPANK_ADDR_COPY(&n->addr, &addr);
    n->nickname = nickname;
    return RB_INSERT(address_map, &address_map, n) == NULL;
}

const char *address_mapping(spank_addr_t addr) {
    spank_mutex_acquire(&address_mutex, SPANK_SYSTICKS_INFINITE);

    struct address_info find = { .addr = addr };
    struct address_info *res = RB_FIND(address_map, &address_map, &find);

    if (res == NULL) {
	struct address_info *n = calloc(1, sizeof(struct address_info));
	SPANK_ADDR_COPY(&n->addr, &addr);
	asprintf(&n->nickname, __SPANK_FMTaddr, __SPANK_FMT_ADDR(addr));
        RB_INSERT(address_map, &address_map, res);
    }
    
    spank_mutex_release(&address_mutex);
    
    return res->nickname;
}

static void __attribute__ ((constructor))
__address_mapping_init() {
    spank_mutex_init(&address_mutex);
    
    char *map = getenv("SPANK_NICKNAME_MAPPING");
    if (map == NULL) {
	SPANK_WARNING("No nickname mapping available"
		      " (define env. var. SPANK_NICKNAME_MAPPING");
	return;
    }

    /* Broadcast
     */
    address_insert(SPANK_ADDR_BROADCAST, strdup("*"));

    
    /* Parsing of mapping string:
     *   mapping  = kv (' ' kv)*
     *   kv       = nickname '=' macaddr
     *   nickname = [a-zA-Z0-9_\-]+
     *   macaddr  = aaaa:bbbb:cccc:dddd
     *    
     */
    do {
	char     nickname[62+1];  // RSVC_NICKNAME_MAXLEN
	uint16_t a, b, c, d;
	int      n;
	
	int rc = sscanf(map, " %16[^=]" "=" "%04" SCNx16 ":" "%04" SCNx16 ":"
			                    "%04" SCNx16 ":" "%04" SCNx16 " %n",
			nickname, &a, &b, &c, &d, &n);
	if (rc != 5) {
	    SPANK_WARNING("partial match for SPANK_NICKNAME_MAPPING");
	    break;
	}

	uint64_t addr_64 = ((uint64_t)a << 48) | ((uint64_t)b << 32) |
	                   ((uint64_t)c << 16) | ((uint64_t)d <<  0) ;
        uint16_t addr_16 = addr_64;

#if   SPANK_ADDR_SIZE == 2
	spank_addr_t addr = addr_16;
#elif SPANK_ADDR_SIZE == 8
	spank_addr_t addr = addr_64;
#endif

        address_insert(addr, strdup(nickname));

        map += n;
    } while(*map);
}

