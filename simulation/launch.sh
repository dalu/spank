#!/bin/sh

stdbuf -i0 -o0 -e0 ruby launch.rb | ruby log-process.rb  | (
    if [ "$1" ]; then
	tee $1
    else
	cat
    fi
)
