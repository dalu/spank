require 'socket'
require_relative 'fletcher'

module SPANK

class Simulator
    using Fletcher
    
    SERVER_PATH    = '/tmp/spank-sim'

    DW1000_BITS    = 40
    DW1000_HZ      = 499200000 * 128 # ~ 15.65 ps
    DW1000_ROUNDUP = 512
    DW1000_MASK    = (2 ** DW1000_BITS) - 1

    
    #
    # See rsvc.h
    #
    PKT_TYPE_RSVC_OPEN			= 0x0001
    PKT_TYPE_RSVC_CLOSE			= 0x0002
    PKT_TYPE_RSVC_SEED_GET 		= 0x0003

    PKT_TYPE_UWB_OPEN			= 0x0301
    PKT_TYPE_UWB_CLOSE			= 0x0302
    PKT_TYPE_UWB_IO			= 0x0303

    PKT_TYPE_CTRL_SET_CONFIGURATION	= 0xff01  # <- C
    PKT_TYPE_CTRL_SET_POSITION	    	= 0xff02  # <- C


    #
    # See port/unix/hal/io/dw1000/src/driver.c
    #
    PKT_UWB_TX				= 0x03  # <- N
    PKT_UWB_RX				= 0x04  # -> N
    PKT_UWB_TX_DONE			= 0x05  # -> N
    PKT_UWB_RX_CONFIG 			= 0x06  # <- N

    PKT_FLG_RANGING			= 0x02

    
    FRAME_MAXSIZE			= 127
    PKT_MAXLEN                  	= 520


    
    #
    # See rsvc.c
    #
    FLG_INCLUDE_NICKNAME		= 0x01
    FLG_INTERRUPT			= 0x02


    #
    #
    PKT_CONFIG_FLG_POSITION		= 0x01
    PKT_CONFIG_FLG_SEED			= 0x02
    PKT_CONFIG_FLG_RX_ERROR             = 0x03
    PKT_CONFIG_FLG_CLOCK_DRIFT		= 0x04
    
    
    def initialize(srvpath = SERVER_PATH, logger: nil)
        @logger      = logger
        @srvpath     = srvpath
        @node_config = {}

        @logger&.info "Starting network simulator on path: #{@srvpath}"
    end

    def trace(*args)
        # puts *args
    end
    
    # Start listening for simulator packet
    def start
        @sock = Socket.new(Socket::AF_UNIX, Socket::SOCK_DGRAM, 0)
        @sock.bind(Socket.pack_sockaddr_un(@srvpath))

        loop do
            msg, sender           = @sock.recvfrom(PKT_MAXLEN)
            path,                 = Socket.getnameinfo(sender)
            type, id, flags, data = msg.unpack("SQCa*")
            opts                  = { path: path }

            if !(flags & FLG_INCLUDE_NICKNAME).zero?
                nick, data = data.unpack("Z*a*")
                opts.merge!(:nick => nick,
                            :node => Node.find_by_nick(nick))
            elsif !path.nil?
                opts.merge!(:node => Node.find_by_path(path))
            end

            opts.compact!
            
            if m = case type
                   when PKT_TYPE_RSVC_OPEN         then :_open
                   when PKT_TYPE_RSVC_CLOSE        then :_close
                   when PKT_TYPE_RSVC_SEED_GET     then :_seed_get
                   when PKT_TYPE_UWB_OPEN          then :_uwb_open
                   when PKT_TYPE_UWB_CLOSE         then :_uwb_close
                   when PKT_TYPE_UWB_IO            then :_uwb_io
                   when PKT_TYPE_CTRL_SET_CONFIGURATION then :_set_configuration
                   when PKT_TYPE_CTRL_SET_POSITION      then :_set_position
                   else raise "unknown packet type (0x#{type.to_s(16)})"
                   end

                trace "Calling: #{m} / #{id} / (#{data.inspect})"
                
                status, data = self.send(m, data, **opts)
                flags        = 0
                pkt = [ type, id, status, flags, data ].pack("SQCCa*")

                trace "#{status} (#{data.inspect}): #{pkt.inspect}"
                if @sock.send(pkt, 0, sender) < 0
                    raise "sending response packet failed"
                end
            end
        end
    ensure
        File.unlink(@srvpath)
    end


    def send_interrupt(type, status, data, dst)
        pkt = [ type, 0, status, FLG_INTERRUPT, data ].pack("SQCCa*")
        if @sock.send(pkt, 0, dst) < 0
            raise "sending interrupt packet failed"
        end
    end

    
    # Get simulator reference time in ticks
    #
    # @return [Numeric] time
    def getTicks(mode = nil)
        t = (Time.now.to_r * DW1000_HZ).to_i
        case mode
        when nil    then t
        when :floor then t / DW1000_ROUNDUP * DW1000_ROUNDUP
        when :ceil  then (t + DW1000_ROUNDUP - 1) / DW1000_ROUNDUP *
                                                    DW1000_ROUNDUP
        else raise ArgumentError
        end
    end
    
    
    # Broadcast data
    #
    # @param [Node]            src          broadcasting node
    # @param [Numeric]         tx_time      transmission time
    # @param [Numeric]         tx_timestamp transmission time (sender local clock)
    # @param [Node, #include?] exclude      nodes to exclude
    def broadcast(data, src: nil, tx_time: nil, tx_timestamp: nil,
                  exclude: nil, ranging: true)
        # Build node list
        nodes = Node.each.lazy

        # Remove node explicitely excluded
        nodes = if    exclude.nil?
                    nodes
                elsif exclude.kind_of?(Node)
                    nodes.reject {|n| exclude == n        }
                elsif exclude.respond_to?(:include?)
                    nodes.reject {|n| exclude.include?(n) }
                else
                    raise ArgumentError, "unsupported exclude type"
                end

        # Sending to each nodes
        $logger.info "Broadcast start"
        nodes.each do |n|
            if n.rx_error
                if Random.rand(100.0) <= n.rx_error
                    $logger.info "Transmitting [#{src} -> #{n}] size=%d crc16=0x%04x cksum=0x%04x RX-DROPPED" % [ data.size, data[-2..-1].unpack1('v'), data.fletcher16 ]
                    next
                end
            end
            
            # Compute RX timestamp (default to 0)
            #  if enough information is provided
            rx_timestamp = 0
            if src && tx_time
                clock_hz     = DW1000_HZ
                clock_hz    *= 1 + (n.clock_drift.to_r / 1_000_000) if n.clock_drift
                rx_time      = tx_time + (src.distance(n).to_r / SPEED_OF_LIGHT)
                rx_time     *= clock_hz
#                rx_time      = tx_time + (src.distance(n).to_r / SPEED_OF_LIGHT * clock_hz)

                rx_timestamp = rx_time.to_i & DW1000_MASK
            end

            # Notify node of packet reception
            $logger.info "Transmitting [#{src} -> #{n}] size=%d crc16=0x%04x (ts=0x%016x -> 0x%016x) cksum=0x%04x" % [ data.size, data[-2..-1].unpack1('v'), tx_timestamp, rx_timestamp, data.fletcher16 ]

            flags = 0
            flags |= PKT_FLG_RANGING if ranging
            
            msg = [ 0, PKT_UWB_RX, flags, rx_timestamp, data ].pack('JCCQa*')
            send_interrupt(PKT_TYPE_UWB_IO, 0, msg, n.sockaddr)
        end
        $logger.info "Broadcast end"
    end


    #----------------------------------------------------------------------
    private

    
    # Process configuration request
    def _set_configuration(msg, nick:, **opts)
        # Parse message
        cfg = {}

        
        while !msg.empty? do
            type, len = msg.unpack("CC")
            data, msg = msg.unpack("x2a#{len}a*")
            case type
            when PKT_CONFIG_FLG_POSITION
                cfg[:position] = data.unpack("fff")
            when PKT_CONFIG_FLG_SEED
                cfg[:seed    ] = data.unpack1("L")
            when PKT_CONFIG_FLG_RX_ERROR
                cfg[:rx_error] = data.unpack1("F")
            when PKT_CONFIG_FLG_CLOCK_DRIFT
                cfg[:clock_drift] = data.unpack1("F")
            end
        end

        @logger&.info "Got configuration for node #{nick}: #{cfg.inspect}"
        @node_config[nick] = cfg

        [ 0 ]
    end

    
    # Process set position request
    def _set_position(msg, **opts)
        # Parse message and get node
        nick, x, y, z = msg.unpack("Z*fff")
        position      = [ x, y, z ]
        node          = Node.find_by_nick(nick)

        # Set new position
        node.position = position
    end

    
    # Process init request
    def _open(msg, path:, nick:)
        # Retrieve configuration
        unless cfg = @node_config[nick]
            raise "configuration not provided for node: #{nick}"
        end

        # Create node and register it
        node = Node::new(path, nick, logger: $logger)
        node.config(**cfg)
        Node.register(node)

        [ 0 ]
    end

    
    # Process destroy request
    def _close(msg, node:, **)
        Node.unregister(node)

        [ 0 ]
    end

    def _seed_get(msg, node:, **opts)
        @logger&.info "Random seed %08x send" % [ node.seed ]
        [ 0, [ node.seed ].pack("L") ]
    end
    
    def _uwb_open(msg, node:, **opts)
        drvid, = msg.unpack("J")
        @logger&.info "UWB driver created"
        [ 0 ]
    end

    def _uwb_close(msg, node:, **opts)
        drvid, = msg.unpack("J")
        @logger&.info "UWB driver detroyed"
        [ 0 ]        
    end

    def _uwb_io(msg, node:, **opts)
        drvid, type, msg = msg.unpack("JCa*")
        case type
        when PKT_UWB_TX # TX
            _uwb_io_tx(msg, node: node, drvid: drvid)
        when PKT_UWB_RX_CONFIG # RX Config
            _uwb_io_rx_config(msg, node: node, drvid: drvid)
        else raise "unknown UWB_IO type (0x#{type.to_s(16)})"
        end
    end
    
    
    # Process TX request
    def _uwb_io_tx(msg, node:, drvid:, **opts)
        # Parse message and get node
        flags, antenna_delay, data = msg.unpack('CSa*')

        trace "Got #{data.inspect} from #{node} (#{node.path})"

        # Local clock frequency
        clock_hz  = DW1000_HZ
        clock_hz *= 1 + (node.clock_drift.to_r / 1_000_000) if node.clock_drift
        
        # Compute TX timestamp
        #  (from simulator reference time to node clock time)
        tx_time  = (Time.now.to_r * clock_hz).to_i
        # DW1000 can only send to a clock boundary
        tx_time  = (tx_time + DW1000_ROUNDUP - 1) /
                   DW1000_ROUNDUP * DW1000_ROUNDUP
        # Take into account antenna delay (expressed in local ticks)
        tx_time += antenna_delay

        # TX timestamp
        tx_timestamp = tx_time & DW1000_MASK

        # Now rollback to master signal sending time
        tx_time  = tx_time / clock_hz
        
#        tx_time  = self.getTicks(:ceil)
#        tx_time += antenna_delay
#        tx_timestamp = tx_time & DW1000_MASK
        
        # Notify of TX done
        trace "UWB TX to #{node}"

        # Broadcast data
        broadcast(data, src: node, tx_time: tx_time, exclude: node,
                  tx_timestamp: tx_timestamp, # only for debugging
                  ranging: (flags & PKT_FLG_RANGING).positive?)


        msg = [ drvid, PKT_UWB_TX_DONE, tx_timestamp ].pack('JCQ')
        send_interrupt(PKT_TYPE_UWB_IO, 0, msg, node.sockaddr)

        # Done
        [ 0 ]
    end


    # Process RX config request
    def _uwb_io_rx_config(msg, node:, **opts)
        # Parse message and get node
        flags, = msg.unpack('C')

        # Apply configuration
        trace "Applying RX config #{flags.to_s(16)} for #{node}"

        # Done
        [ 0 ]
    end


end

end
