require 'set'
require 'matrix'

module SPANK
class Simulator

class Node
    @@node_by_path = {}
    @@node_by_nick = {}
    @@logger       = nil

    
    def self.logger=(logger)
        @@logger = logger
    end
    
    def self.[](nick)
        @@node_by_nick[nick]
    end

    # Find node by path
    def self.find_by_path(path)
        @@node_by_path[path]
    end

    
    # Find node by nickname
    def self.find_by_nick(nick)
        @@node_by_nick[nick]
    end


    # List of all registered nodes
    def self.all
        @@node_by_path.values
    end

    
    # Iterate on registered nodes
    def self.each(&block)
        @@node_by_path.each_value(&block)
    end

    
    # Register a node
    def self.register(node)
        if    n = @@node_by_path[node.path]
            if n.nick != node.nick
                raise "node '#{node}' already registered as '#{n}'"
            end

        elsif n = @@node_by_nick[node.nick]
            @@logger&.warn("Node (#{node}) was already registered (restart?)")
            Node.unregister(n)
            Node.register(node)
            # XXX: need to merge configuration
            raise "node restart is not handled yet"
            
        else
            @@logger&.info "Registering node #{node} with path: #{node.path}"
            @@node_by_path[node.path] = node
            @@node_by_nick[node.nick] = node
        end
        
        node
    end

    
    # Unregister a node
    def self.unregister(node)
        if @@node_by_path.delete(node.path).nil? ||
           @@node_by_nick.delete(node.nick).nil?
            @@logger&.fatal "Unregistering an unknown node (#{path})"
        else
            @@logger&.info "Node #{node} has been unregistered"
        end
        
        node
    end
    

        
    attr_reader :path, :nick
    attr_reader :configured
    attr_reader :sockaddr
    attr_reader :seed
    attr_reader :rx_error
    attr_reader :clock_drift
    
    def initialize(path, nick, logger: nil, notify: Set::new())
        @logger      = logger
        @path        = path.dup.freeze
        @nick        = nick.dup.freeze
        @sockaddr    = Socket.pack_sockaddr_un(@path).freeze
        @position    = Vector[ 0.0, 0.0, 0.0 ]
        @error       = 0;
        @rx_error    = 0.0;
        @clock_drift = 0.0;
        @seed        = Random.rand(2**32)
    end

    # Perform node configuration
    #
    # @param position node position
    # @param error    stddev for error
    #
    # @return self
    def config(position: nil, seed: nil, error: nil, rx_error: nil, clock_drift: nil)
        self.position    = position    if position
        self.seed        = seed        if seed
        self.error       = error       if error
        self.rx_error    = rx_error    if rx_error
        self.clock_drift = clock_drift if clock_drift
        self
    end

    # Set node path
    #
    # @param [String] path to socket
    #
    def path=(path)
        @path = path.dup.freeze
        @logger&.info "Node #{@nick} is changing path (assuming restart)"
        @path
    end

    # Set node position
    #
    # @param v [Vector,Array] position
    #
    # @return [Vector] assigned position
    def position=(v)
        @position = Vector === v ? v : Vector.elements(v)
        @logger&.debug "Node #{@nick} moved to < #{@position.to_a.join(', ')} >"
        @position
    end

    # Set random seed for node
    def seed=(v)
        @seed = v
        @logger&.debug "Node #{@nick} with random seed 0x%08x" % [ seed ]
        @seed
    end

    # Set network error rate (in %)
    def rx_error=(v)
        @rx_error = v
        @logger&.debug "Node #{@nick} with network rx-error rate #{rx_error}%"
        @rx_error
    end

    # Set network error rate (in %)
    def clock_drift=(v)
        @clock_drift = v
        @logger&.debug "Node #{@nick} with clock drift of #{clock_drift} ppm"
        @clock_drift
    end

    # Node position
    #
    # @return [Vector]
    def position
        @position
    end

    # Compute distance to another node
    #
    # @param  n [Node]  node used for measuring distance
    #
    # @return [Numeric] distance
    def distance(n)
        (self.position - n.position).norm
    end


    def to_s
        @nick
    end

    def ==(o)
        @path == o.path
    end

end
    
end
end
