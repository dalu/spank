require 'socket'

module SPANK
class Simulator

class Controller
    def initialize(srvpath = SERVER_PATH, logger: nil)
        @logger   = logger
        @srvpath  = srvpath
        @ctrlpath = "#{srvpath}-#{$$}"
        
        @sock = Socket.new(Socket::AF_UNIX, Socket::SOCK_DGRAM, 0)
        @sock.connect(Socket.pack_sockaddr_un(@srvpath))
        @sock.bind(Socket.pack_sockaddr_un(@ctrlpath))

        @seqid = 0;
    end

    
    def set_configuration(nick, position: nil, seed: nil, rx_error: nil, clock_drift: nil)
        # Send initial configuration
        pkt    = [ PKT_TYPE_CTRL_SET_CONFIGURATION, 0,
                   FLG_INCLUDE_NICKNAME, nick ]
        packer = "SQCZ*"

        if position
            pkt    += [ PKT_CONFIG_FLG_POSITION, 12 ] + position
            packer += "CCfff"
        end

        if seed
            pkt    += [ PKT_CONFIG_FLG_SEED, 4, seed ]
            packer += "CCL"
        end

        if rx_error
            pkt    += [ PKT_CONFIG_FLG_RX_ERROR, 4, rx_error ]
            packer += "CCF"
        end
        
        if clock_drift
            pkt    += [ PKT_CONFIG_FLG_CLOCK_DRIFT, 4, clock_drift ]
            packer += "CCF"
        end

        @sock.send(pkt.pack(packer), 0)
    end

    
    def set_position(x, y, z)
    end


    def close
        @sock.close
        File.unlink(@ctrlpath)
    end
end

end
end
