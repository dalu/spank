module SPANK
module Fletcher
    refine String do
        def fletcher16
            c0 = c1 = 0
            self.each_byte do |x|
                c0 = (c0 + x ) % 255
                c1 = (c1 + c0) % 255
            end
            c1 << 8 | c0
        end
    end
end
end
