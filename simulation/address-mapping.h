#ifndef _ADDRESS_MAPPING_
#define _ADDRESS_MAPPING_

#define _SPANK_FMTaddr "%s"
#define _SPANK_FMT_ADDR(x) address_mapping(x)

#include "spank/types.h"

const char *address_mapping(spank_addr_t addr);

#endif
