/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#if defined(__linux__)
#define _GNU_SOURCE
#endif

#include <sched.h>
#include <pthread.h>
#if defined(__FreeBSD__)
#include <pthread_np.h>
#endif
#include <sys/types.h>
#include <sys/queue.h>
#include <string.h>
#include <signal.h>

#include "rsvc.h"

#define SPANK_SYSLOG_PREFIX "MAIN  "
#include "spank/osal.h"
#include "spank/syslog.h"
#include "spank/driver.h"
#include "spank/io.h"
#include "spank/uav.h"
#include "spank/spank.h"
#include "spank/utils.h"
#include "dw1000/osal.h"


/*-- Backtrace processing ----------------------------------------------*/

#define UNW_LOCAL_ONLY
#include <libunwind.h>

void show_backtrace(void)
{
    unw_cursor_t cursor;
    unw_context_t context;
    char *nickname = spank_whoami_nickname();
    if (nickname == NULL)
	nickname = "?";
    
    // Help debugging, prefix with pid
    printf("[%s] " "---> backtrace (pid=%d) <---\n",
	   nickname, getpid());
    
    // grab the machine context and initialize the cursor
    if ((unw_getcontext(&context)          < 0) ||
	(unw_init_local(&cursor, &context) < 0) ) {
	printf("[%s] " "UNWIND: Can't get necessary information\n",
	       nickname);
	exit(1);
    }
    
    // currently the IP is within backtrace() itself so this loop
    // deliberately skips the first frame.
    while (unw_step(&cursor) > 0) {
	unw_word_t offset, pc;
	char sym[4096];
	if (unw_get_reg(&cursor, UNW_REG_IP, &pc)) {
	    printf("[%s]: " "UNWIND: cannot read program counter\n",
		   nickname);
	    exit(1);
	}
	
	printf("[%s] " "0x%16lx: ", nickname, pc);
	
	if (unw_get_proc_name(&cursor, sym, sizeof(sym), &offset) == 0) {
	    printf("(%s+0x%lx)\n", sym, offset);
	} else {
	    printf("-- no symbol name found\n");
	}
    }
}



/*----------------------------------------------------------------------*/


static spank_io_t   _spank_io;
static spank_driver_t  DW0_drv;


static pthread_t thr_dw1000_processing;
static pthread_t thr_spank;


static rsvc_t *rsvc = NULL;
static struct dw1000_emulation *emulation = NULL;

// Processing order: rx_ok / tx_done / rx_timeout / rx_error

static void
_rx_ok(dw1000_t *dw, uint32_t status, size_t length, bool ranging)
{
    spank_io_t     *io  = &_spank_io;
    spank_driver_t *dev = &DW0_drv;

    SPANK_DEBUG("rx_ok");
    _spank_io_evt_rx_received(io);

    SPANK_DEBUG("rx_ok: rx");
    spank_driver_rx_start(dev);
}

static void
_tx_done(dw1000_t *dw, uint32_t status)
{
    spank_io_t     *io  = &_spank_io;
    spank_driver_t *dev = &DW0_drv;
    
    SPANK_DEBUG("tx_done");
    _spank_io_evt_tx_sent(io);

    if (status & DW1000_FLG_SYS_STATUS_RXFCG) {
	// rx will be re-enabled in the rx_ok callback
	SPANK_DEBUG("tx_done: no-rx");
    } else if (spank_driver_tx_is_expecting_response(dev)) {
	// chipset has automatically enabled rx for us
	SPANK_DEBUG("tx_done: no-rx");
    } else {
	// that was a stand alone transmit, re-enabling rx
	SPANK_DEBUG("tx_done: rx");
	spank_driver_rx_start(dev);
    }
}

static void
_rx_timeout(dw1000_t *dw, uint32_t status)
{
    spank_io_t     *io  = &_spank_io;
    spank_driver_t *dev = &DW0_drv;

    SPANK_DEBUG("rx_timeout");
    _spank_io_evt_rx_timeout(io);

    SPANK_DEBUG("rx_timeout: rx");
    spank_driver_rx_start(dev);
}

static void
_rx_error(dw1000_t *dw, uint32_t status)
{
    spank_io_t     *io  = &_spank_io;
    spank_driver_t *dev = &DW0_drv;

    SPANK_DEBUG("rx_error");
    _spank_io_evt_rx_failed(io);

    SPANK_DEBUG("rx_error: rx");
    spank_driver_rx_start(dev);
}




__attribute__((noreturn))
static void * spank_task(void* parameters) {
    pthread_setname_np(pthread_self(), "spank");

    spank_loop();

    // NOT REACHED
    SPANK_FATAL("Reaching end of spank task");
}


struct dw1000_processing_item {
    STAILQ_ENTRY(dw1000_processing_item) entries;    /* Tail queue. */
    int pending;
};

struct dw1000_processing_queue {
    pthread_mutex_t   lock;
    pthread_cond_t    wait_data;
    STAILQ_HEAD(, dw1000_processing_item) head;
    STAILQ_HEAD(, dw1000_processing_item) free;
    struct dw1000_processing_item items[3];
} dw1000_processing_queue;


#define PROCESSING_TX 1
#define PROCESSING_IRQ 2


bool dw1000_processing_push(int type) {
    pthread_mutex_lock(&dw1000_processing_queue.lock);
    struct dw1000_processing_item *item =
	STAILQ_FIRST(&dw1000_processing_queue.free);
    int rc = true;
    
    if (item != NULL) {
	STAILQ_REMOVE_HEAD(&dw1000_processing_queue.free, entries);
	STAILQ_INSERT_TAIL(&dw1000_processing_queue.head, item, entries);
	item->pending = type;

	SPANK_DEBUG("processing: pushed item: %s",
		    type == PROCESSING_TX ? "TX" : "IRQ");
#if 0
	spank_output_lock();
	SPANK_DEBUG("processing: pushed item: %i", type);
	struct dw1000_processing_item *i;
	STAILQ_FOREACH(i, &dw1000_processing_queue.head, entries) {
	    spank_printf("- pending: %i\n", i->pending);
	}
	spank_output_unlock();
#endif
	
	pthread_cond_signal(&dw1000_processing_queue.wait_data);

    } else {
	SPANK_FATAL("processing queue overrun");
	rc = false;
    }
    pthread_mutex_unlock(&dw1000_processing_queue.lock);

    return rc;
}


bool spank_driver_post_tx(spank_driver_t *drv) {
    return dw1000_processing_push(PROCESSING_TX);

}



__attribute__((noreturn))
static void * dw1000_processing_task(void* parameters) {
    pthread_setname_np(pthread_self(), "dw1000_processing");

    struct dw1000_processing_queue *msgq = &dw1000_processing_queue;
    struct dw1000_processing_item  *item;
    for (;;) {
	int pending = 0;
	
	pthread_mutex_lock(&msgq->lock);
	
	while (STAILQ_EMPTY(&msgq->head))
	    pthread_cond_wait(&msgq->wait_data, &msgq->lock);
	
	item = STAILQ_FIRST(&msgq->head);
	SPANK_ASSERT(item != NULL);
	STAILQ_REMOVE_HEAD(&msgq->head, entries);
	STAILQ_INSERT_TAIL(&msgq->free, item, entries);
	pending = item->pending;
	pthread_mutex_unlock(&msgq->lock);

	spank_driver_t *dev = _spank_io.dev;
	
	switch(pending) {
	case PROCESSING_TX:
	    SPANK_DEBUG("processing: got tx event");
	    if (dev->processing.tx.tx_mode & SPANK_DRIVER_TX_DELAYED_START) {
		dev->processing.tx.rc =
		    spank_driver_tx_extended_sendv(dev,
						   dev->processing.tx.iovec, 2,
						   dev->processing.tx.tx_mode,
						   dev->processing.tx.ts_offset);
	    } else {
		dev->processing.tx.rc =
		    spank_driver_tx_sendv(dev,
					  dev->processing.tx.iovec, 2,
					  dev->processing.tx.tx_mode);
	    }


	    SPANK_DEBUG("processing: tx.sem give");
	    spank_semaphore_give(&dev->processing.tx.sem);
	    break;
	    
	case PROCESSING_IRQ: {
	    SPANK_DEBUG("processing: got irq event");
	    uint32_t mask =
		DW1000_FLG_SYS_MASK_MTXFRS     |
		DW1000_MSK_SYS_MASK_ALL_RX_TO  |
		DW1000_MSK_SYS_MASK_ALL_RX_ERR |
		DW1000_FLG_SYS_MASK_MRXFCG;
	    SPANK_DEBUG("dw1000 int off");
            dw1000_interrupt(&dev->dw, mask, false);
	    dw1000_process_events(&dev->dw);
	    SPANK_DEBUG("dw1000 int on");
	    dw1000_interrupt(&dev->dw, mask, true);
	}
	    break;
	}
	
    }
    
    // NOT REACHED
    SPANK_FATAL("Reaching end of dw1000_processing task");
}

void cleanup(void) {
    SPANK_DEBUG("Cleaning up");
    //dw1000_destroy(&DW0);
    if (rsvc != NULL)
	rsvc_close(rsvc);
    SPANK_DEBUG("All cleaned up");
}

void sig_exit(int sig) {
    exit(-2);
}

void sig_coredump(int sig) {
    pthread_cancel(thr_dw1000_processing);
    pthread_cancel(thr_spank);
    printf("[%d] " "Received signal %s\n", getpid(), strsignal(sig));
    show_backtrace();
    cleanup();
    
    signal(sig, SIG_DFL);
    kill(getpid(), sig);
}


static char *__nickname = NULL;
char *spank_whoami_nickname(void) {
    return __nickname;
}





void dw1000_line_cb(int line, void *arg) {
    switch(line) {
    case DW1000_IOLINE_IRQ:
	SPANK_DEBUG("line_callback");
	dw1000_processing_push(PROCESSING_IRQ);
	break;
    }
}

static struct dw1000_ioline dw1000_ioline_irq = {
    .emulation = NULL,
    .line      = DW1000_IOLINE_IRQ,
};
static struct dw1000_ioline dw1000_ioline_reset = {
    .emulation = NULL,
    .line      = DW1000_IOLINE_RESET
};
static dw1000_spi_driver_t DW0_spi  = {
    .emulation = NULL,
};

static  dw1000_config_t DW0_config  = {
    .spi              = &DW0_spi,
    .irq              = &dw1000_ioline_irq,
    .reset            = &dw1000_ioline_reset,
    .leds             = DW1000_LED_ALL,
    .leds_blink_time  = 3,
    .lde_loading      = 1, // Loading of LDE microcode
    .rxauto           = 1, // Automatically re-enable receiver
    .tx_antenna_delay = SPANK_ANTENNA_DELAY_METER_TO_CLOCK(154.6) / 2,
    .rx_antenna_delay = SPANK_ANTENNA_DELAY_METER_TO_CLOCK(154.6) / 2,
    .cb.tx_done       = _tx_done,
    .cb.rx_timeout    = _rx_timeout,
    .cb.rx_error      = _rx_error,
    .cb.rx_ok         = _rx_ok,
};



int pthread_start(pthread_t *restrict thread, int prio, void *(*start_routine)(void *)) {
    struct sched_param sched_param = { .sched_priority = prio };
    pthread_attr_t attr;
    if ((pthread_attr_init(&attr)                        < 0) ||
	(pthread_attr_setschedpolicy(&attr, SCHED_OTHER) < 0) ||
	(pthread_attr_setschedparam(&attr, &sched_param) < 0)) {
	printf("failed to set thread scheduling\n");
	exit(1);
    }
	
    return pthread_create(thread, &attr, start_routine, NULL);
}



int main(int argc, char *argv[]) {
    pthread_setname_np(pthread_self(), "main");
    
    if (argc < 5) {
	printf("Usage: prog socket_path nickname mac_addr lockfile\n");
	exit(-1);
    } 

    // Socket path
    char *socket_path = argv[1];

    // Nickname
    char *nickname = argv[2];
    if (strlen(nickname) > RSVC_NICKNAME_MAXLEN) {
	printf("Nickname is to long (only %d chars supported)\n",
	       RSVC_NICKNAME_MAXLEN);
	exit(-1);
    }
    __nickname = nickname;

    // MAC addr
    unsigned int a,b,c,d;
    int s = sscanf(argv[3], "%04x:%04x:%04x:%04x", &a, &b, &c, &d);
    if (s != 4) {
	printf("Wrong MAC address given (%d)\n", s);
	exit(-1);
    }
    uint64_t mac_addr_64 = ((uint64_t)a << 48) | ((uint64_t)b << 32) |
	                   ((uint64_t)c << 16) | ((uint64_t)d <<  0) ;

    // Lockfile
    char *lockfile = argv[4];

    // Unix specific
    spank_set_output_lockfile(lockfile);  // Set Lockfile
    
    // At exit
    atexit(cleanup);
    signal(SIGTERM, sig_exit);
    signal(SIGINT,  sig_exit);
#if defined(SIG_COREDUMP)
    signal(SIGSEGV, sig_coredump);
    signal(SIGILL,  sig_coredump);
    signal(SIGBUS,  sig_coredump);
    signal(SIGABRT, sig_coredump);
#endif

    // Print PID
    SPANK_INFO("PID = %d", getpid());

    
    // Antenna delay
    SPANK_INFO("Antenna delay tx=%d / rx=%d",
	       DW0_config.tx_antenna_delay,
	       DW0_config.rx_antenna_delay);

    // Remote service
    rsvc = rsvc_open(socket_path, nickname, NULL);
    if (rsvc == NULL) {
	printf("Failed to connect to remote service\n");
	exit(-1);
    }

    // Initialize emulation
    pthread_mutex_init(&dw1000_processing_queue.lock, NULL);
    pthread_cond_init(&dw1000_processing_queue.wait_data, NULL);
    STAILQ_INIT(&dw1000_processing_queue.head);
    STAILQ_INIT(&dw1000_processing_queue.free);
    
    for (int i = 0 ; i < 3 ; i++)
	STAILQ_INSERT_TAIL(&dw1000_processing_queue.free, 
			   &dw1000_processing_queue.items[i], entries);


    
    emulation = dw1000_emulation_create(rsvc, dw1000_line_cb, NULL);

    dw1000_ioline_irq  .emulation = emulation;
    dw1000_ioline_reset.emulation = emulation;
    DW0_spi            .emulation = emulation;

    
    // Random seed
    uint32_t seed;
    size_t seedlen = sizeof(seed);
    if (rsvc_o(rsvc, RSVC_SEED_GET, &seed, &seedlen) < 0) {
	printf("Failed to retrieve random seed\n");
	exit(-1);
    }
    SPANK_INFO("Random seed = 0x%08" PRIx32, seed);
    spank_random_seed(seed);
    
    // Init UAV
    if (!spank_uav_init()) {
	printf("Failed to initialize UAV module for SPANK\n");
	exit(-1);
    }

    
    // Init spank driver
    if (!spank_driver_init(&DW0_drv, &DW0_config)) {
	printf("Failed to initialize driver for SPANK\n");
	exit(-1);
    }

    // Initialiaze IO
    spank_io_t *io     = &_spank_io;
    io->dev            = &DW0_drv;
    io->pan_id         = 0xbccf;
    io->src_addr       = mac_addr_64;  // possible truncating
    io->time_msk       = ((uint64_t)1 << SPANK_DRIVER_TIME_BITS) - 1;
    io->metre_per_tick = SPANK_SPEED_OF_LIGHT / SPANK_DRIVER_TIME_CLOCK;
    spank_io_init(io);

    // Initialize SPANK
    spank_init(&_spank_io);

    //
    int prio_min = sched_get_priority_min(SCHED_OTHER);
    int prio_max = sched_get_priority_max(SCHED_OTHER);
    SPANK_INFO("Thread SCHED_OTHER priorities = %d .. %d", prio_min, prio_max);
	
    // Create SPANK Task
    pthread_start(&thr_dw1000_processing, prio_max, dw1000_processing_task);
    pthread_start(&thr_spank,             prio_min, spank_task);

    // Start SPANK
    spank_start();

    // Wait for termination (never happen....)
    pthread_join(thr_dw1000_processing, NULL);
    pthread_join(thr_spank,             NULL);

    // NOT REACHED
    SPANK_FATAL("Reaching end of main()");
    return 0;
}
