#!/bin/sh

#
# To ensure core dump when compiled with sanitizer:
# ASAN_OPTIONS=abort_on_error=1:disable_coredump=0:unmap_shadow_on_exit=1
#
# See:
# https://github.com/google/sanitizers/wiki/AddressSanitizerFlags
#


ROOT=~/Repos/spank

GCC="gcc13 -Wl,-rpath=/usr/local/lib/gcc13"
CLANG="clang -fuse-ld=bfd" # or -fPIC

OPTS="-fsanitize=address -fstack-protector-all -ferror-limit=2 -fno-omit-frame-pointer"

CC="$CLANG $OPTS"

DW1000=~/ZephyrProjects/modules/decawave-drivers





$CC -std=c17                                                    \
   -I /usr/local/include 					\
   -L /usr/local/lib 						\
   								\
   -I ${ROOT}/port/unix/binary-semaphore/include 		\
   -I ${ROOT}/port/unix/rsvc/include 				\
   -I ${ROOT}/port/unix/dw1000-rsvc/include			\
   -I ${ROOT}/port/unix/osal/include 				\
   								\
   -I ${DW1000}/hw/drivers/dw1000/include			\
   -I ${ROOT}/port/hal/io/dw1000/include 			\
   -I ${ROOT}/osal/condition-variable/include 			\
   -I ${ROOT}/spank/include 					\
   								\
   -D_SPANK_DISTANCE_LOG=SPANK_INFO 				\
   -DSPANK_SYSLOG_LEVEL=SPANK_SYSLOG_INFO			\
   -D_SPANK_DEBUG_CKSUM						\
   -DRSVC_WITH_SPANK_SYSLOG					\
   -D_RSVC_WITH_DEBUG						\
   -D_RSVC_WITH_DEBUG_LOCK					\
   -D"SPANK_FMTaddr=\"{\"_SPANK_FMTaddr\"}\""			\
   -D"SPANK_SCHEDULED_NEXT_RANGING=SPANK_MS_TO_SYSTICKS(20)"	\
   								\
   ${ROOT}/port/unix/binary-semaphore/src/*.c 			\
   ${ROOT}/port/unix/rsvc/src/*.c 				\
   ${ROOT}/port/unix/osal/src/*.c 				\
   ${ROOT}/port/unix/dw1000-rsvc/src/*.c 			\
   								\
   ${DW1000}/hw/drivers/dw1000/src/*.c				\
   ${ROOT}/port/hal/io/dw1000/src/*.c 				\
   ${ROOT}/osal/condition-variable/src/*.c 			\
   ${ROOT}/spank/src/*.c 					\
   								\
   ${ROOT}/simulation/neighbours_table.c 			\
   ${ROOT}/simulation/main.c 					\
   								\
   -include address-mapping.h                                   \
   ${ROOT}/simulation/address-mapping.c 			\
   								\
   -g -O0 -Wall -rdynamic -pthread -lunwind -lm
   
