require 'yaml'

$nodes = YAML.load_file('nodes.yaml')

$mac_list = Hash[$nodes.map {|n, mac:, **|
                   [ mac.strip.gsub(':', '').downcase, n ] }]

$io_event = Hash[{ 1 => 'TX_DONE',
                   2 => 'RX_DONE',
                 }.map {|k,v| [ "type=#{k}", v ] }]

$spank_rxcallback = { -1 => 'busy',
                      -2 => 'unexpected packet',
                      -3 => 'malformed packet',
                      -4 => 'processing failed',
                    }

ARGF.each do |line|
    $mac_list.each {|mac, n| line.gsub!(mac, n) }

    if line =~ /IO \s* : \s *on_event:/x
        line.sub!(/(?<=\() type=\d+ (?=\))/x) {|str|
            $io_event[str] || str
        }
    end

    puts line
end
