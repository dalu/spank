require 'logger'
require 'yaml'

require_relative 'lib/spank/simulator'
require_relative 'lib/spank/simulator/controller'

$spank_node = ARGV[0] || './a.out'
$srvpath    = ARGV[1] || SPANK::Simulator::SERVER_PATH
$lockfile   = "/tmp/spank-lockfile.#{$$}"

# Loading configuration
puts "- Loading node configuration"
$nodes = YAML.load_file('nodes.yaml')


# Instanciating simulator controler
puts "- Connecting to simulator (#{$srvpath})"
$ctrl = SPANK::Simulator::Controller.new($srvpath)
at_exit do
    puts "- Removing simulator controler path (#{$srvpath})"
    $ctrl.close
end

# Creating console lock file
puts "- Using console lock file (#{$lockfile})"
lock_cleaner = case RbConfig::CONFIG["host_os"]
               when /freebsd/ then ->() { cmd = [ 'posixshmcontrol', 'rm',
                                                  $lockfile ]
                                          system(*cmd, :out => File::NULL,
                                                       :err => File::NULL) }
               when /linux/   then ->() { File.unlink $lockfile }
               else raise "unsupported"
               end
lock_cleaner.()
at_exit do
    puts "- Ensure lock file (#{$lockfile}) is removed"
    lock_cleaner.()
end

# Appling node configuration to simulator
puts "- Configuring nodes (#{$nodes.count})"
$nodes.each do |nick, node|
    node => { mac:, position:, seed:,  ** } 
    rx_error    = node.fetch(:rx_error)    { nil }
    clock_drift = node.fetch(:clock_drift) { nil }

    if mac !~ /\A\h{4}:\h{4}:\h{4}:\h{4}\z/i
        raise "badly formated MAC address (#{mac})"
    end

    if nick.size > 62
        raise "nickname to long (#{nick})"
    end
    
    data = [ mac, nick ]
    opts = []
    if position
        opts << "pos=<%.1f, %.1f, %.1f>"
        data += position
    end
    if rx_error
        opts << "rx-error=%.1f%%"
        data << rx_error
    end
    if clock_drift
        opts << "clock-drift=%.1f ppm"
        data << clock_drift
    end
    fmt  = "  . %19s : %s"
    fmt += " (#{opts.join(', ')})" if !opts.empty?
    puts fmt % data

    $ctrl.set_configuration(nick, position: position,
                                      seed: seed,
                                  rx_error: rx_error,
                               clock_drift: clock_drift)
end

puts "- Building nick-name/mac-addr mapping"
ENV['SPANK_NICKNAME_MAPPING'] = $nodes.map { |nick, node|
    node => { mac:, ** } 
    [ nick, mac ].join('=')
}.join(' ')



# Small pause, to allow to ctrl-c
puts "- Waiting 2 secondes (it's a good time to CTRL-C)"
sleep(2)


# Spawning processes and wait
puts "- Launching SPANK processes"
$processes = []
begin
    $nodes.each {|nick, node|
        node => { mac: }
        $processes << fork do
            cmd = [ $spank_node, $srvpath, nick, mac, $lockfile ]
            # cmd.unshift('valgrind', '--tool=memcheck', '--leak-check=yes')
            begin
                puts "Exec: #{cmd.join(' ')}"
                exec(*cmd)
            rescue Errno::ENOENT
                # Notify of failed launch
                puts "Failed to launch spank node (#{nick}): #{$spank_node}"
                # Terminate launcher
                Process.kill("TERM", Process.ppid)
            end
        end
    }
    
    $processes.map {|pid| Process.waitpid(pid) }
rescue SignalException => e
    case Signal.signame(e.signo)
    when 'INT', 'TERM'
        $processes.each {|pid|
            Process.kill('TERM', pid)
        }
    else raise
    end
end
