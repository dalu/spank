/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_OSAL_H__
#define __SPANK_OSAL_H__

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "cfassert.h"
#ifndef DEBUG_PRINT
#include "debug.h"
#endif
#include "sys/_iovec.h"
#include "arm_math.h"



/*----------------------------------------------------------------------*/
/* Min/Max                                                              */
/*----------------------------------------------------------------------*/

#define SPANK_MIN(a,b)				     \
    ({						     \
	__typeof__ (a) _spank_a = (a);		     \
	__typeof__ (b) _spank_b = (b);		     \
	_spank_a < _spank_b ? _spank_a : _spank_b;   \
    })

#define SPANK_MAX(a,b)				     \
    ({						     \
	__typeof__ (a) _spank_a = (a);		     \
	__typeof__ (b) _spank_b = (b);		     \
	_spank_a > _spank_b ? _spank_a : _spank_b;   \
    })



/*----------------------------------------------------------------------*/
/* Debug / Assert / Log / Report                                        */
/*----------------------------------------------------------------------*/

#ifndef DEBUG_MODULE
#define DEBUG_MODULE "SPANK"
#endif

#define SPANK_ASSERT(x) do				\
	if (!(x)) { __asm__ __volatile__ ("bkpt #1"); } \
    while(0)

#if defined(SPANK_NO_SYSLOG)
#  define SPANK_SYSLOG(lvl, fmt, ...)
#elif defined(SPANK_SYSLOG_PREFIX)
#  define SPANK_SYSLOG(lvl, fmt, ...)					\
    DEBUG_PRINT("[%s] " SPANK_SYSLOG_PREFIX ": " fmt "\n",		\
		spank_syslog_string(lvl), ##__VA_ARGS__)
#else
#  define SPANK_SYSLOG(lvl, fmt, ...)					\
    DEBUG_PRINT("[%s] " fmt "\n",					\
		spank_syslog_string(lvl), ##__VA_ARGS__)
#endif

#define SPANK_REPORT(type, args)



/*----------------------------------------------------------------------*/
/* Timer / Alarm                                                        */
/*----------------------------------------------------------------------*/

typedef TickType_t spank_systime_t;
typedef TickType_t spank_systicks_t;

#define SPANK_TIMER_MAX_SYSTICKS   portMAX_DELAY
#define SPANK_TIMER_DELTA_MIN      0
#define SPANK_SYSTICKS_INFINITE    portMAX_DELAY
#define SPANK_SYSTICKS_PER_SECOND  (1000 / portTICK_PERIOD_MS)
#define SPANK_MS_TO_SYSTICKS(ms)   pdMS_TO_TICKS(ms)


static inline
spank_systime_t spank_systime(void) {
    return xTaskGetTickCount();
}

static inline
void spank_delay(spank_systicks_t ticks) {
    vTaskDelay(ticks);
}


extern spank_systicks_t _spank_alarm_timeout;

void _spank_set_alarm(spank_systime_t time, spank_systicks_t delta);
void _spank_stop_alarm(void);



/*----------------------------------------------------------------------*/
/* Semaphore / Mutex                                                    */
/*----------------------------------------------------------------------*/

typedef SemaphoreHandle_t spank_mutex_t;
typedef SemaphoreHandle_t spank_semaphore_t;

static inline void
spank_semaphore_init(spank_semaphore_t *sem, uint16_t count) {
    *sem = xSemaphoreCreateCounting((2 << sizeof(UBaseType_t))-1, count);
    ASSERT(*sem != NULL);
};

static inline bool
spank_semaphore_take(spank_semaphore_t *sem, spank_systicks_t timeout) {
    return xSemaphoreTake(*sem, timeout) == pdTRUE;
}

static inline void
spank_semaphore_give(spank_semaphore_t *sem) {
    xSemaphoreGive(*sem);
}


static inline void
spank_mutex_init(spank_mutex_t *mutex) {
    *mutex = xSemaphoreCreateMutex();
    ASSERT(*mutex != NULL);
};

static inline bool
spank_mutex_acquire(spank_mutex_t *mutex, spank_systicks_t timeout) {
    return xSemaphoreTake(*mutex, timeout) == pdTRUE;
}

static inline void
spank_mutex_release(spank_mutex_t *mutex) {
    xSemaphoreGive(*mutex);
}


#endif

