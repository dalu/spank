/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include "spank/osal.h"

spank_systicks_t _spank_alarm_timeout = SPANK_TIMER_MAX_SYSTICKS;

void _spank_set_alarm(spank_systime_t time, spank_systicks_t delta) {
    _spank_alarm_timeout = delta;
}

void _spank_stop_alarm(void) {
    _spank_alarm_timeout = SPANK_TIMER_MAX_SYSTICKS;
}


