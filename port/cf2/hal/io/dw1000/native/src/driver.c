/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include <sys/types.h>

#define SPANK_SYSLOG_PREFIX "DRIVER"
#include "sys/_iovec.h"
#include "spank/osal.h"
#include "spank/syslog.h"
#include "spank/driver.h"
#include "libdw1000.h"

/************************************************************************/
/*                                                                      */
/* DWM1000                                                              */
/*                                                                      */
/************************************************************************/

void spank_driver_rx_start(dwDevice_t *dev) {
    SPANK_DEBUG("RX start");
    dwNewReceive(dev);
    dwSetDefaults(dev);
    dwStartReceive(dev);
}



uint64_t _spank_driver_antennaDelay = 0;

void dwGetData_iov(dwDevice_t* dev, struct iovec *iovec, int iovcnt) {
    uint32_t offset = 0;
    for (int i = 0 ; i < iovcnt ; i++) {
	dwSpiRead(dev, RX_BUFFER, offset, iovec[i].iov_base, iovec[i].iov_len);
	offset += iovec[i].iov_len;
    }
}

void dwSetData_iov(dwDevice_t* dev, struct iovec *iovec, int iovcnt) {
    size_t size = 0;
    for (int i = 0 ; i < iovcnt ; i++) 
	size += iovec[i].iov_len;

    if (dev->frameCheck) {
	size += 2; // two bytes CRC-16
    }
    if (size > LEN_EXT_UWB_FRAMES) {
	return; // TODO proper error handling: frame/buffer size
    }
    if (size > LEN_UWB_FRAMES && !dev->extendedFrameLength) {
	return; // TODO proper error handling: frame/buffer size
    }
    
    // transmit data and length
    uint32_t offset = 0;
    for (int i = 0 ; i < iovcnt ; i++) {
	dwSpiWrite(dev, TX_BUFFER, offset, iovec[i].iov_base, iovec[i].iov_len);
	offset += iovec[i].iov_len;
    }
    dev->txfctrl[0] = (uint8_t)(size & 0xFF); // 1 byte (regular length + 1 bit)
    dev->txfctrl[1] &= 0xE0;
    dev->txfctrl[1] |= (uint8_t)((size >> 8) & 0x03);	// 2 added bits if extended length
}




void spank_driver_rx_read_frame_data(dwDevice_t *dev,
			   uint8_t *data, size_t length, size_t offset) {
    // Protect device from overreading the buffer
    if (offset > 1024)
	return;
    if ((offset + length) > 1024)
	length = 1024 - offset;
    
    // Read data
    dwSpiRead(dev, RX_BUFFER, offset, data, length);
}


bool spank_driver_init(spank_driver_t *drv) {
    return true;
}



// Local Variables:
// mode: c
// c-basic-offset: 4
// End:
