/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_DRIVER_H__
#define __SPANK_DRIVER_H__

#include <sys/types.h>
#include "sys/_iovec.h"
#include "libdw1000.h"


#define SPANK_DRIVER_TX_TIMEOUT 	SPANK_MS_TO_SYSTICKS(3)
#define SPANK_DRIVER_TIME_BITS    	40
#define SPANK_DRIVER_TS_FREQ           (499.2e6 * 128)

#define SPANK_DRIVER_FRAME_MAXSIZE      127



/*======================================================================*/

#define SPANK_DRIVER_TX_IMMEDIATE           0x00
#define SPANK_DRIVER_TX_DELAYED_START       0x01
#define SPANK_DRIVER_TX_RESPONSE_EXPECTED   0x02
#define SPANK_DRIVER_TX_RANGING             0x04



typedef dwDevice_t spank_driver_t;


extern uint64_t _spank_driver_antennaDelay;

static inline
void dw_set_antenna_delay(uint64_t delay) {
    _spank_driver_antennaDelay = delay;
}


static inline
size_t spank_driver_get_data_length(spank_driver_t *dev) {
    return dwGetDataLength(dev);
}

// inst->frame_len
void spank_driver_rx_read_frame_data(dwDevice_t *dev,
			   uint8_t *data, size_t length, size_t offset);

void dwSetData_iov(dwDevice_t* dev, struct iovec *iovec, int iovcnt);
ssize_t dw_rx_get_frame_802154(dwDevice_t *dev, void *data, size_t datalen);





void spank_driver_rx_start(dwDevice_t *dev);

static inline
uint64_t spank_driver_tx_get_timestamp(spank_driver_t *dev) {
    dwTime_t dwtime;
    dwGetTransmitTimestamp(dev, &dwtime);
    dwtime.full += _spank_driver_antennaDelay / 2;
    return dwtime.full;
}

static inline
uint64_t spank_driver_rx_get_timestamp(spank_driver_t *dev) {
    dwTime_t dwtime;
    dwGetReceiveTimestamp(dev, &dwtime);
    dwtime.full -= _spank_driver_antennaDelay / 2;
    return dwtime.full;
}

static inline
int spank_driver_tx_send(dwDevice_t *dev, void *data, size_t datalen, uint8_t tx_mode) {
    dwNewTransmit(dev);
    dwSetData(dev, data, datalen);
    dwWaitForResponse(dev, tx_mode & SPANK_DRIVER_TX_RESPONSE_EXPECTED);
    dwStartTransmit(dev);
    return 0;
}


static inline
int spank_driver_tx_sendv(dwDevice_t *dev, struct iovec *iovec, int iovcnt, uint8_t tx_mode) {
    dwNewTransmit(dev);
    dwSetData_iov(dev, iovec, iovcnt);
    dwWaitForResponse(dev, tx_mode & SPANK_DRIVER_TX_RESPONSE_EXPECTED);
    dwStartTransmit(dev);
    return 0;
}

bool spank_driver_init(spank_driver_t *drv);


#endif
