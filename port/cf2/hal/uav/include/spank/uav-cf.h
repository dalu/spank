/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK__UAV_CF_H__
#define __SPANK__UAV_CF_H__

extern spank_position_t *spank_uav_static_position;

bool spank_uav_state_feedback(const state_t *state, const uint32_t tick);


/**
 * Control if we want to feed distance coming from a mobile node.
 */
void spank_uav_set_mobile_update(bool enabled);

#endif
