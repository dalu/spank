#include "FreeRTOS.h"
#include "semphr.h"
#include "estimator_kalman.h"
#include "stabilizer_types.h"
#include "brain.h"
#include "spank/uav.h"
#include "spank/uav-cf.h"
#include "spank/syslog.h"

static spank_mutex_t spank_uav_mutex;
static state_t       spank_uav_state;
static volatile bool spank_uav_initialized = false;
static int32_t       spank_uav_distance_timeout = -1;
static volatile bool spank_uav_mobile_update = false;

spank_position_t *spank_uav_static_position = NULL;

void
spank_uav_set_mobile_update(bool enabled)
{
    spank_uav_mobile_update = enabled;
}


bool
spank_uav_state_feedback(const state_t *state, const uint32_t tick) {
    // Protect against use before initialisation
    if (! spank_uav_initialized) {
	return false;
    }

    // If not anchor-like behaviour (static position)
    // save the state
    if (spank_uav_static_position == NULL) {
	spank_mutex_acquire(&spank_uav_mutex, SPANK_SYSTICKS_INFINITE);
	memcpy(&spank_uav_state, state, sizeof(state_t));
	spank_mutex_release(&spank_uav_mutex);
    }

    // Sanity check
    /*
    if (spank_uav_distance_timeout >= 0) {
	spank_uav_distance_timeout -= 1;
	if (spank_uav_distance_timeout < 0) {
	    stabilizerSetEmergencyStop();
	}
    }
    */    
    return true;
}

void
spank_uav_get_posture(spank_posture_t *posture)
{
    // Ensure clean structure
    //  Position = 0,0,0  <no-stddev>
    //  Velocity = none   <no-stddev>
    memset(posture, 0, sizeof(*posture));

    // Look for anchor-like behaviour with static position
    if (spank_uav_static_position != NULL) {
	memcpy(&posture->position.value, spank_uav_static_position,
	       sizeof(spank_position_t));
	return;
    }

    // Save position & velocity
    spank_mutex_acquire(&spank_uav_mutex, SPANK_SYSTICKS_INFINITE);
    posture->position.value.x = spank_uav_state.position.x * 100;
    posture->position.value.y = spank_uav_state.position.y * 100;
    posture->position.value.z = spank_uav_state.position.z * 100;
    posture->velocity.value.x = spank_uav_state.velocity.x * 100;
    posture->velocity.value.y = spank_uav_state.velocity.y * 100;
    posture->velocity.value.z = spank_uav_state.velocity.z * 100;
    spank_mutex_release(&spank_uav_mutex);
}

void
spank_uav_feed_distance(spank_distance_t dist, spank_stddev_t stddev,
			spank_posture_t *posture, uint64_t id, void *)
{
#if 0
    DEBUG_PRINT("Injecting TWR distance = %d / xyz=%d,%d,%d\n",
		dist,
		posture->position.value.x,
		posture->position.value.y,
		posture->position.value.z);
#endif
    // Make a distinction between fixed (ie: no velocity)
    // nodes (ie: anchor-like) and moving nodes (ie: uav-like)
    bool is_fixed = SPANK_VELOCITY_IS_ZERO(posture->velocity.value);

    // Notify UAV host of new information
    //  - always          if comming from a fixed node
    //  - only if enabled if comming from a moving node
    if (!is_fixed && !spank_uav_mobile_update) {
	return;
    }
    
    // Ensure stddev is not null as it is not supported in the kalman filter
    // XXX: it would be better to use an interval
    SPANK_ASSERT(stddev > 0);
    
    distanceMeasurement_t dist_measurement = {
	.distance = (float)dist                      / 100.0f,
	.x        = (float)posture->position.value.x / 100.0f,
	.y        = (float)posture->position.value.y / 100.0f,
	.z        = (float)posture->position.value.z / 100.0f,
	.stdDev   = (float)stddev                    / 100.0f,
    };

    estimatorKalmanEnqueueDistance(&dist_measurement);

    //spank_uav_distance_timeout = 1000;
}


void
spank_uav_control(int cmd, void *args)
{
    switch(cmd) {
    // Emergency shutdown
    case SPANK_UAV_CTRL_EMERGENCY_SHUTDOWN:
	brainShutdown();
	break;

    // Transmit take off order
    case SPANK_UAV_CTRL_TAKEOFF:
	brainTakeOff(0);
	break;

    // Transmit landing order
    case SPANK_UAV_CTRL_LANDING:
	brainLanding();
	break;

    default:
	SPANK_DEBUG("Unknown UAV control order 0x%x", cmd);
	break;
    }
}

bool
spank_uav_init(void)
{
    spank_mutex_init(&spank_uav_mutex);
    
    return spank_uav_initialized = true;
}
