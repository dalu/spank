#include <zephyr/init.h>

#include "spank/osal.h"
#include "spank/syslog.h"


/*
 * Whoami
 */
char * __attribute__((weak))
spank_whoami_nickname(void) {
    return "<undefined>";
}

/*
 * Alarm
 */
spank_systicks_t _spank_alarm_timeout = SPANK_TIMER_MAX_SYSTICKS;

void _spank_set_alarm(spank_systime_t time, spank_systicks_t delta) {
    _spank_alarm_timeout = delta;
}

void _spank_stop_alarm(void) {
    _spank_alarm_timeout = SPANK_TIMER_MAX_SYSTICKS;
}



/*
 * Syslog
 */
#if defined(CONFIG_SPANK_SYSLOG_RUNTIME)

static int spank_syslog_level = SPANK_SYSLOG_LEVEL;

void spank_syslog_setlevel(int lvl) {
    spank_syslog_level = lvl;
}

int spank_syslog_getlevel(void) {
    return spank_syslog_level;
}

void spank_syslog(int lvl, const char *prefix, const char *fmt, ...) {
    if (lvl > spank_syslog_level) 
	return;

    const char *lvlstr = spank_syslog_string(lvl);
    if (lvlstr == NULL)
	lvlstr = "?";

    printk("[%c] ", lvlstr[0]);
    if (prefix != NULL) 
    	printk("%s: ", prefix);

    va_list args;
    va_start(args, fmt);
    vprintk(fmt, args);
    va_end(args);
    printk("\n");
}

#endif


/*
 * Notify
 */
#if defined(CONFIG_SPANK_REPORT)

void __attribute__((weak))
spank_report(int type, void *args)
{
}



/*
 * Output locking
 */
#if defined(SPANK_WITH_OUTPUT_LOCKING)
static spank_mutex_t _spank_output_mutex;

void spank_output_lock(void) {
    spank_mutex_acquire(&_spank_output_mutex, SPANK_SYSTICKS_INFINITE);
}
void spank_output_unlock(void) {
    spank_mutex_release(&_spank_output_mutex);
}
#endif


/*
 * Specific initialisations
 */
static int _spank_osal_init(void) {
#if defined(SPANK_WITH_OUTPUT_LOCKING)
    spank_mutex_init(&_spank_output_mutex);
#endif
    return 0;   
}
SYS_INIT(_spank_osal_init, POST_KERNEL, 0);
	 
#endif
