#ifndef __SPANK_OSAL_H__
#define __SPANK_OSAL_H__

#include <version.h>

#if (KERNEL_VERSION_NUMBER < 0x030100) || defined(CONFIG_LEGACY_INCLUDE_PATH)
#include <kernel.h>
#else
#include <zephyr/kernel.h>
#include <zephyr/init.h>
#endif

#include <stdio.h>
#include "sys/_iovec.h"


/*----------------------------------------------------------------------*/
/* Constrructors / Destructors                                           */
/*----------------------------------------------------------------------*/

#define __SPANK_CONSTRUCTOR(fn)					\
    static inline void fn(void);				\
    static int fn##_int(void);					\
    static int fn##_int(void) { fn() ; return 0; }		\
    SYS_INIT(fn##_int, POST_KERNEL, 1);				\
    static void fn(void)



/*----------------------------------------------------------------------*/
/* Min/Max                                                              */
/*----------------------------------------------------------------------*/

#define SPANK_MIN(a,b)				     \
    ({						     \
	__typeof__ (a) _spank_a = (a);		     \
	__typeof__ (b) _spank_b = (b);		     \
	_spank_a < _spank_b ? _spank_a : _spank_b;   \
    })

#define SPANK_MAX(a,b)				     \
    ({						     \
	__typeof__ (a) _spank_a = (a);		     \
	__typeof__ (b) _spank_b = (b);		     \
	_spank_a > _spank_b ? _spank_a : _spank_b;   \
    })



/*----------------------------------------------------------------------*/
/* Ease crazyflie debug                                                 */
/*----------------------------------------------------------------------*/

#define DEBUG_PRINT(x...)



/*----------------------------------------------------------------------*/
/* Whoami                                                               */
/*----------------------------------------------------------------------*/

char *spank_whoami_nickname(void);



/*----------------------------------------------------------------------*/
/* Types                                                                */
/*----------------------------------------------------------------------*/

typedef float  float32_t;
typedef double float64_t;



/*----------------------------------------------------------------------*/
/* Stats                                                                */
/*----------------------------------------------------------------------*/

#define SPANK_STAT(var) atomic_val_t var
#define SPANK_STAT_INC(var) atomic_inc(&var)



/*----------------------------------------------------------------------*/
/* Debug / Assert / Log / Report                                        */
/*----------------------------------------------------------------------*/

#define spank_printf(...)						\
    do {								\
	spank_output_lock();						\
	printk(__VA_ARGS__);						\
	spank_output_unlock();						\
    } while(0)

#if defined(CONFIG_SPANK_ASSERT)
#define SPANK_ASSERT(x)                                                 \
    do {								\
	if (!(x)) {							\
	    __ASSERT_LOC(x);						\
	    k_panic();							\
	}								\
    } while (0)
#else
#define SPANK_ASSERT(x)
#endif

#if defined(SPANK_NO_SYSLOG)
#  define SPANK_SYSLOG(lvl, fmt, ...)
#elif defined(SPANK_SYSLOG_PREFIX)
#  if defined(CONFIG_SPANK_SYSLOG_RUNTIME)
#    define SPANK_SYSLOG(lvl, fmt, ...)					\
    do {								\
	spank_output_lock();						\
	spank_syslog(lvl, SPANK_SYSLOG_PREFIX, fmt, ##__VA_ARGS__);	\
	spank_output_unlock();						\
    } while(0)
#  elif defined(CONFIG_SPANK_SYSLOG_LEVEL_NONE)
#    define SPANK_SYSLOG(lvl, fmt, ...)
#  else
#define SPANK_SYSLOG(lvl, fmt, ...)					\
    do {								\
	spank_output_lock();						\
	printk("[%c] " SPANK_SYSLOG_PREFIX ": " fmt "\n",		\
	       spank_syslog_string(lvl)[0], ##__VA_ARGS__);		\
	spank_output_unlock();						\
    } while(0)
#  endif
#else
#  if defined(CONFIG_SPANK_SYSLOG_RUNTIME)
#    define SPANK_SYSLOG(lvl, fmt, ...)					\
    do {								\
	spank_output_lock();						\
	spank_syslog(lvl, NULL, fmt, ##__VA_ARGS__);			\
	spank_output_unlock();						\
    } while(0)
#  elif defined(CONFIG_SPANK_SYSLOG_LEVEL_NONE)
#    define SPANK_SYSLOG(lvl, fmt, ...)
#  else
#    define SPANK_SYSLOG(lvl, fmt, ...)					\
    do {								\
	spank_output_lock();						\
	printk("[%c] " fmt "\n",					\
	       spank_syslog_string(lvl)[0], ##__VA_ARGS__);		\
	spank_output_unlock();						\
    } while(0)
#  endif
#endif

#if defined(CONFIG_SPANK_SYSLOG_RUNTIME)
#define SPANK_SYSLOG_RUNTIME 1
void spank_syslog(int lvl, const char *prefix, const char *fmt, ...)
    __attribute__ ((format (printf, 3, 4)));
void spank_syslog_setlevel(int lvl);
int spank_syslog_getlevel(void);
#endif


#if   defined(CONFIG_SPANK_SYSLOG_LEVEL_NONE)
#  define SPANK_SYSLOG_LEVEL SPANK_SYSLOG_NONE
#elif defined(CONFIG_SPANK_SYSLOG_LEVEL_FATAL)
#  define SPANK_SYSLOG_LEVEL SPANK_SYSLOG_FATAL
#elif defined(CONFIG_SPANK_SYSLOG_LEVEL_ERROR)
#  define SPANK_SYSLOG_LEVEL SPANK_SYSLOG_ERROR
#elif defined(CONFIG_SPANK_SYSLOG_LEVEL_WARNING)
#  define SPANK_SYSLOG_LEVEL SPANK_SYSLOG_WARNING
#elif defined(CONFIG_SPANK_SYSLOG_LEVEL_INFO)
#  define SPANK_SYSLOG_LEVEL SPANK_SYSLOG_INFO
#elif defined(CONFIG_SPANK_SYSLOG_LEVEL_DEBUG)
#  define SPANK_SYSLOG_LEVEL SPANK_SYSLOG_DEBUG
#else
#  error SPANK_SYSLOG_LEVEL has not been defined
#endif


void spank_report(int type, void *args);
#if defined(CONFIG_SPANK_REPORT)
#define SPANK_REPORT(type, args) 					\
    spank_report(SPANK_REPORT_##type, (void *)(args))
#else
#define SPANK_REPORT(type, args)
#endif



/*----------------------------------------------------------------------*/
/* Timer / Alarm                                                        */
/*----------------------------------------------------------------------*/

#if (KERNEL_VERSION_NUMBER < 0x020300) || defined(CONFIG_LEGACY_TIMEOUT_API)

typedef int64_t   spank_systime_t;
typedef uint32_t  spank_systicks_t;

#define SPANK_FMTsystime  PRIi64
#define SPANK_FMTsysticks PRIu32

#define SPANK_TIMER_MAX_SYSTICKS	((uint32_t)-1)
#define SPANK_TIMER_DELTA_MIN    	0
#define SPANK_SYSTICKS_INFINITE  	K_FOREVER
#define SPANK_SYSTICKS_PER_SECOND 	1000
#define SPANK_MS_TO_SYSTICKS(ms)					\
    (((ms) * SPANK_SYSTICKS_PER_SECOND + 999) / 1000 )

static inline spank_systime_t
spank_systime(void) {
    return k_uptime_get();
}

static inline void
spank_delay(spank_systicks_t ticks) {
    while (ticks != 0) {
	ticks = k_sleep(ticks);
    }
}

#else

typedef int64_t   spank_systime_t;
typedef k_ticks_t spank_systicks_t;

#define SPANK_FMTsystime  PRIi64
#define SPANK_FMTsysticks PRIu32

#define SPANK_TIMER_MAX_SYSTICKS					\
    (((1ULL<<(sizeof(k_ticks_t)*CHAR_BIT-2))-1)*2+1)
#define SPANK_TIMER_DELTA_MIN    	0
#define SPANK_SYSTICKS_INFINITE  	K_TICKS_FOREVER
#define SPANK_SYSTICKS_PER_SECOND 	CONFIG_SYS_CLOCK_TICKS_PER_SEC
#define SPANK_MS_TO_SYSTICKS(ms)					\
    (((ms) * SPANK_SYSTICKS_PER_SECOND + 999) / 1000)

static inline spank_systime_t
spank_systime(void) {
    return k_uptime_ticks();
}

static inline void
spank_delay(spank_systicks_t ticks) {
#if KERNEL_VERSION_NUMBER < 0x030500
    uint64_t end       = sys_clock_timeout_end_calc(Z_TIMEOUT_TICKS(ticks));
    int64_t  remaining;

    while ((remaining = end - sys_clock_tick_get()) > 0) {
	if (! k_sleep(Z_TIMEOUT_TICKS(remaining)))
	    return;
    }
#else
    k_timepoint_t timepoint = sys_timepoint_calc(K_TICKS(ticks));

    while(1) {
	k_timeout_t timeout = sys_timepoint_timeout(timepoint);
	if (! k_sleep(timeout))
	    return;
    }
#endif
}

#endif

extern spank_systicks_t _spank_alarm_timeout;

void _spank_set_alarm(spank_systime_t time, spank_systicks_t delta);
void _spank_stop_alarm(void);



/*----------------------------------------------------------------------*/
/* Semaphore / Mutex                                                    */
/*----------------------------------------------------------------------*/

typedef struct k_mutex   spank_mutex_t;
typedef struct k_sem     spank_semaphore_t;
typedef struct k_condvar spank_cv_t;

static inline void
spank_semaphore_init(spank_semaphore_t *sem, uint16_t count) {
    k_sem_init(sem, count, (unsigned int)(-1));
};

static inline bool
spank_semaphore_take(spank_semaphore_t *sem, spank_systicks_t timeout) {
#if (KERNEL_VERSION_NUMBER < 0x020300) || defined(CONFIG_LEGACY_TIMEOUT_API)
    int rc = k_sem_take(sem, timeout);
#else
    int rc = k_sem_take(sem, K_TICKS(timeout));
#endif
    __ASSERT((rc == 0) || (rc == -EAGAIN) || (rc == -EBUSY),
	     "unexpected semaphore return value (%d)", rc);
    return rc == 0;
}

static inline void
spank_semaphore_give(spank_semaphore_t *sem) {
    k_sem_give(sem);
}

static inline void
spank_mutex_init(spank_mutex_t *mutex) {
    k_mutex_init(mutex);
};

static inline bool
spank_mutex_acquire(spank_mutex_t *mutex, spank_systicks_t timeout) {
#if (KERNEL_VERSION_NUMBER < 0x020300) || defined(CONFIG_LEGACY_TIMEOUT_API)
    int rc = k_mutex_lock(mutex, timeout);
#else
    int rc = k_mutex_lock(mutex, K_TICKS(timeout));
#endif
    __ASSERT((rc == 0) || (rc == -EAGAIN) || (rc == -EBUSY),
	     "unexpected mutex return value (%d)", rc);
    return rc == 0;
}

static inline void
spank_mutex_release(spank_mutex_t *mutex) {
    k_mutex_unlock(mutex);
}

#if (KERNEL_VERSION_NUMBER >= 0x020500)
#define SPANK_CV_NATIVE
static inline void
spank_cv_init(spank_cv_t *cv) {
    k_condvar_init(cv);
}

static inline bool
spank_cv_wait(spank_cv_t *cv, spank_mutex_t *mutex, spank_systicks_t timeout) {
#if (KERNEL_VERSION_NUMBER < 0x020300) || defined(CONFIG_LEGACY_TIMEOUT_API)
    int rc = k_condvar_wait(cv, mutex, timeout);
#else
    int rc = k_condvar_wait(cv, mutex, K_TICKS(timeout));
#endif   
    __ASSERT((rc == 0) || (rc == -EAGAIN),
	     "unexpected condition-variable return value (%d)", rc);
    return rc == 0;
}

static inline void
spank_cv_signal(spank_cv_t *cv) {
    k_condvar_signal(cv);
}

static inline void
spank_cv_broadcast(spank_cv_t *cv) {
    k_condvar_broadcast(cv);
}
#endif

/*----------------------------------------------------------------------*/
/* Output locking                                                       */
/*----------------------------------------------------------------------*/

#if defined(SPANK_WITH_OUTPUT_LOCKING)
void spank_output_lock(void);
void spank_output_unlock(void);
#else
#define spank_output_lock(...)
#define spank_output_unlock(...)
#endif

#endif

