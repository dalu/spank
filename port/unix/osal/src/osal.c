/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include "spank/osal.h"
#include "spank/syslog.h"
#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <pthread.h>
#include <stdatomic.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>

/*----------------------------------------------------------------------*/
/* Unportable                                                           */
/*----------------------------------------------------------------------*/


static struct spank_shared_data {
    atomic_int initialized;
    pthread_mutex_t mutex;
} *_spank_shared_data = NULL;
    

void spank_set_output_lockfile(char *filename) {
    int fd    = - 1;
    int retry = 100;

    struct spank_shared_data *shared = NULL;
    // Try to create shared memory
    // Using execution bit to mark as "not-intialized"
    fd = shm_open(filename, O_RDWR|O_CREAT|O_EXCL|O_TRUNC, 0700);

    // Created, so initialization is on our side
    if (fd >= 0) {
	if ((ftruncate(fd, sizeof(struct spank_shared_data)) < 0) ||
	    ((shared = mmap(NULL, sizeof(struct spank_shared_data),
			    PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0)
	      ) == MAP_FAILED)) {
	    shm_unlink(filename);
	    close(fd);
	    goto failed;
	}

        // Zero
	memset(shared, 0, sizeof(struct spank_shared_data));

	// Create mutex	
        pthread_mutexattr_t attr;

        pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
	pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);
	pthread_mutex_init(&shared->mutex, &attr);
	pthread_mutexattr_destroy(&attr);

	// Mark as ready
        fchmod(fd, 0600);

    // Oups
    } else if (errno != EEXIST) {
	SPANK_ASSERT(errno != EEXIST);

    // Open existing shared memory
    } else { 
	fd = shm_open(filename, O_RDWR, 0600);
	assert(fd >= 0);

	// Spin wait on excution bit
	while (retry--) {
	    struct stat sb;
	    fstat(fd, &sb);
	    if (! (sb.st_mode & S_IXUSR))
		break;
	    usleep(25);
	}
	assert(retry >= 0);
	

        if ((shared = mmap(NULL, sizeof(struct spank_shared_data),
			   PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0)
	     ) == MAP_FAILED) {
	    close(fd);
	    goto failed;
	}
    }

    // Assigned pointer
    _spank_shared_data = shared;
    return ;
    
 failed:
    assert(0);
}



/*----------------------------------------------------------------------*/
/* Whoami                                                               */
/*----------------------------------------------------------------------*/

char * __attribute__((weak))
spank_whoami_nickname(void) {
    static char *string = "?";
    return string;
}


/*----------------------------------------------------------------------*/
/* Debug / Assert / Log / Report                                        */
/*----------------------------------------------------------------------*/

int
spank_vprintf(const char *format, va_list ap)
{
    char *nickname = spank_whoami_nickname();

    // Fast path, if nickname is not defined
    if (nickname == NULL)
	return vprintf(format, ap);

    // Newline
    static int newline = 1;

    // Format string into an allocated buffer
    char *str      = NULL;
    int   rc       = vasprintf(&str, format, ap);

    // Check result
    if (rc < 0)
	goto failed;

    // Prefix output lines
    spank_output_lock();
    for (char *c = str ; *c ; c++) {
	if (newline   ) { printf("<%s> ", nickname); newline = 0; }
	if (*c == '\n') {                            newline = 1; }
	putchar(*c);
    }
    fflush(stdout);
    spank_output_unlock();

    // Done
 failed:
    free(str);
    return rc;
 }
 
int
spank_printf(const char *format, ...)     
{    
    va_list ap;
    int     rc;

    va_start(ap, format);
    rc = spank_vprintf(format, ap);
    va_end(ap);

    return rc;
}



void
__spank_print_assert (const char* snippet, const char* file, int line,
		      const char* message, ...)
{

    if (*message) {
	spank_output_lock();
        spank_printf("assert failed %s:%d %s (", file, line, snippet);
	va_list ap;
	va_start(ap, message);
	char *format = va_arg(ap, char *);
	spank_vprintf(format, ap);
	va_end(ap);
	spank_printf(")\n");
        spank_output_unlock();
    } else {
	spank_printf("assert failed %s:%d %s\n", file, line, snippet);
    }

    abort();
}


/*----------------------------------------------------------------------*/
/* Timer / Alarm                                                        */
/*----------------------------------------------------------------------*/


void
spank_delay(spank_systicks_t ticks) {
    usleep(ticks * (1000000 / SPANK_SYSTICKS_PER_SECOND));
}

spank_systime_t
spank_systime(void) {
    /* Using CLOCK_REALTIME here, as the implementation must be
     * coherent with the one provided for timeout in mutex/semaphore
     * Unfortunately unix mutex/semaphore only allows CLOCK_REALTIME
     */
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    uint64_t time = (ts.tv_nsec / (1000000000 / SPANK_SYSTICKS_PER_SECOND)) +
	            (ts.tv_sec * SPANK_SYSTICKS_PER_SECOND);
    return time % 4294967295;
}


spank_systicks_t _spank_alarm_timeout = SPANK_TIMER_MAX_SYSTICKS;

void _spank_set_alarm(spank_systime_t time, spank_systicks_t delta) {
    _spank_alarm_timeout = delta;
}

void _spank_stop_alarm(void) {
    _spank_alarm_timeout = SPANK_TIMER_MAX_SYSTICKS;
}



/*----------------------------------------------------------------------*/
/* Semaphore / Mutex / Condition variable                               */
/*----------------------------------------------------------------------*/

void
_spank_ticks_to_abstime(spank_systicks_t ticks, struct timespec *ts) {
    clock_gettime(CLOCK_REALTIME, ts);
    ts->tv_sec  += ticks / SPANK_SYSTICKS_PER_SECOND;
    ts->tv_nsec += (ticks % SPANK_SYSTICKS_PER_SECOND) *
	          (1000000000 / SPANK_SYSTICKS_PER_SECOND);
    if (ts->tv_nsec > 1000000000) {
	ts->tv_sec  += 1;
	ts->tv_nsec -= 1000000000;
    }
}

void
spank_semaphore_init(spank_semaphore_t *sem, uint16_t count) {
    int rc = sem_init(sem, 0, count);
    assert(rc == 0);
};

void
spank_semaphore_destroy(spank_semaphore_t *sem) {
    int rc = sem_destroy(sem);
    assert(rc == 0);
};

bool
spank_semaphore_take(spank_semaphore_t *sem, spank_systicks_t timeout) {
    int rc;
    if (timeout == SPANK_SYSTICKS_INFINITE) {
	do {
	    rc = sem_wait(sem);
	} while ((rc == -1) && (errno == EINTR));
	assert(rc == 0);
    } else {
	struct timespec ts;
	_spank_ticks_to_abstime(timeout, &ts);
	do {
	    rc = sem_timedwait(sem, &ts);
	} while ((rc == -1) && (errno == EINTR));
	assert((rc == 0) || (errno == ETIMEDOUT));
    }
    return rc == 0;
}

void
spank_semaphore_give(spank_semaphore_t *sem) {
    int rc = sem_post(sem);
    assert(rc == 0);
}

void
spank_mutex_init(spank_mutex_t *mutex) {
    int rc = pthread_mutex_init(mutex, NULL);
    assert(rc == 0);
};

void
spank_mutex_destroy(spank_mutex_t *mutex) {
    int rc = pthread_mutex_destroy(mutex);
    assert(rc == 0);
};

bool
spank_mutex_acquire(spank_mutex_t *mutex, spank_systicks_t timeout) {
    int rc = 0;
    if (timeout == SPANK_SYSTICKS_INFINITE) {
	rc = pthread_mutex_lock(mutex);
	assert(rc == 0);
    } else {
	struct timespec ts;
	_spank_ticks_to_abstime(timeout, &ts);
        rc = pthread_mutex_timedlock(mutex, &ts);
	assert((rc == 0) || (rc == ETIMEDOUT));
    }
    return rc == 0;
}

void
spank_mutex_release(spank_mutex_t *mutex) {
    int rc = pthread_mutex_unlock(mutex);
    assert(rc == 0);
}

void
spank_cv_init(spank_cv_t *cv) {
    int rc = pthread_cond_init(cv, NULL);
    assert(rc == 0);
}

bool
spank_cv_wait(spank_cv_t *cv, spank_mutex_t *mutex, spank_systicks_t timeout) {
    int rc = 0;
    if (timeout == SPANK_SYSTICKS_INFINITE) {
	rc = pthread_cond_wait(cv, mutex);
	assert(rc == 0);
    } else {
	struct timespec ts;
	_spank_ticks_to_abstime(timeout, &ts);
	rc = pthread_cond_timedwait(cv, mutex, &ts);
	assert((rc == 0) || (rc == ETIMEDOUT));
    }
    return rc == 0;
}

void spank_cv_signal(spank_cv_t *cond) {
    int rc = pthread_cond_signal(cond);
    assert(rc == 0);
}

void spank_cv_broadcast(spank_cv_t *cond) {
    int rc = pthread_cond_broadcast(cond);
    assert(rc == 0);
}


/*----------------------------------------------------------------------*/
/* Output locking                                                       */
/*----------------------------------------------------------------------*/

void
spank_output_lock(void) {
    if (_spank_shared_data == NULL)
	return;
    int rc = pthread_mutex_lock(&_spank_shared_data->mutex);
    SPANK_ASSERT((rc == 0) || (rc == EOWNERDEAD));

    if (rc == EOWNERDEAD) {
	pthread_mutex_consistent(&_spank_shared_data->mutex);
    }
}

void
spank_output_unlock(void) {
    if (_spank_shared_data == NULL)
	return;
    int rc = pthread_mutex_unlock(&_spank_shared_data->mutex);
    SPANK_ASSERT(rc == 0);
}
