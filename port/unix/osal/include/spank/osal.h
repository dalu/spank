/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_OSAL_H__
#define __SPANK_OSAL_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <sys/time.h>
#include <assert.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

/*
** Inline functions must be avoided, as we are porting on Unix,
** we don't have the performence constraint of embedded system
** but we want to easy debug.
**
**  => Inline functions don't provide easy debugging.
*/


/*----------------------------------------------------------------------*/
/* Constructors / Destructors                                           */
/*----------------------------------------------------------------------*/

#define __SPANK_CONSTRUCTOR(fn)					\
    static void __attribute__((constructor)) fn(void)



/*----------------------------------------------------------------------*/
/* Min/Max                                                              */
/*----------------------------------------------------------------------*/

#define SPANK_MIN(a,b)				     \
    ({						     \
	__typeof__ (a) _spank_a = (a);		     \
	__typeof__ (b) _spank_b = (b);		     \
	_spank_a < _spank_b ? _spank_a : _spank_b;   \
    })

#define SPANK_MAX(a,b)				     \
    ({						     \
	__typeof__ (a) _spank_a = (a);		     \
	__typeof__ (b) _spank_b = (b);		     \
	_spank_a > _spank_b ? _spank_a : _spank_b;   \
    })



/*----------------------------------------------------------------------*/
/* Ease crazyflie debug                                                 */
/*----------------------------------------------------------------------*/

#define DEBUG_PRINT(x...)



/*----------------------------------------------------------------------*/
/* Unportable                                                           */
/*----------------------------------------------------------------------*/

void spank_set_output_lockfile(char *str);



/*----------------------------------------------------------------------*/
/* Whoami                                                               */
/*----------------------------------------------------------------------*/

char *spank_whoami_nickname(void);



/*----------------------------------------------------------------------*/
/* Types                                                                */
/*----------------------------------------------------------------------*/

typedef float  float32_t;
typedef double float64_t;



/*----------------------------------------------------------------------*/
/* Stats                                                                */
/*----------------------------------------------------------------------*/

#define SPANK_STAT(var) _Atomic(uint32_t) var
#define SPANK_STAT_INC(var) atomic_fetch_add(&var, 1)



/*----------------------------------------------------------------------*/
/* Debug / Assert / Log / Report                                        */
/*----------------------------------------------------------------------*/


int __attribute__ ((format(printf, 1, 2)))
spank_printf(const char *format, ...);

int spank_vprintf(const char *format, va_list ap);

void __attribute__ ((format(printf, 4, 5)))
__spank_print_assert(const char* snippet, const char* file, int line,
		     const char* message, ...);

// Always build with assert
// #define SPANK_ASSERT(x) assert(x)
#define SPANK_ASSERT(x, ...)						\
    do {								\
	if (!(x)) {							\
	    __spank_print_assert(#x, __FILE__, __LINE__,		\
			 #__VA_ARGS__ __VA_OPT__(,) ##__VA_ARGS__); 	\
	}								\
    } while (0)								\

#if defined(SPANK_NO_SYSLOG)
#  define SPANK_SYSLOG(lvl, fmt, ...)
#elif defined(SPANK_SYSLOG_PREFIX)
#  define SPANK_SYSLOG(lvl, fmt, ...)					\
    do {								\
	spank_output_lock();						\
	spank_printf("[%c] " SPANK_SYSLOG_PREFIX ": " fmt "\n",		\
	       spank_syslog_string(lvl)[0], ##__VA_ARGS__);		\
	spank_output_unlock();						\
    } while(0)
#else
#  define SPANK_SYSLOG(lvl, fmt, ...)					\
    do {								\
	spank_output_lock();						\
	spank_printf("[%c] " fmt "\n",					\
	       spank_syslog_string(lvl)[0], ##__VA_ARGS__);		\
	spank_output_unlock();						\
    } while(0)
#endif

#define SPANK_REPORT(type, args)



/*----------------------------------------------------------------------*/
/* Timer / Alarm                                                        */
/*----------------------------------------------------------------------*/

typedef uint32_t spank_systime_t;
typedef uint32_t spank_systicks_t;

#define SPANK_FMTsystime  PRIu32
#define SPANK_FMTsysticks PRIu32

#define SPANK_TIMER_MAX_SYSTICKS  	((uint32_t)-1)
#define SPANK_TIMER_DELTA_MIN     	0
#define SPANK_SYSTICKS_INFINITE   	((uint32_t)-1)
#define SPANK_SYSTICKS_PER_SECOND 	1000
#define SPANK_MS_TO_SYSTICKS(ms)					\
    (((ms) * SPANK_SYSTICKS_PER_SECOND + (SPANK_SYSTICKS_PER_SECOND-1)) / \
     SPANK_SYSTICKS_PER_SECOND )

spank_systime_t spank_systime(void);

void spank_delay(spank_systicks_t ticks);

extern spank_systicks_t _spank_alarm_timeout;

void _spank_set_alarm(spank_systime_t time, spank_systicks_t delta);
void _spank_stop_alarm(void);



/*----------------------------------------------------------------------*/
/* Semaphore / Mutex / Condition varariable                             */
/*----------------------------------------------------------------------*/

typedef pthread_mutex_t spank_mutex_t;
typedef sem_t           spank_semaphore_t;
typedef pthread_cond_t  spank_cv_t;
    
void spank_semaphore_init(spank_semaphore_t *sem, uint16_t count);
void spank_semaphore_destroy(spank_semaphore_t *sem);
bool spank_semaphore_take(spank_semaphore_t *sem, spank_systicks_t timeout);
void spank_semaphore_give(spank_semaphore_t *sem);

void spank_mutex_init(spank_mutex_t *mutex);
void spank_mutex_destroy(spank_mutex_t *mutex);
bool spank_mutex_acquire(spank_mutex_t *mutex, spank_systicks_t timeout);
void spank_mutex_release(spank_mutex_t *mutex);

#define SPANK_CV_NATIVE
void spank_cv_init(spank_cv_t *cv);
bool spank_cv_wait(spank_cv_t *cv, spank_mutex_t *mutex, spank_systicks_t timeout);
void spank_cv_signal(spank_cv_t *cv);
void spank_cv_broadcast(spank_cv_t *cv);


/*----------------------------------------------------------------------*/
/* Output locking                                                       */
/*----------------------------------------------------------------------*/
void spank_output_lock(void);
void spank_output_unlock(void);


#endif
