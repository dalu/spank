/*
 * Copyright (c) 2024
 * Stephane D'Alu, Inria Chroma team, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __DW1000_OSAL_H__
#define __DW1000_OSAL_H__

#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>
#include <errno.h>
#include <sys/uio.h>
#include <signal.h>

#include "dw1000/dw1000_reg.h"
#include "spank/osal.h"
#include "rsvc.h"


typedef struct dw1000 dw1000_t;



struct dw1000_emulation;

struct dw1000_emulation *
dw1000_emulation_create(rsvc_t *rsvc, void (*line_cb)(int line, void *args), void *line_args);
void dw1000_emulation_reset(struct dw1000_emulation *e);


/*----------------------------------------------------------------------*/
/* Config                                                               */
/*----------------------------------------------------------------------*/

#if  defined(DW1000_SFD_TIMEOUT_DEFAULT) &&	\
    !defined(DW1000_WITH_SFD_TIMEOUT_DEFAULT)
#if DW1000_SFD_TIMEOUT_DEFAULT > 0
#define DW1000_WITH_SFD_TIMEOUT_DEFAULT 1
#else
#define DW1000_WITH_SFD_TIMEOUT_DEFAULT 0
#endif
#endif


/*----------------------------------------------------------------------*/
/* Debug / Assert                                                       */
/*----------------------------------------------------------------------*/

#define DW1000_ASSERT(x, reason) assert(x)


/*----------------------------------------------------------------------*/
/* Time                                                                 */
/*----------------------------------------------------------------------*/

static inline  void
_dw1000_delay_usec(uint16_t us) {
    sigset_t mask, oldmask;
    sigfillset(&mask);
    pthread_sigmask(SIG_SETMASK, &mask, &oldmask);
    usleep(us);
    pthread_sigmask(SIG_SETMASK, &oldmask, NULL);
}

static inline void
_dw1000_delay_msec(uint16_t ms) {
    sigset_t mask, oldmask;
    sigfillset(&mask);
    pthread_sigmask(SIG_SETMASK, &mask, &oldmask);
    usleep(ms * 1000);
    pthread_sigmask(SIG_SETMASK, &oldmask, NULL);
}


/*----------------------------------------------------------------------*/
/* IO line                                                              */
/*----------------------------------------------------------------------*/

typedef struct dw1000_ioline {
    struct dw1000_emulation *emulation;
    int line;
    struct {
	void (*cb)(void *data);
	void *data;
    } irq;
}  *dw1000_ioline_t;


#define DW1000_IOLINE_NONE	0
#define DW1000_IOLINE_IRQ	1
#define DW1000_IOLINE_RESET	2
#define DW1000_IOLINE_WAKEUP	3


void _dw1000_ioline_set(dw1000_ioline_t line);
void _dw1000_ioline_clear(dw1000_ioline_t line);



/*----------------------------------------------------------------------*/
/* SPI                                                                  */
/*----------------------------------------------------------------------*/

typedef struct dw1000_spi_driver {
    struct dw1000_emulation *emulation;
} dw1000_spi_driver_t;

void _dw1000_spi_send(dw1000_spi_driver_t *spi,
              uint8_t *hdr, size_t hdrlen, uint8_t *data, size_t datalen);
void _dw1000_spi_recv(dw1000_spi_driver_t *spi,
              uint8_t *hdr, size_t hdrlen, uint8_t *data, size_t datalen);

void _dw1000_spi_low_speed(dw1000_spi_driver_t *spi);

void _dw1000_spi_high_speed(dw1000_spi_driver_t *spi);

#endif
