
/*
 * See: https://stackoverflow.com/questions/7478684/how-to-initialise-a-binary-semaphore-in-c 
 */

#include <errno.h>
#include <assert.h>

#include "binsem.h"

int
binsem_init(binsem_t *sem, int value)
{
    if ((value != 0) && (value !=1 )) {
	errno = EINVAL;
	return -1;
    }
    
    if (pthread_mutex_init(&sem->mutex, NULL) < 0) {
	return -1;
    }
    if (pthread_cond_init(&sem->cond,   NULL) < 0) {
	int rc = pthread_mutex_destroy(&sem->mutex);
	assert(rc == 0);
	return -1;
    }
    sem->v = value;

    return 0;
}

int
binsem_destroy(binsem_t *sem)
{
    if (pthread_cond_destroy(&sem->cond) < 0) {
	return -1;
    }
    if (pthread_mutex_destroy(&sem->mutex) < 0) {
	assert(0);
    }
    return 0;
}

int
binsem_post(binsem_t *sem)
{
    int rc = 0;

    if (pthread_mutex_lock(&sem->mutex) < 0) {
	return -1;
    }

    if (sem->v == 1) {
	rc = pthread_mutex_unlock(&sem->mutex);
	assert(rc == 0);
	errno = EOVERFLOW;
	return -1;
    }
    
    sem->v++;
    rc = pthread_cond_signal(&sem->cond);
    assert(rc == 0);
    rc = pthread_mutex_unlock(&sem->mutex);
    assert(rc == 0);
    return 0;
}

int
binsem_wait(binsem_t *sem)
{
    if (pthread_mutex_lock(&sem->mutex) < 0) {
	return -1;
    }
    while (!sem->v) {
        int rc = pthread_cond_wait(&sem->cond, &sem->mutex);
	assert(rc == 0);
    }
    sem->v--;
    int rc = pthread_mutex_unlock(&sem->mutex);
    assert(rc == 0);

    return 0;
}

int
binsem_trywait(binsem_t *sem)
{
    int status = 0;
    
    if (pthread_mutex_lock(&sem->mutex) < 0) {
	return -1;
    }

    if (sem->v > 0) {
	sem->v--;
    } else {
	status = EAGAIN;
    }

    int rc = pthread_mutex_unlock(&sem->mutex);
    assert(rc == 0);

    if (status) { errno = status; return -1; }
    else        {                 return  0; }
}
