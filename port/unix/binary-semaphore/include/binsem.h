#ifndef __BINSEM_H__
#define __BINSEM_H__

#include <pthread.h>

/**
 * Binary semaphore implementation
 */
typedef struct binsem {
    pthread_mutex_t mutex;
    pthread_cond_t  cond;
    int v;
} binsem_t;


/**
 * Initialize a binary semaphore
 * 
 * Errno values:
 * ~~~
 * Value  | Meaning
 * ------ | ----------------
 * EINVAL | Invalid argument
 * ENOMEM | Not enough memory
 * EAGAIN | Temporarily lacks the resources
 * ~~~
 *
 * @param sem   semaphore
 * @param value initial semaphore value (0 or 1)
 *
 * @return -1 on error
 */
int binsem_init(binsem_t *sem, int value);

/**
 * Destroy a binary semaphore
 *
 * Errno values:
 * ~~~
 * Value  | Meaning
 * ------ | ----------------
 * EINVAL | Invalid argument
 * EBUSY  | Is locked by another thread
 * ~~~
 *
 * @param sem   semaphore
 *
 * @return -1 on error
 */ 
int binsem_destroy(binsem_t *sem);

/**
 * Post to a binary semaphore
 *
 * Errno values:
 * ~~~
 * Value           | Meaning
 * --------------- | ----------------
 * EOVERFLOW       | The semaphore value would exceed 1
 * EINVAL          | Invalid argument
 * EDEADLK         | 
 * EOWNERDEAD      | 
 * ENOTRECOVERABLE |
 * ~~~
 *
 * @param sem   semaphore
 *
 * @return -1 on error
 */
int binsem_post(binsem_t *sem);

/**
 * Wait on a binary semaphore
 *
 * Errno values:
 * ~~~
 * Value           | Meaning
 * --------------- | ----------------
 * EINVAL          | Invalid argument
 * EPERM           | 
 * EDEADLK         | 
 * EOWNERDEAD      | 
 * ENOTRECOVERABLE |
 * ~~~
 *
 * @param sem   semaphore
 *
 * @return -1 on error
 */
int binsem_wait(binsem_t *sem);


/**
 * Try waiting on a binary semaphore
 *
 * Errno values:
 * ~~~
 * Value           | Meaning
 * --------------- | ----------------
 * EINVAL          | Invalid argument
 * EAGAIN          | The semaphore would block
 * EPERM           | 
 * EDEADLK         | 
 * EOWNERDEAD      | 
 * ENOTRECOVERABLE |
 * ~~~
 *
 * @param sem   semaphore
 *
 * @return -1 on error
 */
int binsem_trywait(binsem_t *sem);

#endif 
