#ifndef __SPANK_DRIVER_H__
#define __SPANK_DRIVER_H__

#include <sys/types.h>

#include "sys/_iovec.h"

#include <dw1000/dw1000_dev.h>
#include <dw1000/dw1000_hal.h>
#include <dw1000/dw1000_phy.h>
#include <dw1000/dw1000_mac.h>
#include <dw1000/dw1000_ftypes.h>

#define SPANK_DRIVER_TX_TIMEOUT SPANK_MS_TO_SYSTICKS(3)

#define SPANK_DRIVER_TIME_BITS    40
#define SPANK_DRIVER_TS_FREQ     (499.2e6 * 128)
#define SPANK_DRIVER_FRAME_MAXSIZE          127

#define DW_TX_IMMEDIATE           0x00
#define DW_TX_DELAYED_START       0x01
#define DW_TX_RESPONSE_EXPECTED   0x02
#define DW_TX_RANGING             0x04
#define DW_TX_NO_AUTO_CRC         0x08



typedef dw1000_dev_instance_t spank_driver_t;


extern uint64_t _spank_driver_antennaDelay;

static inline
void dw_set_antenna_delay(uint64_t delay) {
    _spank_driver_antennaDelay = delay;
}


static inline
size_t spank_driver_get_data_length(spank_driver_t *dev) {
    return dev->frame_len;
}


void spank_driver_rx_read_frame_data(spank_driver_t *dev,
			   uint8_t *data, size_t length, size_t offset);


ssize_t dw_rx_get_frame_802154(spank_driver_t *dev, void *data, size_t datalen);





static inline
void spank_driver_rx_start(spank_driver_t *dev) {
    dw1000_start_rx(dev);
}

static inline
uint64_t spank_driver_tx_get_timestamp(spank_driver_t *dev) {
    return dw1000_read_txtime(dev);
}

static inline
uint64_t spank_driver_rx_get_timestamp(spank_driver_t *dev) {
    return dw1000_read_rxtime(dev);
}


int spank_driver_tx_send(spank_driver_t *dev,
			 void *data, size_t datalen, uint8_t tx_mode);



int spank_driver_tx_sendv(spank_driver_t *dev,
			  struct iovec *iovec, int iovcnt, uint8_t tx_mode);


bool spank_driver_init(spank_driver_t *drv,
		       void (*tx_complete_cb)(spank_driver_t *),
		       void (*rx_complete_cb)(spank_driver_t *),
		       void (*rx_timeout_cb )(spank_driver_t *),
		       void (*rx_error_cb   )(spank_driver_t *));


#endif
