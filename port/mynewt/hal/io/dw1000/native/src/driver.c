#include <sys/types.h>
#include <string.h>

#include "sys/_iovec.h"

#include <dw1000/dw1000_dev.h>
#include <dw1000/dw1000_hal.h>
#include <dw1000/dw1000_phy.h>
#include <dw1000/dw1000_mac.h>
#include <dw1000/dw1000_ftypes.h>

#define SPANK_SYSLOG_PREFIX "DRIVER"
#include "spank/osal.h"
#include "spank/syslog.h"
#include "spank/driver.h"


static dwt_config_t mac_config = {
    .chan           = 5,            // Channel number. 
    .prf            = DWT_PRF_64M,  // Pulse repetition frequency. 
    .txPreambLength = DWT_PLEN_256, // Preamble length. Used in TX only. 
    .rxPAC          = DWT_PAC8,     // Preamble acquisition chunk size. Used in RX only. 
    .txCode         = 8,            // TX preamble code. Used in TX only. 
    .rxCode         = 8,            // RX preamble code. Used in RX only. 
    .nsSFD          = 0,            // 0 to use standard SFD, 1 to use non-standard SFD. 
    .dataRate       = DWT_BR_6M8,         // Data rate. 
    .phrMode        = DWT_PHRMODE_STD,         // PHY header mode. 
    .sfdTO          = (256 + 1 + 8 - 8)         // SFD timeout (preamble length + 1 + SFD length - PAC size). Used in RX only. 
};

static dw1000_phy_txrf_config_t txrf_config = { 
    .PGdly = TC_PGDELAY_CH5,
    //.power = 0x25456585
    .BOOSTNORM = dw1000_power_value(DW1000_txrf_config_9db, 5),
    .BOOSTP500 = dw1000_power_value(DW1000_txrf_config_9db, 5),
    .BOOSTP250 = dw1000_power_value(DW1000_txrf_config_9db, 5),
    .BOOSTP125 = dw1000_power_value(DW1000_txrf_config_9db, 5)
};




bool
spank_driver_init(spank_driver_t *drv,
		  void (*tx_complete_cb)(spank_driver_t *),
		  void (*rx_complete_cb)(spank_driver_t *),
		  void (*rx_timeout_cb )(spank_driver_t *),
		  void (*rx_error_cb   )(spank_driver_t *))
{
    dw1000_phy_init(drv, &txrf_config); 

    drv->PANID            = 0xDECA;
    drv->my_short_address = 0x1234;


    dw1000_set_panid(drv, drv->PANID);
    dw1000_mac_init(drv, &mac_config);

    dw1000_set_callbacks(drv, tx_complete_cb,
			      rx_complete_cb, rx_timeout_cb, rx_error_cb);

	
    dw1000_set_rx_timeout(drv, 0);

    return true;
}




int spank_driver_tx_send(spank_driver_t *dev, void *data, size_t datalen, uint8_t tx_mode) {
    dw1000_phy_forcetrxoff(dev);
    dw1000_write_tx(dev, data, 0, datalen);
    dw1000_write_tx_fctrl(dev, datalen, 0, true);
    dw1000_set_wait4resp(dev, tx_mode & DW_TX_RESPONSE_EXPECTED);
    //dw1000_set_delay_start(dev, response_tx_delay);   
    dw1000_start_tx(dev);
    return 0;
}


int
spank_driver_tx_sendv(spank_driver_t *dev,
		      struct iovec *iovec, int iovcnt, uint8_t tx_mode) {
    
    static uint8_t data[SPANK_DRIVER_FRAME_MAXSIZE];

    size_t datalen = 0;
    for (int i = 0 ; i < iovcnt ; i++) {
	datalen += iovec[i].iov_len;
    }

    uint32_t offset = 0;
    for (int i = 0 ; i < iovcnt ; i++) {
	memcpy(&data[offset], iovec[i].iov_base, iovec[i].iov_len);
	offset += iovec[i].iov_len;
    }

    /*
    if (size > LEN_EXT_UWB_FRAMES) {
	return; // TODO proper error handling: frame/buffer size
    }
    if (size > LEN_UWB_FRAMES && !dev->extendedFrameLength) {
	return; // TODO proper error handling: frame/buffer size
    }
    */

    return spank_driver_tx_send(dev, data, datalen, tx_mode);
}



void spank_driver_rx_read_frame_data(spank_driver_t *dev,
			   uint8_t *data, size_t length, size_t offset) {
    // Protect device from overreading the buffer
    if (offset > 1024)
	return;
    if ((offset + length) > 1024)
	length = 1024 - offset;
    
    // Read data
    dw1000_read_rx(dev, data, offset, length);
}





// Local Variables:
// mode: c
// c-basic-offset: 4
// End:
