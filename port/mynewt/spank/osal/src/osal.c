#include "spank/osal.h"
#include "spank/syslog.h"

/*
 * Alarm
 */
spank_systicks_t _spank_alarm_timeout = SPANK_TIMER_MAX_SYSTICKS;

void _spank_set_alarm(spank_systime_t time, spank_systicks_t delta) {
    _spank_alarm_timeout = delta;
}

void _spank_stop_alarm(void) {
    _spank_alarm_timeout = SPANK_TIMER_MAX_SYSTICKS;
}



/*
 * Syslog
 */
#if MYNEWT_VAL(SPANK_SYSLOG_RUNTIME)

static int spank_syslog_level = SPANK_SYSLOG_LEVEL;

void spank_syslog_setlevel(int lvl) {
    spank_syslog_level = lvl;
}

int spank_syslog_getlevel(void) {
    return spank_syslog_level;
}

void spank_syslog(int lvl, const char *prefix, const char *fmt, ...) {
    if (lvl > spank_syslog_level) 
	return;

    const char *lvlstr = spank_syslog_string(lvl);
    if (lvlstr == NULL)
	lvlstr = "?";
    
    printf("[%c] ", lvlstr[0]);
    if (prefix != NULL) 
    	printf("%s: ", prefix);

    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    putchar('\n');
}

#endif
