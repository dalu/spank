#ifndef __SPANK_OSAL_H__
#define __SPANK_OSAL_H__

#include <assert.h>
#include <syscfg/syscfg.h>
#include <os/os.h>
#include <console/console.h>


/*----------------------------------------------------------------------*/
/* Min/Max                                                              */
/*----------------------------------------------------------------------*/

#define SPANK_MIN(a,b)				     \
    ({						     \
	__typeof__ (a) _spank_a = (a);		     \
	__typeof__ (b) _spank_b = (b);		     \
	_spank_a < _spank_b ? _spank_a : _spank_b;   \
    })

#define SPANK_MAX(a,b)				     \
    ({						     \
	__typeof__ (a) _spank_a = (a);		     \
	__typeof__ (b) _spank_b = (b);		     \
	_spank_a > _spank_b ? _spank_a : _spank_b;   \
    })



/*----------------------------------------------------------------------*/
/* Ease crazyflie debug                                                 */
/*----------------------------------------------------------------------*/

#define DEBUG_PRINT(x...)



/*----------------------------------------------------------------------*/
/* Types                                                                */
/*----------------------------------------------------------------------*/

typedef float  float32_t;
typedef double float64_t;



/*----------------------------------------------------------------------*/
/* Debug / Assert / Log / Report                                        */
/*----------------------------------------------------------------------*/

#define SPANK_ASSERT(x) assert(x)

#if defined(SPANK_NO_SYSLOG)
#  define SPANK_SYSLOG(lvl, fmt, ...)
#elif defined(SPANK_SYSLOG_PREFIX)
#  if MYNEWT_VAL(SPANK_SYSLOG_RUNTIME) > 0
#    define SPANK_SYSLOG(lvl, fmt, ...)					\
    spank_syslog(lvl, SPANK_SYSLOG_PREFIX, fmt, ##__VA_ARGS__)
#  else
#    define SPANK_SYSLOG(lvl, fmt, ...)					\
    console_printf("[%c] " SPANK_SYSLOG_PREFIX ": " fmt "\n",		\
		   spank_syslog_string(lvl)[0], ##__VA_ARGS__)
#  endif
#else
#  if MYNEWT_VAL(SPANK_SYSLOG_RUNTIME) > 0
#    define SPANK_SYSLOG(lvl, fmt, ...)					\
    spank_syslog(lvl, NULL, fmt, ##__VA_ARGS__)
#  else
#    define SPANK_SYSLOG(lvl, fmt, ...)					\
    console_printf("[%c] " fmt "\n", spank_syslog_string(lvl)[0], ##__VA_ARGS__)
#  endif
#endif

#if MYNEWT_VAL(SPANK_SYSLOG_RUNTIME) > 0
#define SPANK_SYSLOG_RUNTIME 1
void spank_syslog(int lvl, const char *prefix, const char *fmt, ...)
    __attribute__ ((format (printf, 3, 4)));
void spank_syslog_setlevel(int lvl);
int spank_syslog_getlevel(void);
#endif

#define SPANK_SYSLOG_LEVEL MYNEWT_VAL_SPANK_SYSLOG_LEVEL

#define SPANK_REPORT(type, args)



/*----------------------------------------------------------------------*/
/* Timer / Alarm                                                        */
/*----------------------------------------------------------------------*/

typedef os_time_t spank_systime_t;
typedef uint32_t  spank_systicks_t;

#define SPANK_TIMER_MAX_SYSTICKS	OS_TIME_MAX
#define SPANK_TIMER_DELTA_MIN    	0
#define SPANK_SYSTICKS_INFINITE  	OS_WAIT_FOREVER
#define SPANK_SYSTICKS_PER_SECOND 	OS_TICKS_PER_SEC
#define SPANK_MS_TO_SYSTICKS(ms)        (((ms) * OS_TICKS_PER_SEC + 999) / 1000)

static inline spank_systime_t
spank_systime(void) {
    return os_time_get();
}

static inline void
spank_delay(spank_systicks_t ticks) {
    os_time_delay(ticks);
}


extern spank_systicks_t _spank_alarm_timeout;

void _spank_set_alarm(spank_systime_t time, spank_systicks_t delta);
void _spank_stop_alarm(void);



/*----------------------------------------------------------------------*/
/* Semaphore / Mutex                                                    */
/*----------------------------------------------------------------------*/

typedef struct os_mutex spank_mutex_t;
typedef struct os_sem   spank_semaphore_t;

static inline void
spank_semaphore_init(spank_semaphore_t *sem, uint16_t count) {
    os_error_t rc = os_sem_init(sem, count);
    assert(rc == OS_OK);
};

static inline bool
spank_semaphore_take(spank_semaphore_t *sem, spank_systicks_t timeout) {
    os_error_t rc = os_sem_pend(sem, timeout);
    assert((rc == OS_OK) || (rc == OS_TIMEOUT));
    return rc == OS_OK;
}

static inline void
spank_semaphore_give(spank_semaphore_t *sem) {
    os_error_t rc = os_sem_release(sem);
    assert(rc == OS_OK);
}

static inline void
spank_mutex_init(spank_mutex_t *mutex) {
    os_error_t rc = os_mutex_init(mutex);
    assert(rc == OS_OK);
};

static inline bool
spank_mutex_acquire(spank_mutex_t *mutex, spank_systicks_t timeout) {
    os_error_t rc = os_mutex_pend(mutex, timeout);
    assert((rc == OS_OK) || (rc == OS_TIMEOUT));
    return rc == OS_OK;
}

static inline void
spank_mutex_release(spank_mutex_t *mutex) {
    os_error_t rc = os_mutex_release(mutex);
    assert(rc == OS_OK);
}

#endif

