/*
 * Copyright (c) 2018-2024
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#ifndef __SPANK_DRIVER_H__
#define __SPANK_DRIVER_H__

#include <sys/types.h>
#include <stdlib.h>
#include <stdint.h>

#include <dw1000/osal.h>
#include <dw1000/dw1000.h>
#include <dw1000/dw1000_send.h>
#include <spank/types.h>

#define SPANK_DRIVER_TIME_BITS              DW1000_TIME_CLOCK_BITS
#define SPANK_DRIVER_TIME_CLOCK             DW1000_TIME_CLOCK_HZ
#define SPANK_DRIVER_FRAME_MAXSIZE          127
#define SPANK_DRIVER_FRAME_CRC_SIZE	     2
#define SPANK_DRIVER_TIME_MASK (((uint64_t)1 << SPANK_DRIVER_TIME_BITS) - 1)

#if SPANK_DRIVER_FRAME_MAXSIZE > DW1000_FRAME_MAXSIZE
#error SPANK_DRIVER_FRAME_MAXSIZE is bigger than DW1000 supported frame size
#endif


/*======================================================================*/

#define SPANK_DRIVER_TX_IMMEDIATE           DW1000_TX_IMMEDIATE
#define SPANK_DRIVER_TX_DELAYED_START       DW1000_TX_DELAYED_START
#define SPANK_DRIVER_TX_RESPONSE_EXPECTED   DW1000_TX_RESPONSE_EXPECTED
#define SPANK_DRIVER_TX_RANGING             DW1000_TX_RANGING
#define SPANK_DRIVER_TX_NO_AUTO_CRC         DW1000_TX_NO_AUTO_CRC

#if DW1000_WITH_EXTENDED_SEND

#define SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP 			\
    DW1000_TX_DELAYED_EMBED_TIMESTAMP

#if defined(DW1000_TX_DELAYED_EMBED_TIMESTAMP_BIG_ENDIAN)
#define SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_BIG_ENDIAN		\
    DW1000_TX_DELAYED_EMBED_TIMESTAMP_BIG_ENDIAN
#endif
#if defined(DW1000_TX_DELAYED_EMBED_TIMESTAMP_LITTLE_ENDIAN)
#define SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_LITTLE_ENDIAN 		\
   DW1000_TX_DELAYED_EMBED_TIMESTAMP_LITTLE_ENDIAN
#endif
#if defined(DW1000_TX_DELAYED_EMBED_TIMESTAMP_40BIT)
#define SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_40BIT			\
   DW1000_TX_DELAYED_EMBED_TIMESTAMP_40BIT
#endif
#if defined(DW1000_TX_DELAYED_EMBED_TIMESTAMP_64BIT)
#define SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_64BIT			\
    DW1000_TX_DELAYED_EMBED_TIMESTAMP_64BIT
#endif

#define SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_DEFAULT			\
    SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_BIG_ENDIAN |		\
    SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_40BIT      

#define SPANK_DRIVER_TX_DELAYED_DELAY 					\
   DW1000_TX_DELAYED_DELAY
#define SPANK_DRIVER_TX_DELAYED_RETRY_DELAY 				\
    DW1000_TX_DELAYED_RETRY_DELAY

#define SPANK_DRIVER_TX_DELAYED_DEFAULT_DELAY				\
   DW1000_TX_DELAYED_DEFAULT_DELAY
#define SPANK_DRIVER_TX_DELAYED_DEFAULT_RETRY_DELAY 			\
    DW1000_TX_DELAYED_DEFAULT_RETRY_DELAY

#endif

typedef struct spank_driver {
    dw1000_t dw;

    struct {
	struct {
	    //
	    spank_mutex_t     mutex;
	    spank_semaphore_t sem;
	    struct iovec      iovec[2];
	    int               tx_mode;
	    size_t            ts_offset;
	    int               rc;
	} tx;
    } processing;
    
} spank_driver_t;

// To implement, OS specific.
bool spank_driver_post_tx(spank_driver_t *dev);


#define SPANK_DRIVER_TX_ANTENNA_DELAY(dev)	\
    ((dev)->dw.config->tx_antenna_delay)

#define SPANK_DRIVER_TX_POWER(dev)		\
    ((dev)->dw.tx_power)

bool spank_driver_init(spank_driver_t *drv, const dw1000_config_t *cfg);

// Estimate system dependant delay for delayed send
// !! should be used before interrupt processing is setup
// !! as we just perform low level send to test return code
uint32_t spank_driver_delayed_send_estimator(spank_driver_t *drv, size_t size);



/*===========================================================================*/
/* System                                                                    */
/*===========================================================================*/

static inline uint64_t
spank_driver_get_system_time(spank_driver_t *dev) {
    return dw1000_get_system_time(&dev->dw);
}

static inline void
spank_driver_read_temp_vbat(spank_driver_t *dev,
			    uint16_t *temp, uint16_t *vbat) {
    dw1000_read_temp_vbat(&dev->dw, temp, vbat);
}



/*===========================================================================*/
/* Operations common to TX/RX                                                */
/*===========================================================================*/

static inline void
spank_driver_txrx_set_time(spank_driver_t *dev, uint64_t time) {
    dw1000_txrx_set_time(&dev->dw, time);
}



/*===========================================================================*/
/* Transmission (TX)                                                         */
/*===========================================================================*/

static inline int
spank_driver_tx_send(spank_driver_t *dev,
		     void *data, size_t datalen, int tx_mode) {
    dw1000_txrx_off(&dev->dw);
    return dw1000_tx_send(&dev->dw, data, datalen, tx_mode);
}

static inline int
spank_driver_tx_sendv(spank_driver_t *dev,
		      struct iovec *iovec, int iovcnt, int tx_mode) {
    dw1000_txrx_off(&dev->dw);
    return dw1000_tx_sendv(&dev->dw, iovec, iovcnt, tx_mode);
}

#if DW1000_WITH_EXTENDED_SEND
static inline int
spank_driver_tx_extended_sendv(spank_driver_t *dev,
	struct iovec *iovec, int iovcnt, int tx_mode, ...) {
    dw1000_txrx_off(&dev->dw);
    va_list ap;
    va_start(ap, tx_mode);
    int rc = dw1000_tx_extended_vsendv(&dev->dw, iovec, iovcnt, tx_mode, ap);
    va_end(ap);
    return rc;
}
#endif

static inline bool
spank_driver_tx_is_expecting_response(spank_driver_t *dev) {
    return dw1000_tx_is_expecting_response(&dev->dw);
}

static inline uint64_t
spank_driver_tx_get_timestamp(spank_driver_t *dev) {
    return dw1000_tx_get_rmarker_time(&dev->dw);
}



/*===========================================================================*/
/* Reception (RX)                                                            */
/*===========================================================================*/

static inline void
spank_driver_rx_start(spank_driver_t *dev) {
    dw1000_rx_start(&dev->dw, 0);
}

static inline size_t
spank_driver_get_data_length(spank_driver_t *dev) {
    return dw1000_rx_get_frame_length(&dev->dw);
}

static inline void
spank_driver_rx_read_frame_data(spank_driver_t *dev,
			   uint8_t *data, size_t length, size_t offset) {
    dw1000_rx_read_frame_data(&dev->dw, data, length, offset);
}

static inline void
spank_driver_rx_get_power_estimate(spank_driver_t *dev,
				   spank_rssi_t *signal,
				   spank_rssi_t *firstpath) {
    double _signal, _firstpath;
    dw1000_rx_get_power_estimate(&dev->dw,
				 signal    ? &_signal    : NULL,
				 firstpath ? &_firstpath : NULL);

    if (signal   ) *signal    = _signal    * 100;
    if (firstpath) *firstpath = _firstpath * 100;
}

static inline spank_rssi_t
spank_driver_rx_power_correction(spank_driver_t *dev, spank_rssi_t power) {
    return dw1000_rx_power_correction(&dev->dw, power / 100.0) * 100;
}

static inline uint64_t
spank_driver_rx_get_timestamp(spank_driver_t *dev) {
    return dw1000_rx_get_rmarker_time(&dev->dw);
}

static inline void
spank_driver_rx_get_clock_tracking(spank_driver_t *dev,
				   int32_t *offset, uint32_t *interval) {
    dw1000_rx_get_time_tracking(&dev->dw, offset, interval);
}


#endif
