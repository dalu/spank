/*
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 */

#include <dw1000/dw1000.h>

#define SPANK_SYSLOG_PREFIX "DRIVER"
#include "spank/osal.h"
#include "spank/syslog.h"
#include "spank/driver.h"

/* UM §9.3: air utilisation must be < 18% to have 97% transmission
 * 
 * UM §9.5: node density and air utilisation:
 * Data rate | Preamble length | Payload | Transmission | TX per second 
 *           |   (symbols)     | (bytes) |  time        | 18% air-utilisation
 * ----------+-----------------+---------+-------------+---------------------
 *  110 kbps |            2048 |      12 | 3.042 ms     | 59.2
 *  850 kbps |             256 |      12 | 380.3 μs     | 473.4
 *  6.8 Mbps |              64 |      12 | 103.3 μs     | 1742
 */


/* Radio configuration of the DW1000 
 *
 * UM §9.3  : Data rate, preamble length, PRF
 * UM §4.1.1: Preamble detection
 * UM §10.5 : UWB channels and preamble codes
 *
 * Recommanded preamble length for the following bitrates:
 *  6800 kbps :   64 or  128 or  256
 *   850 kbps :  256 or  512 or 1024
 *   110 kbps : 2048 or 4096
 *
 * PLEN (Preamble length) / PAC (Preamble Acquisition Chunk)
 *   tx_plen: 64 | 128 | 256 | 512 | 1024 | 1536 | 2048 | 4096
 *   rx_pac : 8  | 8   | 16  | 16  | 32   | 64   | 64   | 64
 *
 * Recommanded preamble codes according to selected channel and PRF:
 *  Channel | Preamble codes | Preamble codes 
 *          | for 16MHz PRF  | for 64 MHz PRF
 * ---------+----------------+----------------
 *     1    |     1, 2       |  9, 10, 11, 12
 *     2    |     3, 4       |  9, 10, 11, 12
 *     3    |     5, 6       |  9, 10, 11, 12
 *     4    |     7, 8       | 17, 18, 19, 20
 *     5    |     3, 4       |  9, 10, 11, 12
 *     7    |     7, 8       | 17, 18, 19, 20
 */
static struct dw1000_radio spank_driver_dw1000_radio = {
    .channel          = 5, // Possible to use 2 or 5 with the same parameters
    .bitrate          = DW1000_BITRATE_6800KBPS,
#if defined(SPANK_DRIVER_UWB_TX_POWER)
    .tx_power         = DW1000_TX_POWER_05DB(SPANK_DRIVER_UWB_TX_POWER),
#endif
    .prf              = DW1000_PRF_64MHZ,
    .tx_plen          = DW1000_PLEN_128, // UM §9.3
    .rx_pac           = DW1000_PAC8,     // UM §4.1
    .tx_pcode         = 10,              // UM §10.5
    .rx_pcode         = 10,              // UM §10.5
#if DW1000_WITH_PROPRIETARY_SFD || DW1000_WITH_PROPRIETARY_LONG_FRAME
    .proprietary      = {
#if DW1000_WITH_PROPRIETARY_LONG_FRAME
	.long_frames  = 0,
#endif
#if DW1000_WITH_PROPRIETARY_SFD
	.sfd          = 1,
#endif
    },
#endif
#if DW1000_WITH_SFD_TIMEOUT
    .sfd_timeout      = DW1000_SFD_TIMEOUT_MAX
#endif
};


bool
spank_driver_init(spank_driver_t *drv, const dw1000_config_t *cfg)
{
    dw1000_init(&drv->dw, cfg);                             // DW creation
    dw1000_hardreset(&drv->dw);                             // Reset the chip
    if (dw1000_initialise(&drv->dw) < 0)                    // Initialise device
	return false;
    dw1000_leds_blink(&drv->dw, DW1000_LED_ALL);            // Blinking all leds
    dw1000_configure(&drv->dw, &spank_driver_dw1000_radio); // Configure radio
    dw1000_rx_set_timeout(&drv->dw, 0);                     // No RX timeout

    spank_mutex_init(&drv->processing.tx.mutex);
    spank_semaphore_init(&drv->processing.tx.sem, 0);
    return true;
}


uint32_t
spank_driver_delayed_send_estimator(spank_driver_t *drv, size_t size) {
    // TX mode
    int tx_mode = SPANK_DRIVER_TX_DELAYED_START                   |
	          SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP         |
	          SPANK_DRIVER_TX_DELAYED_EMBED_TIMESTAMP_DEFAULT |
	          SPANK_DRIVER_TX_DELAYED_DELAY                   |
	          SPANK_DRIVER_TX_DELAYED_RETRY_DELAY             ;
    
    // If CRC is automatically computed, we need to reduce by 2 bytes
    if (! (tx_mode & SPANK_DRIVER_TX_NO_AUTO_CRC))
	size -= 2;

    // Dummy payload
    //  - don't even bother to be 802.15.4 compatible
    //  - don't bother to sanitize data
    uint8_t data[size];
    struct iovec iovec[] = {
	{ .iov_base = data, .iov_len = size }
    };

    // Timestamp offset
    //  Don't mind if testing size is to small to fit timestamp data,
    //  it will fit anyway the dw1000 internal buffer, and will be truncated
    size_t ts_offset = 0;

    // Lookup for the optimal delay for up to 10ms (which is bad enough)
    uint32_t up  = SPANK_MIN(SPANK_DRIVER_TIME_CLOCK / 100, UINT32_MAX - 1);
    uint32_t max = up; 
    uint32_t min = 1;
    uint32_t mid;
    
    do {
	mid = min + (max - min) / 2;
	int rc = spank_driver_tx_extended_sendv(drv, iovec, 1,
						tx_mode, ts_offset, mid, 0);
        if (rc == 0) { max = mid; }
	else         { min = mid; }
    } while ((max - min) > 1);

    // Return driver to an IDLE state
    dw1000_txrx_off(&drv->dw);

    // Return 0 if reached the upper limit
    return (mid >= (up-1)) ? 0 : mid;
}

// Local Variables:
// mode: c
// c-basic-offset: 4
// End:
