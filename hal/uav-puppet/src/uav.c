/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: 
 */

#include <sys/queue.h>
#include <string.h>

#define SPANK_SYSLOG_PREFIX "UAV   "

#include <spank/osal.h>
#include <spank/syslog.h>
#include <spank/uav.h>
#include <spank/uav-puppet.h>


/**
 * Maximum queue size for stacking distance measurements
 */
#define SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_MAXSIZE 65535

/*======================================================================*/
/* Config                                                               */
/*======================================================================*/


#if !defined(SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_SIZE)
#  define SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_SIZE	10
#endif

#if SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_SIZE > SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_MAXSIZE
#error SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_SIZE is greater than SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_MAXSIZE
#endif


/*======================================================================*/
/* Local types                                                          */
/*======================================================================*/

struct spank_uav_distance_measurement_entry {
    STAILQ_ENTRY(spank_uav_distance_measurement_entry) entries;
    struct spank_uav_distance_measurement dm;
};




/*======================================================================*/
/* Local variables                                                      */
/*======================================================================*/

/* UAV context
 */
static struct spank_uav_ctx {
    spank_mutex_t   mutex;		// Mutex
    spank_posture_t posture;		// UAV posture

    int drop_behaviour;                 // Drop behaviour when queue is full
    
    struct {				// Callbacks
	spank_uav_callback_t distance;	//  distance measurement
    } cb;

    struct {
	struct spank_uav_distance_measurement_entry entries[
				    SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_SIZE];
	STAILQ_HEAD(, spank_uav_distance_measurement_entry) pending;
	STAILQ_HEAD(, spank_uav_distance_measurement_entry) free;
	uint32_t lost;
	uint8_t  behaviour;
    } distance_measurements;
} spank_uav_ctx = {
    .drop_behaviour                = SPANK_UAV_DROP_BEHAVIOUR_DEFAULT,
    .distance_measurements.pending =
	STAILQ_HEAD_INITIALIZER(spank_uav_ctx.distance_measurements.pending),
    .distance_measurements.free    =
        STAILQ_HEAD_INITIALIZER(spank_uav_ctx.distance_measurements.free),
};




/*======================================================================*/
/* Exported functions                                                   */
/*======================================================================*/

/* Get info about internal management
 */
void
spank_uav_get_metainfo(spank_uav_metainfo_t *info)
{
    struct spank_uav_ctx                        *ctx     = &spank_uav_ctx;
    struct spank_uav_distance_measurement_entry *dme     = NULL;
    uint16_t                                     pending = 0;

    // Count pending entries
    spank_mutex_acquire(&ctx->mutex, SPANK_SYSTICKS_INFINITE);
    STAILQ_FOREACH(dme, &ctx->distance_measurements.pending, entries) {
	pending++;
    }
    spank_mutex_release(&ctx->mutex);

    // Populate info
    info->pending = pending;
    info->lost    = ctx->distance_measurements.lost;
}

/* Set drop behaviour when internal queue is full
 */
void spank_uav_set_drop_behaviour(int behaviour) {
    struct spank_uav_ctx *ctx = &spank_uav_ctx;
    ctx->drop_behaviour = behaviour;
}

/* Get drop behaviour when internal queue is full
 */
int spank_uav_get_drop_behaviour(void) {
    struct spank_uav_ctx *ctx = &spank_uav_ctx;
    return ctx->drop_behaviour;
}


/* Get UAV posture
 */
void
spank_uav_get_posture(spank_posture_t *posture)
{
    struct spank_uav_ctx *ctx = &spank_uav_ctx;

    SPANK_DEBUG("UAV posture.position -> %d cm, %d cm, %d cm",
	posture->position.value.x,
	posture->position.value.y,
	posture->position.value.z);

    spank_mutex_acquire(&ctx->mutex, SPANK_SYSTICKS_INFINITE);
    memcpy(posture, &ctx->posture, sizeof(spank_posture_t));
    spank_mutex_release(&ctx->mutex);
}



/* Set UAV posture
 */
void
spank_uav_set_posture(spank_posture_t *posture)
{
    struct spank_uav_ctx *ctx = &spank_uav_ctx;

    SPANK_DEBUG("UAV posture.position <- %d cm, %d cm, %d cm",
	posture->position.value.x,
	posture->position.value.y,
	posture->position.value.z);

    spank_mutex_acquire(&ctx->mutex, SPANK_SYSTICKS_INFINITE);
    memcpy(&ctx->posture, posture, sizeof(spank_posture_t));
    spank_mutex_release(&ctx->mutex);
}



/* Get distance measurement.
 */
spank_uav_distance_measurement_t *
spank_uav_get_distance_measurement(spank_uav_metainfo_t *info)
{
    struct spank_uav_ctx                        *ctx = &spank_uav_ctx;
    struct spank_uav_distance_measurement_entry *dme = NULL;
    
    spank_mutex_acquire(&ctx->mutex, SPANK_SYSTICKS_INFINITE);
    if ((dme = STAILQ_FIRST(&ctx->distance_measurements.pending)) != NULL) {
	STAILQ_REMOVE_HEAD(&ctx->distance_measurements.pending, entries);
	dme->dm.has_more = !STAILQ_EMPTY(&ctx->distance_measurements.pending);
    }
    if (info) {
	uint16_t                                     pending = 0;
	struct spank_uav_distance_measurement_entry *dme_itr;
	STAILQ_FOREACH(dme_itr, &ctx->distance_measurements.pending, entries) {
	    pending++;
	}
	info->pending = pending;
	info->lost    = ctx->distance_measurements.lost;
    }
    spank_mutex_release(&ctx->mutex);

    return dme ? &dme->dm : NULL;
}



/* Release distance measurement.
 */
void
spank_uav_release_distance_measurement(spank_uav_distance_measurement_t *dm)
{
    if (dm == NULL)
	return;
    
    struct spank_uav_ctx                        *ctx = &spank_uav_ctx;
    struct spank_uav_distance_measurement_entry *dme =
	CONTAINER_OF(dm, struct spank_uav_distance_measurement_entry, dm);
    
    spank_mutex_acquire(&ctx->mutex, SPANK_SYSTICKS_INFINITE);
    STAILQ_INSERT_TAIL(&ctx->distance_measurements.free, dme, entries);
    spank_mutex_release(&ctx->mutex);
}



/* Feedback the UAV with computed distance from another UAV.
 */
void
spank_uav_feed_distance(spank_distance_t dist, spank_stddev_t stddev,
			spank_posture_t *posture, uint64_t id, void *extra)
{
    struct spank_uav_ctx                        *ctx = &spank_uav_ctx;
    struct spank_uav_distance_measurement_entry *dme = NULL;
    spank_uav_callback_t                 distance_cb = NULL;
    
    SPANK_DEBUG("UAV distance = %d", (int)(dist * 100));

    // Acquire mutex
    spank_mutex_acquire(&ctx->mutex, SPANK_SYSTICKS_INFINITE);

    // Take a distance_measurement item from the free list
    if ((dme = STAILQ_FIRST(&ctx->distance_measurements.free)) != NULL) {
	STAILQ_REMOVE_HEAD(&ctx->distance_measurements.free, entries);

    // or kill one from the pending list and increment lost counter
    } else {
	ctx->distance_measurements.lost++;

	if ((dme = STAILQ_FIRST(&ctx->distance_measurements.pending)) != NULL) {
	    STAILQ_REMOVE_HEAD(&ctx->distance_measurements.pending, entries);
	} else {
	    SPANK_WARNING("free and pending list are empty");
	    goto done;
	}
    }

    // Fill data structure
    ///  (play it safe with alignment constraints, use memcpy)
    dme->dm.distance = dist;
    dme->dm.stddev   = stddev;
    memcpy(&dme->dm.posture, posture, sizeof(*posture));
    dme->dm.id       = id;
    dme->dm.time     = spank_systime();
    dme->dm.flags    = 0;

    if (extra != NULL) {
	memcpy(&dme->dm.extra, extra,
	       sizeof(spank_uav_distance_measurement_extra_t));
    } else {
	memset(&dme->dm.extra, 0,
	       sizeof(spank_uav_distance_measurement_extra_t));
    }

    // Put it in the pending list
    STAILQ_INSERT_TAIL(&ctx->distance_measurements.pending, dme, entries);

    // Save callback
    distance_cb = ctx->cb.distance;
    
 done:
    // Release mutex
    spank_mutex_release(&ctx->mutex);

    // Trigger callback (out of mutex)
    if (distance_cb)
	distance_cb(NULL);
}



/* Initialize UAV interraction
 */
bool
spank_uav_init(void)
{
    struct spank_uav_ctx *ctx = &spank_uav_ctx;

    // Init mutex
    spank_mutex_init(&ctx->mutex);

    // Initial posture is at origin with no speed
    memset(&ctx->posture, 0, sizeof(ctx->posture));

    // Create list of unused allocated distance measurement 
    for (int i = 0 ; i < SPANK_UAV_DISTANCE_MEASUREMENT_QUEUE_SIZE ; i++) {
	STAILQ_INSERT_TAIL(&ctx->distance_measurements.free,
			   &ctx->distance_measurements.entries[i],
			   entries);
    }
    
    return true;
}



/* Set callbacks
 */
void
spank_uav_set_callbacks(spank_uav_callback_t distance)
{
    struct spank_uav_ctx *ctx = &spank_uav_ctx;

    spank_mutex_acquire(&ctx->mutex, SPANK_SYSTICKS_INFINITE);
    ctx->cb.distance = distance;
    spank_mutex_release(&ctx->mutex);
}
