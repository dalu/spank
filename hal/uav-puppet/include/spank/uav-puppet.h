/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: 
 */

#ifndef __SPANK__UAV_PUPPET_H__
#define __SPANK__UAV_PUPPET_H__

#include <spank/io.h>

#define SPANK_UAV_DROP_BEHAVIOUR_DEFAULT	0
#define SPANK_UAV_DROP_BEHAVIOUR_OLDEST		1
#define SPANK_UAV_DROP_BEHAVIOUR_RECENT		2
#define SPANK_UAV_DROP_BEHAVIOUR_RANDOM		3


typedef spank_io_frame_properties_t spank_uav_distance_measurement_extra_t;

/**
 * Distance measurement structure
 */
typedef struct spank_uav_distance_measurement {
    // Data from measurement
    spank_distance_t distance;		// Distance
    spank_stddev_t   stddev;		// Standard deviation
    spank_posture_t  posture;		// Other UAV posture
    uint64_t         id;		// Other UAV id

    // Extra info
    spank_uav_distance_measurement_extra_t extra;
    
    // Extra information added at time of:
    //  - insertion: time
    //  - retrieval: has_more
    spank_systime_t  time;		// Time of captured distance
    union {
	uint8_t      flags;		// Flags
	struct {
	    uint8_t  has_more : 1;  	// More measurements are pending
	};
    };
} spank_uav_distance_measurement_t;

/**
 * Information about internal management
 */
typedef struct spank_uav_metainfo {
    uint16_t pending;
    uint32_t lost;
} spank_uav_metainfo_t;

/**
 * Callback definition
 */
typedef void (*spank_uav_callback_t)(void *args);

/**
 * Set callbacks
 *
 * @param distance		callback for distance received
 */
void spank_uav_set_callbacks(spank_uav_callback_t distance);

/**
 * Get information about internal management
 */
void spank_uav_get_metainfo(spank_uav_metainfo_t *info);

/**
 * Set drop behaviour when internal queue is full
 */
void spank_uav_set_drop_behaviour(int behaviour);

/**
 * Get drop behaviour when internal queue is full
 */
int spank_uav_get_drop_behaviour(void);

/**
 * Set UAV posture.
 *
 * @param posture		UAV posture
 */
void spank_uav_set_posture(spank_posture_t *posture);

/**
 * Get distance measurement.
 * Also retrieve internal management information if requested.
 *
 * @note Must be released as soon as possible.
 *
 * @param[out] info		Internal management information
 *
 * @return NULL if no distance available
 * @return pointer on distance measurement
 */
spank_uav_distance_measurement_t *spank_uav_get_distance_measurement(spank_uav_metainfo_t *info);

/**
 * Release distance measurement.
 *
 * @param dm			pointer on distance measurement 
 *				(NULL-value accepted)
 */
void spank_uav_release_distance_measurement(
				    spank_uav_distance_measurement_t *dm);

#endif
