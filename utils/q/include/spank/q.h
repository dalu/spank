/*
 * Copyright (c) 2018 Stephane D'Alu
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _SPANK__Q_H_
#define _SPANK__Q_H_

#include <stdint.h>

/*
 * https://en.wikipedia.org/wiki/Q_(number_format)
 */

#define SPANK_Qm    8
#define SPANK_Qn    (16 - SPANK_Qm)
#define SPANK_Q_WITH_NAN 1

#define SPANK_I_TO_Q_NOSAT(m) ((m) << SPANK_Qm)

/**
 * Minimum value for Q number
 */
#if SPANK_Q_WITH_NAN
#  define SPANK_Q_MIN (-INT16_MAX)
#else
#  define SPANK_Q_MIN INT16_MIN
#endif

/**
 * Maximum value for Q number
 */
#define SPANK_Q_MAX INT16_MAX


/**
 * Not a Number
 */
#if SPANK_Q_WITH_NAN
#define SPANK_Q_NAN INT16_MIN
#endif

/**
 * Q number type
 */
typedef int16_t spank_q_t;

/**
 * Test if Q number is not a number (NaN)
 */
static inline int
spank_q_isnan(spank_q_t q)
{
#if SPANK_Q_WITH_NAN
    return q == SPANK_Q_NAN;
#else
    return 0;
#endif
}    

/**
 * Adding Q number.
 */
spank_q_t spank_q_add(spank_q_t a, spank_q_t b);

/**
 * Substracting Q number.
 */
spank_q_t spank_q_sub(spank_q_t a, spank_q_t b);

/**
 * Multiplying Q number.
 */
spank_q_t spank_q_mul(spank_q_t a, spank_q_t b);

/**
 * Dividing Q number.
 */
spank_q_t spank_q_div(spank_q_t a, spank_q_t b);

/**
 * Converting a float to a Q number
 */
spank_q_t spank_f_to_q(float f);

/**
 * Converting an int to a Q number.
 */
spank_q_t spank_i_to_q(int  q);

/**
 * Converting a Q number to an int (fractional part is truncated).
 */
static inline int
spank_q_to_i(spank_q_t q)
{
    return q >> SPANK_Qm;
}

/**
 * Converting a Q number to a float.
 */
static inline float
spank_q_to_f(spank_q_t q)
{
#if SPANK_Q_WITH_NAN
    if (q == SPANK_Q_NAN)
	return SPANK_Q_NAN;
#endif
    
    return ((float)q) / (1 << SPANK_Qm);
}

/**
 * Adding Q number (possible overflow wrapping).
 */
static inline spank_q_t
spank_q_add_nosat(spank_q_t a, spank_q_t b)
{ 
#if SPANK_Q_WITH_NAN
    if ((a == SPANK_Q_NAN) || (b == SPANK_Q_NAN))
	return SPANK_Q_NAN;
#endif

    return a + b;
}

/**
 * Substracting Q number (possible overflow wrapping).
 */
static inline spank_q_t
spank_q_sub_nosat(spank_q_t a, spank_q_t b)
{
#if SPANK_Q_WITH_NAN
    if ((a == SPANK_Q_NAN) || (b == SPANK_Q_NAN))
	return SPANK_Q_NAN;
#endif
    
    return a - b;
}

/**
 * Parse a string to extract the Q number
 *
 * @param str			String to parse
 * @param endptr		Last character parsed (if not NULL)
 *
 * @return Q number
 */
spank_q_t spank_q_parse(const char *str, char **endptr);

#endif
