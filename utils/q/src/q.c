/*
 * Copyright (c) 2018 Stephane D'Alu
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include "spank/q.h"

/* Maximum number of digits */
#define SPANK_Q_FDIGITS ((int)((SPANK_Qm / 3.32192809489) + 1))

static inline spank_q_t
spank_q_sat(int32_t x)
{
#if SPANK_Q_WITH_NAN
    if (x == SPANK_Q_NAN) {
	return SPANK_Q_NAN;
    }
#endif
    if        (x > SPANK_Q_MAX) {
        return SPANK_Q_MAX;
    } else if (x < SPANK_Q_MIN) {
        return SPANK_Q_MIN;
    } else {
        return (spank_q_t)x;
    }
}

spank_q_t
spank_q_add(spank_q_t a, spank_q_t b)
{
#if SPANK_Q_WITH_NAN
    if ((a == SPANK_Q_NAN) || (b == SPANK_Q_NAN))
	return SPANK_Q_NAN;
#endif
    
    return spank_q_sat((int32_t)a + (int32_t)b);
}

spank_q_t
spank_q_sub(spank_q_t a, spank_q_t b)
{
#if SPANK_Q_WITH_NAN
    if ((a == SPANK_Q_NAN) || (b == SPANK_Q_NAN))
	return SPANK_Q_NAN;
#endif
    
    return spank_q_sat((int32_t)a - (int32_t)b);
}

spank_q_t
spank_q_mul(spank_q_t a, spank_q_t b)
{
#if SPANK_Q_WITH_NAN
    if ((a == SPANK_Q_NAN) || (b == SPANK_Q_NAN))
	return SPANK_Q_NAN;
#endif
    
    int32_t tmp;
    tmp = (int32_t)a * (int32_t)b;    
    tmp += 1 << (SPANK_Qm - 1);
    return spank_q_sat(tmp >> SPANK_Qm);
}

spank_q_t
spank_q_div(spank_q_t a, spank_q_t b)
{
#if SPANK_Q_WITH_NAN
    if ((a == SPANK_Q_NAN) || (b == SPANK_Q_NAN))
	return SPANK_Q_NAN;
#endif
    
    int32_t tmp;
    tmp = (int32_t)a << SPANK_Qm;
    if((tmp >= 0 && b >= 0) || (tmp < 0 && b < 0)) {   
        tmp += b / 2;
    } else {
        tmp -= b / 2;
    }
    return (spank_q_t)(tmp / b);
}

spank_q_t
spank_f_to_q(float f)
{
#if SPANK_Q_WITH_NAN
    if (isnan(f))
	return SPANK_Q_NAN;
#endif
    
    return spank_q_sat(roundf(f * (1 << SPANK_Qm)));
}

spank_q_t
spank_i_to_q(int  q)
{
    return spank_q_sat((int32_t)q << SPANK_Qm);
}

spank_q_t
spank_q_parse(const char *str, char **endptr)
{
    char *eob;
    long v;

    /* Integer part */
    v = strtol(str, &eob, 10);
    
    /* What's next? */
    switch(*eob) {
    case '\0':
	v *= (1 << SPANK_Qm);
	str = eob;
	goto done;
    case '.':
	str = eob + 1;
	break;
    default:
	goto fail;
    }

    /* Fractional part */
    /*  limit to the meaningful digits */
    eob = (char *)(str + SPANK_Q_FDIGITS);
    while ((str < eob) && isdigit(*str)) {
	v = (v * 10) + (*str++ - '0');
    }
    v *= (1 << SPANK_Qm);
    while (eob++ - str < SPANK_Q_FDIGITS) {
	v  /= 10;
    }
    /*  consume remaining digits */
    while (isdigit(*str)) {
	str++;
    }
    if (*str != '\0') {
    fail:
	if (endptr) {
	    *endptr = (char *)str;
	}
	return 0;
    }
    
 done:
    if (endptr) {
	*endptr = (char *)str;
    }
    return spank_q_sat(v);
}
